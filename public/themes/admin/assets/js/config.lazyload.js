// lazyload config

angular.module('app')
    /**
   * jQuery plugin config use ui-jq directive , config the js and css files that required
   * key: function name of the jQuery plugin
   * value: array of the css js file located
   */
  .constant('JQ_CONFIG', {
      easyPieChart:   [   'themes/admin/assets/libs/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js'],
      sparkline:      [   'themes/admin/assets/libs/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js'],
      plot:           [   'themes/admin/assets/libs/jquery/flot/jquery.flot.js',
                          'themes/admin/assets/libs/jquery/flot/jquery.flot.pie.js', 
                          'themes/admin/assets/libs/jquery/flot/jquery.flot.resize.js',
                          'themes/admin/assets/libs/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                          'themes/admin/assets/libs/jquery/flot.orderbars/js/jquery.flot.orderBars.js',
                          'themes/admin/assets/libs/jquery/flot-spline/js/jquery.flot.spline.min.js'],
      moment:         [   'themes/admin/assets/libs/jquery/moment/moment.js'],
      screenfull:     [   'themes/admin/assets/libs/jquery/screenfull/dist/screenfull.min.js'],
      slimScroll:     [   'themes/admin/assets/libs/jquery/slimscroll/jquery.slimscroll.min.js'],
      sortable:       [   'themes/admin/assets/libs/jquery/html5sortable/jquery.sortable.js'],
      nestable:       [   'themes/admin/assets/libs/jquery/nestable/jquery.nestable.js',
                          'themes/admin/assets/libs/jquery/nestable/jquery.nestable.css'],
      filestyle:      [   'themes/admin/assets/libs/jquery/bootstrap-filestyle/src/bootstrap-filestyle.js'],
      slider:         [   'themes/admin/assets/libs/jquery/bootstrap-slider/bootstrap-slider.js',
                          'themes/admin/assets/libs/jquery/bootstrap-slider/bootstrap-slider.css'],
      chosen:         [   'themes/admin/assets/libs/jquery/chosen/chosen.jquery.min.js',
                          'themes/admin/assets/libs/jquery/chosen/bootstrap-chosen.css'],
      TouchSpin:      [   'themes/admin/assets/libs/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                          'themes/admin/assets/libs/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
      wysiwyg:        [   'themes/admin/assets/libs/jquery/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                          'themes/admin/assets/libs/jquery/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
      dataTable:      [   'themes/admin/assets/libs/jquery/datatables/media/js/jquery.dataTables.min.js',
                          'themes/admin/assets/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                          'themes/admin/assets/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
      vectorMap:      [   'themes/admin/assets/libs/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js', 
                          'themes/admin/assets/libs/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                          'themes/admin/assets/libs/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js',
                          'themes/admin/assets/libs/jquery/bower-jvectormap/jquery-jvectormap.css'],
      footable:       [   'themes/admin/assets/libs/jquery/footable/v3/js/footable.min.js',
                          'themes/admin/assets/libs/jquery/footable/v3/css/footable.bootstrap.min.css'],
      fullcalendar:   [   'themes/admin/assets/libs/jquery/moment/moment.js',
                          'themes/admin/assets/libs/jquery/fullcalendar/dist/fullcalendar.min.js',
                          'themes/admin/assets/libs/jquery/fullcalendar/dist/fullcalendar.css',
                          'themes/admin/assets/libs/jquery/fullcalendar/dist/fullcalendar.theme.css'],
      daterangepicker:[   'themes/admin/assets/libs/jquery/moment/moment.js',
                          'themes/admin/assets/libs/jquery/bootstrap-daterangepicker/daterangepicker.js',
                          'themes/admin/assets/libs/jquery/bootstrap-daterangepicker/daterangepicker-bs3.css'],
      tagsinput:      [   'themes/admin/assets/libs/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
                          'themes/admin/assets/libs/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.css']
                      
    }
  )
  .constant('MODULE_CONFIG', [
      {
          name: 'ngGrid',
          files: [
              'themes/admin/assets/libs/angular/ng-grid/build/ng-grid.min.js',
              'themes/admin/assets/libs/angular/ng-grid/ng-grid.min.css',
              'themes/admin/assets/libs/angular/ng-grid/ng-grid.bootstrap.css'
          ]
      },
      {
          name: 'ui.grid',
          files: [
              'themes/admin/assets/libs/angular/angular-ui-grid/ui-grid.min.js',
              'themes/admin/assets/libs/angular/angular-ui-grid/ui-grid.min.css',
              'themes/admin/assets/libs/angular/angular-ui-grid/ui-grid.bootstrap.css'
          ]
      },
      {
          name: 'ui.select',
          files: [
              'themes/admin/assets/libs/angular/angular-ui-select/dist/select.min.js',
              'themes/admin/assets/libs/angular/angular-ui-select/dist/select.min.css'
          ]
      },
      {
          name:'angularFileUpload',
          files: [
            'themes/admin/assets/libs/angular/angular-file-upload/angular-file-upload.js'
          ]
      },
      {
          name:'ui.calendar',
          files: ['themes/admin/assets/libs/angular/angular-ui-calendar/src/calendar.js']
      },
      {
          name: 'ngImgCrop',
          files: [
              'themes/admin/assets/libs/angular/ngImgCrop/compile/minified/ng-img-crop.js',
              'themes/admin/assets/libs/angular/ngImgCrop/compile/minified/ng-img-crop.css'
          ]
      },
      {
          name: 'angularBootstrapNavTree',
          files: [
              'themes/admin/assets/libs/angular/angular-bootstrap-nav-tree/dist/abn_tree_directive.js',
              'themes/admin/assets/libs/angular/angular-bootstrap-nav-tree/dist/abn_tree.css'
          ]
      },
      {
          name: 'toaster',
          files: [
              'themes/admin/assets/libs/angular/angularjs-toaster/toaster.js',
              'themes/admin/assets/libs/angular/angularjs-toaster/toaster.css'
          ]
      },
      {
          name: 'textAngular',
          files: [
              'themes/admin/assets/libs/angular/textAngular/dist/textAngular-sanitize.min.js',
              'themes/admin/assets/libs/angular/textAngular/dist/textAngular.min.js'
          ]
      },
      {
          name: 'vr.directives.slider',
          files: [
              'themes/admin/assets/libs/angular/venturocket-angular-slider/build/angular-slider.min.js',
              'themes/admin/assets/libs/angular/venturocket-angular-slider/build/angular-slider.css'
          ]
      },
      {
          name: 'com.2fdevs.videogular',
          files: [
              'themes/admin/assets/libs/angular/videogular/videogular.min.js'
          ]
      },
      {
          name: 'com.2fdevs.videogular.plugins.controls',
          files: [
              'themes/admin/assets/libs/angular/videogular-controls/controls.min.js'
          ]
      },
      {
          name: 'com.2fdevs.videogular.plugins.buffering',
          files: [
              'themes/admin/assets/libs/angular/videogular-buffering/buffering.min.js'
          ]
      },
      {
          name: 'com.2fdevs.videogular.plugins.overlayplay',
          files: [
              'themes/admin/assets/libs/angular/videogular-overlay-play/overlay-play.min.js'
          ]
      },
      {
          name: 'com.2fdevs.videogular.plugins.poster',
          files: [
              'themes/admin/assets/libs/angular/videogular-poster/poster.min.js'
          ]
      },
      {
          name: 'com.2fdevs.videogular.plugins.imaads',
          files: [
              'themes/admin/assets/libs/angular/videogular-ima-ads/ima-ads.min.js'
          ]
      },
      {
          name: 'xeditable',
          files: [
              'themes/admin/assets/libs/angular/angular-xeditable/dist/js/xeditable.min.js',
              'themes/admin/assets/libs/angular/angular-xeditable/dist/css/xeditable.css'
          ]
      },
      {
          name: 'smart-table',
          files: [
              'themes/admin/assets/libs/angular/angular-smart-table/dist/smart-table.min.js'
          ]
      },
      {
          name: 'angular-skycons',
          files: [
              'themes/admin/assets/libs/angular/angular-skycons/angular-skycons.js'
          ]
      }
    ]
  )
  // oclazyload config
  .config(['$ocLazyLoadProvider', 'MODULE_CONFIG', function($ocLazyLoadProvider, MODULE_CONFIG) {
      // We configure ocLazyLoad to use the lib script.js as the async loader
      $ocLazyLoadProvider.config({
          debug:  false,
          events: true,
          modules: MODULE_CONFIG
      });
  }])
;
