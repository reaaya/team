'use strict';

/**
 * Config for the router
 */
angular.module('app')
    .run(
        ['$rootScope', '$state', '$stateParams',
            function($rootScope, $state, $stateParams) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
            }
        ]
    )
    .config(
        ['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG',
            function($stateProvider, $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG) {
                
                var layout = "front/tpl/admin";
                $urlRouterProvider
                    .otherwise('/dashbord');

                $stateProvider
                    .state('app', {
                        abstract: true,
                        url: '/app',
                        templateUrl: layout
                    })
                    .state('app.dashboard', {
                        url: '/dashboard',
                        templateUrl: 'admin/tpl/dashbord',
                    })

                    //Reaya Routers Admin
                    .state('admin', {
                        abstract: true,
                        //url: '/admin',
                        templateUrl: layout
                    })
                    .state('admin.dashbord', {
                        url: '/dashbord',
                        templateUrl: 'front/tpl/dashbord',
                    })
                    .state('admin.schools_generator', {
                        url: '/schools/generator',
                        templateUrl: 'front/tpl/schools/generator',
                    })

                    //Reaya Routers CourseSession
                    .state('coursesession', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('coursesession.timeline', {
                        url: '/coursesession/timeline',
                        templateUrl: 'front/tpl/coursesession/timeline',
                        resolve: load('themes/admin/assets/js/controllers/coursesession_timeline.js'),
                        controller: 'CourseSessionTimelineController'
                    })




                    .state('app.dashboard-v2', {
                        url: '/dashboard-v2',
                        templateUrl: 'themes/admin/assets/tpl/app_dashboard_v2.html',
                        resolve: load(['themes/admin/assets/js/controllers/chart.js'])
                    })
                    .state('app.dashboard-v3', {
                        url: '/dashboard-v3',
                        templateUrl: 'themes/admin/assets/tpl/app_dashboard_v3.html',
                        resolve: load(['themes/admin/assets/js/controllers/chart.js'])
                    })
                    .state('app.ui', {
                        url: '/ui',
                        template: '<div ui-view class="fade-in-up"></div>'
                    })
                    .state('app.ui.buttons', {
                        url: '/buttons',
                        templateUrl: 'themes/admin/assets/tpl/ui_buttons.html'
                    })
                    .state('app.ui.icons', {
                        url: '/icons',
                        templateUrl: 'themes/admin/assets/tpl/ui_icons.html'
                    })
                    .state('app.ui.grid', {
                        url: '/grid',
                        templateUrl: 'themes/admin/assets/tpl/ui_grid.html'
                    })
                    .state('app.ui.widgets', {
                        url: '/widgets',
                        templateUrl: 'themes/admin/assets/tpl/ui_widgets.html'
                    })
                    .state('app.ui.bootstrap', {
                        url: '/bootstrap',
                        templateUrl: 'themes/admin/assets/tpl/ui_bootstrap.html'
                    })
                    .state('app.ui.sortable', {
                        url: '/sortable',
                        templateUrl: 'themes/admin/assets/tpl/ui_sortable.html'
                    })
                    .state('app.ui.scroll', {
                        url: '/scroll',
                        templateUrl: 'themes/admin/assets/tpl/ui_scroll.html',
                        resolve: load('js/controllers/scroll.js')
                    })
                    .state('app.ui.portlet', {
                        url: '/portlet',
                        templateUrl: 'themes/admin/assets/tpl/ui_portlet.html'
                    })
                    .state('app.ui.timeline', {
                        url: '/timeline',
                        templateUrl: 'themes/admin/assets/tpl/ui_timeline.html'
                    })
                    .state('app.ui.tree', {
                        url: '/tree',
                        templateUrl: 'themes/admin/assets/tpl/ui_tree.html',
                        resolve: load(['angularBootstrapNavTree', 'js/controllers/tree.js'])
                    })
                    .state('app.ui.toaster', {
                        url: '/toaster',
                        templateUrl: 'themes/admin/assets/tpl/ui_toaster.html',
                        resolve: load(['toaster', 'js/controllers/toaster.js'])
                    })
                    .state('app.ui.jvectormap', {
                        url: '/jvectormap',
                        templateUrl: 'themes/admin/assets/tpl/ui_jvectormap.html',
                        resolve: load('js/controllers/vectormap.js')
                    })
                    .state('app.ui.googlemap', {
                        url: '/googlemap',
                        templateUrl: 'themes/admin/assets/tpl/ui_googlemap.html',
                        resolve: load(['themes/admin/assets/js/app/map/load-google-maps.js', 'js/app/map/ui-map.js', 'js/app/map/map.js'], function() {
                            return loadGoogleMaps(); })
                    })
                    .state('app.chart', {
                        url: '/chart',
                        templateUrl: 'themes/admin/assets/tpl/ui_chart.html',
                        resolve: load('js/controllers/chart.js')
                    })
                    // table
                    .state('app.table', {
                        url: '/table',
                        template: '<div ui-view></div>'
                    })
                    .state('app.table.static', {
                        url: '/static',
                        templateUrl: 'themes/admin/assets/tpl/table_static.html'
                    })
                    .state('app.table.datatable', {
                        url: '/datatable',
                        templateUrl: 'themes/admin/assets/tpl/table_datatable.html'
                    })
                    .state('app.table.footable', {
                        url: '/footable',
                        templateUrl: 'themes/admin/assets/tpl/table_footable.html'
                    })
                    .state('app.table.grid', {
                        url: '/grid',
                        templateUrl: 'themes/admin/assets/tpl/table_grid.html',
                        resolve: load(['ngGrid', 'themes/admin/assets/js/controllers/grid.js'])
                    })
                    .state('app.table.uigrid', {
                        url: '/uigrid',
                        templateUrl: 'themes/admin/assets/tpl/table_uigrid.html',
                        resolve: load(['ui.grid', 'themes/admin/assets/js/controllers/uigrid.js'])
                    })
                    .state('app.table.editable', {
                        url: '/editable',
                        templateUrl: 'themes/admin/assets/tpl/table_editable.html',
                        controller: 'XeditableCtrl',
                        resolve: load(['xeditable', 'themes/admin/assets/js/controllers/xeditable.js'])
                    })
                    .state('app.table.smart', {
                        url: '/smart',
                        templateUrl: 'themes/admin/assets/tpl/table_smart.html',
                        resolve: load(['smart-table', 'themes/admin/assets/js/controllers/table.js'])
                    })
                    // form
                    .state('app.form', {
                        url: '/form',
                        template: '<div ui-view class="fade-in"></div>',
                        resolve: load('themes/admin/assets/js/controllers/form.js')
                    })
                    .state('app.form.components', {
                        url: '/components',
                        templateUrl: 'themes/admin/assets/tpl/form_components.html',
                        resolve: load(['ngBootstrap', 'daterangepicker', 'themes/admin/assets/js/controllers/form.components.js'])
                    })
                    .state('app.form.elements', {
                        url: '/elements',
                        templateUrl: 'themes/admin/assets/tpl/form_elements.html'
                    })
                    .state('app.form.validation', {
                        url: '/validation',
                        templateUrl: 'themes/admin/assets/tpl/form_validation.html'
                    })
                    .state('app.form.wizard', {
                        url: '/wizard',
                        templateUrl: 'themes/admin/assets/tpl/form_wizard.html'
                    })
                    .state('app.form.fileupload', {
                        url: '/fileupload',
                        templateUrl: 'themes/admin/assets/tpl/form_fileupload.html',
                        resolve: load(['angularFileUpload', 'themes/admin/assets/js/controllers/file-upload.js'])
                    })
                    .state('app.form.imagecrop', {
                        url: '/imagecrop',
                        templateUrl: 'themes/admin/assets/tpl/form_imagecrop.html',
                        resolve: load(['ngImgCrop', 'themes/admin/assets/js/controllers/imgcrop.js'])
                    })
                    .state('app.form.select', {
                        url: '/select',
                        templateUrl: 'themes/admin/assets/tpl/form_select.html',
                        controller: 'SelectCtrl',
                        resolve: load(['ui.select', 'themes/admin/assets/js/controllers/select.js'])
                    })
                    .state('app.form.slider', {
                        url: '/slider',
                        templateUrl: 'themes/admin/assets/tpl/form_slider.html',
                        controller: 'SliderCtrl',
                        resolve: load(['vr.directives.slider', 'themes/admin/assets/js/controllers/slider.js'])
                    })
                    .state('app.form.editor', {
                        url: '/editor',
                        templateUrl: 'themes/admin/assets/tpl/form_editor.html',
                        controller: 'EditorCtrl',
                        resolve: load(['textAngular', 'themes/admin/assets/js/controllers/editor.js'])
                    })
                    .state('app.form.xeditable', {
                        url: '/xeditable',
                        templateUrl: 'themes/admin/assets/tpl/form_xeditable.html',
                        controller: 'XeditableCtrl',
                        resolve: load(['xeditable', 'themes/admin/assets/js/controllers/xeditable.js'])
                    })
                    // pages
                    .state('app.page', {
                        url: '/page',
                        template: '<div ui-view class="fade-in-down"></div>'
                    })
                    .state('app.page.profile', {
                        url: '/profile',
                        templateUrl: 'themes/admin/assets/tpl/page_profile.html'
                    })
                    .state('app.page.post', {
                        url: '/post',
                        templateUrl: 'themes/admin/assets/tpl/page_post.html'
                    })
                    .state('app.page.search', {
                        url: '/search',
                        templateUrl: 'themes/admin/assets/tpl/page_search.html'
                    })
                    .state('app.page.invoice', {
                        url: '/invoice',
                        templateUrl: 'themes/admin/assets/tpl/page_invoice.html'
                    })
                    .state('app.page.price', {
                        url: '/price',
                        templateUrl: 'themes/admin/assets/tpl/page_price.html'
                    })
                    .state('app.docs', {
                        url: '/docs',
                        templateUrl: 'themes/admin/assets/tpl/docs.html'
                    })
                    // others
                    .state('lockme', {
                        url: '/lockme',
                        templateUrl: 'themes/admin/assets/tpl/page_lockme.html'
                    })
                    .state('access', {
                        url: '/access',
                        template: '<div ui-view class="fade-in-right-big smooth"></div>'
                    })
                    .state('access.signin', {
                        url: '/signin',
                        templateUrl: 'themes/admin/assets/tpl/page_signin.html',
                        resolve: load(['themes/admin/assets/js/controllers/signin.js'])
                    })
                    .state('access.signup', {
                        url: '/signup',
                        templateUrl: 'themes/admin/assets/tpl/page_signup.html',
                        resolve: load(['themes/admin/assets/js/controllers/signup.js'])
                    })
                    .state('access.forgotpwd', {
                        url: '/forgotpwd',
                        templateUrl: 'themes/admin/assets/tpl/page_forgotpwd.html'
                    })
                    .state('access.404', {
                        url: '/404',
                        templateUrl: 'themes/admin/assets/tpl/page_404.html'
                    })

                // fullCalendar
                .state('app.calendar', {
                    url: '/calendar',
                    templateUrl: 'themes/admin/assets/tpl/app_calendar.html',
                    // use resolve to load other dependences
                    resolve: load(['moment', 'fullcalendar', 'ui.calendar', 'themes/admin/assets/js/app/calendar/calendar.js'])
                })

                // mail
                .state('app.mail', {
                        abstract: true,
                        url: '/mail',
                        templateUrl: 'themes/admin/assets/tpl/mail.html',
                        // use resolve to load other dependences
                        resolve: load(['themes/admin/assets/js/app/mail/mail.js', 'themes/admin/assets/js/app/mail/mail-service.js', 'moment'])
                    })
                    .state('app.mail.list', {
                        url: '/inbox/{fold}',
                        templateUrl: 'themes/admin/assets/tpl/mail.list.html'
                    })
                    .state('app.mail.detail', {
                        url: '/{mailId:[0-9]{1,4}}',
                        templateUrl: 'themes/admin/assets/tpl/mail.detail.html'
                    })
                    .state('app.mail.compose', {
                        url: '/compose',
                        templateUrl: 'themes/admin/assets/tpl/mail.new.html'
                    })

                .state('layout', {
                        abstract: true,
                        url: '/layout',
                        templateUrl: 'themes/admin/assets/tpl/layout.html'
                    })
                    .state('layout.fullwidth', {
                        url: '/fullwidth',
                        views: {
                            '': {
                                templateUrl: 'themes/admin/assets/tpl/layout_fullwidth.html'
                            },
                            'footer': {
                                templateUrl: 'themes/admin/assets/tpl/layout_footer_fullwidth.html'
                            }
                        },
                        resolve: load(['themes/admin/assets/js/controllers/vectormap.js'])
                    })
                    .state('layout.mobile', {
                        url: '/mobile',
                        views: {
                            '': {
                                templateUrl: 'themes/admin/assets/tpl/layout_mobile.html'
                            },
                            'footer': {
                                templateUrl: 'themes/admin/assets/tpl/layout_footer_mobile.html'
                            }
                        }
                    })
                    .state('layout.app', {
                        url: '/app',
                        views: {
                            '': {
                                templateUrl: 'themes/admin/assets/tpl/layout_app.html'
                            },
                            'footer': {
                                templateUrl: 'themes/admin/assets/tpl/layout_footer_fullwidth.html'
                            }
                        },
                        resolve: load(['themes/admin/assets/js/controllers/tab.js'])
                    })
                    .state('apps', {
                        abstract: true,
                        url: '/apps',
                        templateUrl: 'themes/admin/assets/tpl/layout.html'
                    })
                    .state('apps.note', {
                        url: '/note',
                        templateUrl: 'themes/admin/assets/tpl/apps_note.html',
                        resolve: load(['themes/admin/assets/js/app/note/note.js', 'moment'])
                    })
                    .state('apps.contact', {
                        url: '/contact',
                        templateUrl: 'themes/admin/assets/tpl/apps_contact.html',
                        resolve: load(['themes/admin/assets/js/app/contact/contact.js'])
                    })
                    .state('app.weather', {
                        url: '/weather',
                        templateUrl: 'themes/admin/assets/tpl/apps_weather.html',
                        resolve: load(['themes/admin/assets/js/app/weather/skycons.js', 'angular-skycons', 'js/app/weather/ctrl.js', 'moment'])
                    })
                    .state('app.todo', {
                        url: '/todo',
                        templateUrl: 'themes/admin/assets/tpl/apps_todo.html',
                        resolve: load(['themes/admin/assets/js/app/todo/todo.js', 'moment'])
                    })
                    .state('app.todo.list', {
                        url: '/{fold}'
                    })
                    .state('app.note', {
                        url: '/note',
                        templateUrl: 'themes/admin/assets/tpl/apps_note_material.html',
                        resolve: load(['themes/admin/assets/js/app/note/note.js', 'moment'])
                    })
                    .state('music', {
                        url: '/music',
                        templateUrl: 'themes/admin/assets/tpl/music.html',
                        controller: 'MusicCtrl',
                        resolve: load([
                            'com.2fdevs.videogular',
                            'com.2fdevs.videogular.plugins.controls',
                            'com.2fdevs.videogular.plugins.overlayplay',
                            'com.2fdevs.videogular.plugins.poster',
                            'com.2fdevs.videogular.plugins.buffering',
                            'themes/admin/assets/js/app/music/ctrl.js',
                            'themes/admin/assets/js/app/music/theme.css'
                        ])
                    })
                    .state('music.home', {
                        url: '/home',
                        templateUrl: 'themes/admin/assets/tpl/music.home.html'
                    })
                    .state('music.genres', {
                        url: '/genres',
                        templateUrl: 'themes/admin/assets/tpl/music.genres.html'
                    })
                    .state('music.detail', {
                        url: '/detail',
                        templateUrl: 'themes/admin/assets/tpl/music.detail.html'
                    })
                    .state('music.mtv', {
                        url: '/mtv',
                        templateUrl: 'themes/admin/assets/tpl/music.mtv.html'
                    })
                    .state('music.mtvdetail', {
                        url: '/mtvdetail',
                        templateUrl: 'themes/admin/assets/tpl/music.mtv.detail.html'
                    })
                    .state('music.playlist', {
                        url: '/playlist/{fold}',
                        templateUrl: 'themes/admin/assets/tpl/music.playlist.html'
                    })
                    .state('app.material', {
                        url: '/material',
                        template: '<div ui-view class="wrapper-md"></div>',
                        resolve: load(['themes/admin/assets/js/controllers/material.js'])
                    })
                    .state('app.material.button', {
                        url: '/button',
                        templateUrl: 'themes/admin/assets/tpl/material/button.html'
                    })
                    .state('app.material.color', {
                        url: '/color',
                        templateUrl: 'themes/admin/assets/tpl/material/color.html'
                    })
                    .state('app.material.icon', {
                        url: '/icon',
                        templateUrl: 'themes/admin/assets/tpl/material/icon.html'
                    })
                    .state('app.material.card', {
                        url: '/card',
                        templateUrl: 'themes/admin/assets/tpl/material/card.html'
                    })
                    .state('app.material.form', {
                        url: '/form',
                        templateUrl: 'themes/admin/assets/tpl/material/form.html'
                    })
                    .state('app.material.list', {
                        url: '/list',
                        templateUrl: 'themes/admin/assets/tpl/material/list.html'
                    })
                    .state('app.material.ngmaterial', {
                        url: '/ngmaterial',
                        templateUrl: 'themes/admin/assets/tpl/material/ngmaterial.html'
                    });

                function load(srcs, callback) {
                    return {
                        deps: ['$ocLazyLoad', '$q',
                            function($ocLazyLoad, $q) {
                                var deferred = $q.defer();
                                var promise = false;
                                srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                                if (!promise) {
                                    promise = deferred.promise;
                                }
                                angular.forEach(srcs, function(src) {
                                    promise = promise.then(function() {
                                        if (JQ_CONFIG[src]) {
                                            return $ocLazyLoad.load(JQ_CONFIG[src]);
                                        }
                                        angular.forEach(MODULE_CONFIG, function(module) {
                                            if (module.name == src) {
                                                name = module.name;
                                            } else {
                                                name = src;
                                            }
                                        });
                                        return $ocLazyLoad.load(name);
                                    });
                                });
                                deferred.resolve();
                                return callback ? promise.then(function() {
                                    return callback(); }) : promise;
                            }
                        ]
                    }
                }


            }
        ]
    );