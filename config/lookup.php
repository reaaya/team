<?php
/**
 * Created by PhpStorm.
 * User: aziz
 * Date: 04/03/16
 * Time: 06:33 ص
 * Updated by achraf: 05/03/2016
 */

define('Session_status_Coming_session', 1);
define('Session_status_Current_session', 2);
define('Session_status_Closed_session', 3);
define('Session_status_Canceled_session', 4);

define('School_Job_Pdg', 1);
define('School_Job_Admin', 2);
define('School_Job_Teacher', 3);

define('Attendance_status_On_time', 1);
define('Attendance_status_Waiting_he_come', 2);
define('Attendance_status_Not_called_yet', 3);
define('Attendance_status_On_late', 4);
define('Attendance_status_Early_quitted', 5);
define('Attendance_status_Absent', 6);
define('Attendance_status_Good_quitted', 7);

define('Symbol_A', 1);
define('Symbol_B', 2);
define('Symbol_C', 3);
define('Symbol_D', 4);
define('Symbol_E', 5);
define('Symbol_F', 6);
define('Symbol_G', 7);
define('Symbol_H', 8);
define('Symbol_I', 9);
define('Symbol_J', 10);

define('IDN_TYPE_HAWIYA', 1);
define('IDN_TYPE_IQAMA', 2);

return [
    'session_status' => [
        'coming_session'    => Session_status_Coming_session,
        'current_session'   => Session_status_Current_session,
        'closed_session'    => Session_status_Closed_session,
        'canceled_session'  => Session_status_Canceled_session
    ],
    'attendance_status' => [
        'on_time'    		=> Attendance_status_On_time,
        'waiting_he_come'   => Attendance_status_Waiting_he_come,
        'not_called_yet'    => Attendance_status_Not_called_yet,
        'on_late'  			=> Attendance_status_On_late,
        'early_quitted'  	=> Attendance_status_Early_quitted,
        'absent'  			=> Attendance_status_Absent,
        'good_quitted'  	=> Attendance_status_Good_quitted
    ],
    'school_job'        => [
        'pdg'               => School_Job_Pdg,
        'admin'             => School_Job_Admin,
        'teacher'           => School_Job_Teacher,
    ],
    'symbol'            => [
        'A'                 => Symbol_A,
        'B'                 => Symbol_B,
        'C'                 => Symbol_C,
        'D'                 => Symbol_D,
        'E'                 => Symbol_E,
        'F'                 => Symbol_F,
        'G'                 => Symbol_G,
        'H'                 => Symbol_H,
        'I'                 => Symbol_I,
        'J'                 => Symbol_J,
    ]
];
