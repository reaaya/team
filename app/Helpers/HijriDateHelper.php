<?php
/**
 * Created by PhpStorm.
 * User: aziz
 * Date: 01/03/16
 * Time: 10:09 م
 */

namespace App\Helpers;
use App\Models\HijraDateBase;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class HijriDateHelper
{

//------------------------------------------------------------------------------
// Fonctions sur les dates
//------------------------------------------------------------------------------
    /**
     * Cette fonction convertit une date au format YYYY-mm-dd au time stamp correspondant
     * @param datemysql $madate
     * @return time stamp correspondant
     * @author Rafik BOUBAKER
     */

    static function from_mysql_to_timestamp($madate)
    {

        $arr_dat = explode(' ', $madate);

        $arr_day = explode('-', $arr_dat[0]);
        if($arr_dat[1]);
            $arr_hour = explode(':', $arr_dat[1]);
        if (!$arr_hour[0]) $arr_hour[0] = 0;
        if (!$arr_hour[1]) $arr_hour[1] = 0;
        if (!$arr_hour[2]) $arr_hour[2] = 0;
        $tmstmp = mktime($arr_hour[0], $arr_hour[1], $arr_hour[2], $arr_day[1], $arr_day[2], $arr_day[0]);

        return $tmstmp;
    }

    static function add_dashes_to_gregdate($madate)
    {
        $madate_YYYY = substr($madate, 0, 4);
        $madate_MM = substr($madate, 4, 2);
        $madate_DD = substr($madate, 6, 2);

        return "$madate_YYYY-$madate_MM-$madate_DD";
    }

    static function diff_date($madate2, $madate1)
    {
        //$madate1 = "20151025";
        if (strpos($madate2, '-') === false) {
            $madate2 = HijriDateHelper::add_dashes_to_gregdate($madate2);
        }
        if (strpos($madate1, '-') === false) {
            $madate1 = HijriDateHelper::add_dashes_to_gregdate($madate1);
        }



        $stmp2 = strtotime($madate2);
        $stmp1 = strtotime($madate1);

        // round because some time there 's one hour of tawkit sayfi so it
        // adds or removes 0.0416666666666667  = (1/24), the round static function correct the bug
        $result_diff = round(($stmp2 - $stmp1) / (24 * 3600));

        //die("$madate2 - $madate1 =($stmp2-$stmp1)/(24*3600) = $result_diff");

        return $result_diff;
    }


    static function add_datetime_to_mysql_datetime($mondatetime = "", $a = 0, $m = 0, $j = 0, $h = 0, $n = 0, $s = 0)
    {
        if (!$mondatetime) $mondatetime = date("Y-m-d H:i:s");

        $arr_dat = explode(' ', $mondatetime);
        $arr_day = explode('-', $arr_dat[0]);
        $arr_hour = explode(':', $arr_dat[1]);
        $tmstmp = mktime($arr_hour[0] + $h, $arr_hour[1] + $n, $arr_hour[2] + $s, $arr_day[1] + $m, $arr_day[2] + $j, $arr_day[0] + $a);

        return date("Y-m-d H:i:s", $tmstmp);
    }

    /**
     * Cette fonction calcule la date de début de semaine (lundi) de la
     * semaine dans laquelle se trouve la date date.
     * Si aucune date est mise c la date en cours donc la semaine en cours
     * @param timestamp $auj : retour de mktime ou cf from_mysql_to_timestamp aussi
     * @return date au format mysql YYYY-mm-dd
     * @author Rafik BOUBAKER
     */

    static function debut_semaine($auj = 0)
    {
        if (!$auj) $auj = time();
        $n = date('w', $auj);
        if ($n > 0) $offset = 1; else $offset = -6;
        $premier_jour = mktime(0, 0, 0, date("m", $auj), date("d", $auj) - $n + $offset, date("Y", $auj));
        $datedeb = date("Y-m-d", $premier_jour);

        return $datedeb;
    }

    /**
     * Cette fonction calcule la date de fin de semaine (dimanche) de la
     * semaine dans laquelle se trouve la date date.
     * Si aucune date est mise c la date en cours donc la semaine en cours
     * @param timestamp $auj : retour de mktime ou cf from_mysql_to_timestamp aussi
     * @return date au format mysql YYYY-mm-dd
     * @author Rafik BOUBAKER
     */

    static function fin_semaine($auj = 0, $taille_sem = 7)
    {
        if (!$auj) $auj = time();
        $n = date('w', $auj);
        if ($n > 0) $offset = 0; else $offset = -7;
        $dernier_jour = mktime(0, 0, 0, date("m", $auj), date("d", $auj) - $n + $offset + $taille_sem, date("Y", $auj));
        $datefin = date("Y-m-d", $dernier_jour);

        return $datefin;
    }

    /**
     * Cette fonction ....@todo-rafik
     * @param timestamp $auj : retour de mktime ou cf from_mysql_to_timestamp aussi
     * @return date au format mysql YYYY-mm-dd
     * @author Rafik BOUBAKER
     */


    static function debut_mois($auj = 0)
    {
        if (!$auj) $auj = time();
        return date("Y-m-01", $auj);
    }

    /**
     * Cette fonction ....@todo-rafik
     * @param timestamp $auj : retour de mktime ou cf from_mysql_to_timestamp aussi
     * @return date au format mysql YYYY-mm-dd
     * @author Rafik BOUBAKER
     */

    static function fin_mois($auj = 0)
    {
        if (!$auj) $auj = time();
        return date("Y-m-", $auj) . intval(date("t", date("m", $auj)));
    }

    /**
     * Cette fonction revien N jours si elle tombe sur un weekend elle avance ou recule encore selon si n<0 ou >0
     * @param int $n : nbre de jour
     *        int $date_dep : date de départ au format mysql YYYY-mm-dd
     * @return date au format mysql YYYY-mm-dd
     * @author Rafik BOUBAKER
     */

    static function add_njours_ouvrables($n, $date_dep)
    {
        $tms_dep = HijriDateHelper::from_mysql_to_timestamp($date_dep);

        // vérifier que la journée en cours est ouvrable sinon décaler le début de calcul
        // à jusqu'au debut d'une date ouvrable

        $nbr_jour_in_week = date('w', $tms_dep);
        if ($nbr_jour_in_week == 0) {
            $arr_dat = explode(' ', $date_dep);
            $arr_day = explode('-', $arr_dat[0]);
            $arr_hour = explode(':', $arr_dat[1]);
            if ($n >= 0) $delta = 1;
            else $delta = -1;
            $tms_dep = mktime(0, 0, 0, $arr_day[1], $arr_day[2] + $delta, $arr_day[0]);
            $date_dep = date("Y-m-d H:i:s", $tms_dep);
            //debugg("c'est un dimanche j'avance de $delta jour(s) = $date_dep");
        }

        if ($nbr_jour_in_week == 6) {
            $arr_dat = explode(' ', $date_dep);
            $arr_day = explode('-', $arr_dat[0]);
            $arr_hour = explode(':', $arr_dat[1]);
            if ($n >= 0) $delta = 2;
            else $delta = 0;
            $tms_dep = mktime(0, 0, 0, $arr_day[1], $arr_day[2] + $delta, $arr_day[0]);
            $date_dep = date("Y-m-d H:i:s", $tms_dep);
            //debugg("c'est un samedi j'avance de $delta jour(s) = $date_dep");
        }


        if ($n > 0) {
            $nb = $n - 1;
            $new_date = HijriDateHelper::demain_ouvrable($tms_dep);
        } elseif ($n < 0) {
            $nb = $n + 1;
            $new_date = HijriDateHelper::hier_ouvrable($tms_dep);
        } elseif ($n == 0) {
            return $date_dep;
        }

        //debugg("recursivite - add_njours_ouvrables($nb,$new_date)");

        return HijriDateHelper::add_njours_ouvrables($nb, $new_date);
    }


    static function hier_ouvrable($auj = 0)
    {
        if (!$auj) $auj = time();
        $nbr_jour_in_week = date('w', $auj);
        $mois = intval(date('m', $auj));
        $jour = intval(date('d', $auj));
        $annee = intval(date('Y', $auj));
        $HH = intval(date('H', $auj));
        $ii = intval(date('i', $auj));
        $ss = intval(date('s', $auj));
        //debugg("hier ouv of $jour/$mois/$annee $HH:$ii:$ss");
        if ($nbr_jour_in_week == 1) {
            $hier_ouv = date('Y-m-d H:i:s', mktime($HH, $ii, $ss, $mois, ($jour - 3), $annee));
        } elseif ($nbr_jour_in_week == 0) {
            $hier_ouv = date('Y-m-d H:i:s', mktime($HH, $ii, $ss, $mois, ($jour - 2), $annee));
        } else {
            $hier_ouv = date('Y-m-d H:i:s', mktime($HH, $ii, $ss, $mois, ($jour - 1), $annee));
        }
        //debugg("hier ouv = $hier_ouv");
        return $hier_ouv;
    }

    /**
     * Cette fonction revien 1 jour en arrière si elle tombe sur un weekend elle revien a vendredi
     * @param timestamp $auj : retour de mktime ou cf from_mysql_to_timestamp aussi
     * @return date au format mysql YYYY-mm-dd
     * @author Rafik BOUBAKER
     */


    static function demain_ouvrable($auj = 0)
    {
        if (!$auj) $auj = time();
        $nbr_jour_in_week = date('w', $auj);
        $mois = intval(date('m', $auj));
        $jour = intval(date('d', $auj));
        $annee = intval(date('Y', $auj));
        $HH = intval(date('H', $auj));
        $ii = intval(date('i', $auj));
        $ss = intval(date('s', $auj));
        //debugg("demain ouv of $jour/$mois/$annee $HH:$ii:$ss");
        if ($nbr_jour_in_week == 5) {
            $hier_ouv = date('Y-m-d H:i:s', mktime($HH, $ii, $ss, $mois, ($jour + 3), $annee));
        } elseif ($nbr_jour_in_week == 6) {
            $hier_ouv = date('Y-m-d H:i:s', mktime($HH, $ii, $ss, $mois, ($jour + 2), $annee));
        } else {
            $hier_ouv = date('Y-m-d H:i:s', mktime($HH, $ii, $ss, $mois, ($jour + 1), $annee));
        }
        //debugg("demain ouv = $hier_ouv");
        return $hier_ouv;
    }

    /**
     * Avec la fonction suivante vous allez pouvoir transformer
     * une date au format MySQL : 2002-06-11 en : Mardi 11 Juin 2002.
     * En option vous pouvez choisir de ne pas afficher le jour de la semaine et/ou l'année.
     *
     * @param timestamp $auj : retour de mktime ou cf from_mysql_to_timestamp aussi
     * @return date au format mysql YYYY-mm-dd
     * @author Rafik BOUBAKER
     */

    static function mysqldate_to_explicit_fr_date($MyDate, $WeekDayOn = 1, $YearOn = 1, $MonthNameOn = 1, $Separator = " ")
    {
        if (!$MyDate) return $MyDate;

        $MyMonths = array("جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان",
            "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر");
        $MyDays = array("الأحد", "الأثنين", "الثلاثاء", "الإربعاء", "الخميس",
            "الجمعة", "السبت");
        list($MyDate2, $MyDate3) = explode(' ', $MyDate);
        $DF = explode('-', $MyDate2);
        $TheDay = getdate(mktime(0, 0, 0, $DF[1], $DF[2], $DF[0]));

        $MyDateFinal = $DF[2] . $Separator;
        if ($MonthNameOn)
            $MyDateFinal .= $MyMonths[$DF[1] - 1];
        else
            $MyDateFinal .= $DF[1];

        if ($WeekDayOn) $MyDateFinal = $MyDays[$TheDay["wday"]] . " " . $MyDateFinal;
        if ($YearOn) $MyDateFinal .= $Separator . $DF[0];

        return $MyDateFinal;
    }

    static function mysqldate_to_tn_hour($MyDate, $seconds = 0)
    {
        $arr_dat = explode(' ', $MyDate);
        $arr_dat1 = explode('-', $arr_dat[0]);
        $arr_time = explode(':', $arr_dat[1]);
        $hh = $arr_time[0];
        $nn = $arr_time[1];
        $ss = $arr_time[2];

        $result = "الساعة $hh و $nn دق";
        if ($seconds) $result .= " و $ss ث";
        return $result;
    }

    static function mysqldate_to_tn_date($MyDate, $WeekDayOn = 1)
    {
        return HijriDateHelper::mysqldate_to_explicit_fr_date($MyDate, $WeekDayOn, 1, 1, " ");
    }

    static function mysqldate_to_fr_date($MyDate, $WeekDayOn = 0)
    {
        return HijriDateHelper::mysqldate_to_explicit_fr_date($MyDate, $WeekDayOn, 1, 0, "/");
    }


    static function fr_to_mysqldate_date($MyDate)
    {
        $tab_dat = explode("/", $MyDate);
        $dd = $tab_dat[0];
        $mm = $tab_dat[1];
        $yyyy = $tab_dat[2];

        return "$yyyy-$mm-$dd";
    }


    /**
     * Cette fonction vous permet de calculer une date dans le futur
     * (dans un certain nombre de jours) à partir de la date du jour ou d'une date donnée
     * au format MySQL (YYYY-MM-DD). Il suffit de fournir le nombre de jours en paramètre :
     *
     * @param timestamp $auj : retour de mktime ou cf from_mysql_to_timestamp aussi
     * @return date au format mysql YYYY-mm-dd
     * @author Rafik BOUBAKER
     */
    static function add_x_days_to_mysqldate($nb, $from_date = '')
    {
        if (!$from_date) $from_date = date('Y-m-d');

        $from_tab = explode('-', $from_date);

        $to_date = date("Y-m-d", mktime(0, 0, 0, $from_tab[1], $from_tab[2] + $nb, $from_tab[0]));

        return ($to_date);
    }


    /**
     * Convertir un nombre de secondes en son équivalent heure:minute:seconde.
     * @param timestamp $auj : retour de mktime ou cf from_mysql_to_timestamp aussi
     * @return date au format mysql YYYY-mm-dd
     * @author Rafik BOUBAKER
     */

    static function convert_time_tohhmmss($temps, $Format = '')
    {
        //combien d'heures ?
        $hours = floor($temps / 3600);

        //combien de minutes ?
        $min = floor(($temps - ($hours * 3600)) / 60);

        if ($Format == 'h:m') {
            if ($sec > 0) $min += 1;
            if ($min == 60) {
                $hours += 1;
                $min = 0;
            }
        }

        if ($min < 10) $min = "0" . $min;

        //combien de secondes
        $sec = round($temps - ($hours * 3600) - ($min * 60));
        if ($sec < 10) $sec = "0" . $sec;

        if ($Format == 'h:m')
            return $hours . ":" . $min;
        else
            return $hours . ":" . $min . ":" . $sec;

    }

    static function ds_to_hhmmss($tds, $Format = '')
    {
        $temps = $tds / 10;

        //combien d'heures ?
        $hours = floor($temps / 3600);

        //combien de minutes ?
        $min = floor(($temps - ($hours * 3600)) / 60);

        if ($Format == 'h:m') {
            if ($sec > 0) $min += 1;
            if ($min == 60) {
                $hours += 1;
                $min = 0;
            }
        }

        if ($min < 10) $min = "0" . $min;

        //combien de secondes
        $sec = round($temps - ($hours * 3600) - ($min * 60));
        if ($sec < 10) $sec = "0" . $sec;

        if ($Format == 'h:m')
            return $hours . ":" . $min;
        else
            return $hours . ":" . $min . ":" . $sec;

    }

    static function long_hijri_date($hijri_year, $mm, $dd, $TheDay, $WeekDayOn = 1, $YearOn = 1, $MonthNameOn = 1, $Separator = " ")
    {

        $MyMonths = array("محرم", "صفر", "ربيع الأول", "ربيع الآخر", "جمادى الأولى", "جمادى الآخرة",
            "رجب", "شعبان", "رمضان", "شوّال", "ذو القعدة", "ذو الحجة");

        $MyDays = array("الأحد", "الأثنين", "الثلاثاء", "الإربعاء", "الخميس",
            "الجمعة", "السبت");

        $MyDateFinal = $dd . $Separator;
        if ($MonthNameOn)
            $MyDateFinal .= $MyMonths[$mm - 1];
        else
            $MyDateFinal .= $mm;

        if ($WeekDayOn) $MyDateFinal = $MyDays[$TheDay["wday"]] . " " . $MyDateFinal;
        if ($YearOn) $MyDateFinal .= $Separator . $hijri_year;

        return $MyDateFinal;
    }


    static function hijri_current_date($mode = "hdate")
    {
        return HijriDateHelper::to_hijri(date("Ymd"), $mode);
    }

// gdate should be whithout dashes

    static function to_hijri($gdate, $mode = "hdate")
    {

        $row_hijri = HijraDateBase::select(
                        DB::Raw('hijri_year as HY'),
                        DB::Raw('hijri_month as HM'),
                        DB::Raw('greg_date as GD')
                    )->where('greg_date','=',DB::Raw("(select max(greg_date) from hijra_date_base where greg_date <= '$gdate')"))
                    ->first()->toArray();

        if (strpos($gdate, '-') === false) {
            $gdate = HijriDateHelper::add_dashes_to_gregdate($gdate);
        }

        $DF = explode('-', $gdate);
        $TheDay = getdate(mktime(0, 0, 0, $DF[1], $DF[2], $DF[0]));

        $hijri_year = $row_hijri["HY"];
        $hijri_month = $row_hijri["HM"];
        $greg_date = $row_hijri["GD"];

        $mm = str_pad($hijri_month, 2, "0", STR_PAD_LEFT);

        $hijri_day = HijriDateHelper::diff_date($gdate, $greg_date) + 1;

        $dd = str_pad($hijri_day, 2, '0', STR_PAD_LEFT);

        if ($mode == "hdate")
            return $hijri_year.$mm.$dd;
        elseif ($mode == "hlist")
            return array($hijri_year, $mm, $dd);
        elseif ($mode == "hdate_long")
            return HijriDateHelper::long_hijri_date($hijri_year, $mm, $dd, $TheDay);
        else
            return "?????";
    }


}