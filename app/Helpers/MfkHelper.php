<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

/**
 * Created by PhpStorm.
 * User: aziz
 * Date: 27/02/16
 * Time: 07:13 م
 */
class MfkHelper
{
    public static function mkfDecode($mfk,$table,$field=null){
        $items =  explode(',',trim($mfk, ","));
        $i = 0;
        foreach($items as $item){
            $item = Lang::get($table.'.'.$item);
            $items[$i] = $item;
            $i++;
        }
        return $items;
    }

    public static function mfkEncode(array $data)
    {
        return ','.implode(',',$data).',';
    }
}