<?php
/**
 * Created by PhpStorm.
 * User: aziz
 * Date: 16/03/16
 * Time: 04:47 م
 */

namespace App\Helpers;


class validateId{

    var $stype = array(
        'ar'=>array (1=>"هوية وطنية سعودية", "رقم إقامة لغير السعوديين"),
        'en'=>array (1=>"Saudi National ID", "Non-Saudi Resident ID (Iqama)"));
    var $hl = "ar";
    function validateSAID($hl = "ar"){
        $this->hl = $hl;
    }
    function scheck($id){
        return $this->stype[$this->hl][$this->check($id)];
    }
    static function check($id){
        $id = trim($id);
        if(!is_numeric($id)) return false;
        if(strlen($id) !== 10) return false;
        $type = substr ( $id, 0, 1 );
        if($type != 2 && $type != 1 ) return false;
        $sum=0;
        for( $i = 0 ; $i<10 ; $i++ ) {
            //echo "  $id <b>"."</b> -";
            if ( $i % 2 == 0){
                $ZFOdd = str_pad ( ( substr($id, $i, 1) * 2 ), 2, "0", STR_PAD_LEFT );
                $sum += substr ( $ZFOdd, 0, 1 ) + substr ( $ZFOdd, 1, 1 );
            }else{
                $sum += substr ( $id, $i, 1 );
            }
        }
        //echo $sum;
        return $sum%10 ? false : $type;
    }
}