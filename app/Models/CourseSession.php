<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class CourseSession extends Model
{
    
	public $table = "course_session";
    

	public $fillable = [
	    "session_hdate",
		"session_end_time",
		"session_start_time",
		"course_id",
		"level_class_id",
		"prof_id",
		"school_id",
		"session_status_id",
		"session_status_comment",
		"symbol",
		"year"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"session_hdate" => "string",
		"session_end_time" => "string",
		"session_start_time" => "string",
		"course_id" => "integer",
		"level_class_id" => "integer",
		"prof_id" => "integer",
		"school_id" => "integer",
		"session_status_id" => "integer",
		"session_status_comment" => "string",
		"symbol" => "string",
		"year" => "string"
    ];

	public static $rules = [
	    
	];

	/**
	 * Display a listing of the CourseSession of one day.
	 * POST|HEAD /courseSessions/isExist
	 *
	 * @return Response
	 */
	public static function isExist(array $course_session)
	{
		extract($course_session);
		$nb = CourseSession::where('group_num','=',$group_num)
			->where('school_id','=',$school_id)
			->where('year','=',$year)
			->where('hmonth','=',$hmonth)
			->where('hday_num','=',$hday_num)
			->where('level_class_id','=',$level_class_id)
			->where('symbol','=',$symbol)
			->where('session_order','=',$session_order)
			->get()->count();

		if($nb > 0)
			return true;
		else
			return false;
	}

	public static function checkStatusCourseSession($status_id, array $course_session)
	{
		extract($course_session);
		$course_session = CourseSession::where('prof_id', $prof_id)
										->where('session_status_id',$status_id)
										->where('group_num','=',$group_num)
										->where('school_id','=',$school_id)
										->where('year','=',$year)
										->where('hmonth','=',$hmonth)
										->where('hday_num','=',$hday_num)
										->where('level_class_id','=',$level_class_id)
										->where('symbol','=',$symbol)
										->first();
		return $course_session;
	}


	public static function checkIfIsOwnerAnTeacher(array $course_session)
	{
		extract($course_session);
		$result = CourseSession::join('school_employee','school_employee.rea_user_id','=','course_session.prof_id')
								->where('course_session.group_num','=',$group_num)
								->where('course_session.school_id','=',$school_id)
								->where('course_session.year','=',$year)
								->where('course_session.hmonth','=',$hmonth)
								->where('course_session.hday_num','=',$hday_num)
								->where('course_session.level_class_id','=',$level_class_id)
								->where('course_session.symbol','=',$symbol)
								->where('course_session.session_order','=',$session_order)
								->where('course_session.session_status_id', Session_status_Coming_session)
								->where('school_employee.school_job_mfk','like','%,'.School_Job_Teacher.',%')
								->first();
		return $result;
	}

	public static function updateCourseSession(array $course_session,array $attributes)
	{
		extract($course_session);
		$result = CourseSession::where('prof_id', $prof_id)
					->where('session_status_id',$session_status_id)
					->where('group_num','=',$group_num)
					->where('school_id','=',$school_id)
					->where('year','=',$year)
					->where('hmonth','=',$hmonth)
					->where('hday_num','=',$hday_num)
					->where('level_class_id','=',$level_class_id)
					->where('symbol','=',$symbol)
					->where('session_order','=',$session_order)
					->update($attributes);
		return $result;
	}

}
