<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class HijraDateBase extends Model
{
    
	public $table = "hijra_date_base";
    

	public $fillable = [
	    "HIJRI_YEAR",
		"HIJRI_MONTH",
		"HIJRI_MONTH_DAYS",
		"GREG_DATE",
		"HOLIDAY_DAYS"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "HIJRI_YEAR" => "integer",
		"HIJRI_MONTH" => "integer",
		"HIJRI_MONTH_DAYS" => "integer",
		"GREG_DATE" => "string",
		"HOLIDAY_DAYS" => "string"
    ];

	public static $rules = [
	    
	];

}
