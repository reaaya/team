<?php namespace App\Models;



class StudentFile extends BaseModel
{
    
	public $table = "student_file";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"final_rating_id",
		"level_class_id",
		"school_year_id",
		"student_file_status_id",
		"student_num"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"final_rating_id" => "integer",
		"level_class_id" => "integer",
		"school_year_id" => "integer",
		"student_file_status_id" => "integer",
		"student_num" => "string"
    ];

	public static $rules = [
	    
	];

}
