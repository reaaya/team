<?php namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class ReaUser extends Authenticatable
{

    public $table = "rea_user";


    public $fillable = [
        "active",
        "city_id",
        "country_id",
        "current_school_id",
        "idn_type_id",
        "address",
        "cp",
        "firstname",
        "f_firstname",
        "idn",
        "lastname",
        "mobile",
        "quarter",
        "sim_card_sn",
        "pwd",
        "email"
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
        'creation_user_id' => "integer",
        "update_user_id" => "integer",
        "validation_user_id" => "integer",
        "version" => "integer",
        "update_groups_mfk" => "string",
        "delete_groups_mfk" => "string",
        "display_groups_mfk" => "string",
        "sci_id" => "integer",
        "active" => "string",
        "city_id" => "integer",
        "country_id" => "integer",
        "current_school_id" => "integer",
        "idn_type_id" => "integer",
        "address" => "string",
        "cp" => "string",
        "firstname" => "string",
        "f_firstname" => "string",
        "idn" => "string",
        "lastname" => "string",
        "mobile" => "string",
        "pwd" => "string",
        "email" => "string",
        "quarter" => "string",
        "sim_card_sn" => "string",
    ];

    public static $rules = [
        'firstname' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users',
        'pwd' => 'required|confirmed|min:4',
    ];

    protected $hidden = [
        'pwd',
        'remember_token'
    ];

    protected $guarded = [
        'creation_user_id',
        "update_user_id",
        "validation_user_id",
        "version",
        "update_groups_mfk",
        "delete_groups_mfk",
        "display_groups_mfk",
        "sci_id",
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->attributes['id'];
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    Public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] = bcrypt($password);
    }



    public function getAuthPassword()
    {
        return $this->pwd;
    }

    public static function updateLang($user_id,$lang_id)
    {
        ReaUser::where('id','=',$user_id)->update(['lang_id'=>$lang_id]);
    }

    public static function updateSimCardSerialNumber($user_id,$sim_card_sn){
        return ReaUser::where(['id'=>$user_id])->update(['sim_card_sn'=>$sim_card_sn]);
    }

    public static function getActiveUsers()
    {
        return ReaUser::where(['active'=>'Y'])->get();
    }

    public static function getByIdnAndIdnType($idn,$idn_type)
    {
        return ReaUser::where(['idn'=>$idn,'idn_type_id'=>$idn_type])->first();
    }

    public static function checkRegistration($idn,$idn_type)
    {
        $user = ReaUser::getByIdnAndIdnType($idn,$idn_type);
        if($user)
            return $user->id;
        else
            return false;
    }
}
