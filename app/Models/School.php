<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Support\Facades\DB;
class School extends Model
{
    
	public $table = "school";

	public $fillable = [
	    "scapacity",
		"expiring_hdate",
		"city_id",
		"genre_id",
		"group_school_id",
		"lang_id",
		"school_type_id",
		"period_mfk",
		"sp2",
		"address",
		"maps_location_url",
		"pc",
		"quarter",
		"school_name_ar",
		"school_name_en",
		"sp1",
		"group_num"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"scapacity" => "integer",
		"expiring_hdate" => "string",
		"city_id" => "integer",
		"genre_id" => "integer",
		"group_school_id" => "integer",
		"lang_id" => "integer",
		"school_type_id" => "integer",
		"period_mfk" => "string",
		"address" => "string",
		"maps_location_url" => "string",
		"pc" => "string",
		"quarter" => "string",
		"school_name_ar" => "string",
		"school_name_en" => "string",
		"group_num"	=> "integer"
    ];

	public static $rules = [
	    
	];

	public static function getSchoolsByUserId($id,$lang){
		return self::select(DB::Raw('school.id AS school_id'),'school_employee.school_job_mfk','school.school_name_'.$lang)
			->leftJoin('school_employee','school_employee.school_id','=','school.id')
			->where('school_employee.rea_user_id','=',$id)
			->get();
	}

	public static function getGroupNum($id)
	{
		return self::findOrFail($id)->value('group_num');
	}

}
