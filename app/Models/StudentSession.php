<?php namespace App\Models;



class StudentSession extends BaseModel
{
    
	public $table = "student_session";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"coming_time",
		"exit_time",
		"coming_status_id",
		"course_session_id",
		"exit_status_id",
		"rea_user_id",
		"comments"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"coming_time" => "string",
		"exit_time" => "string",
		"coming_status_id" => "integer",
		"course_session_id" => "integer",
		"exit_status_id" => "integer",
		"rea_user_id" => "integer",
		"comments" => "string"
    ];

	public static $rules = [
	    
	];


	public static function updateStudentSession($course_session,$student_id,$attributes)
	{
		extract($course_session);
		if(isset($attributes['comments']) && is_array($attributes['comments']));
			$attributes['comments'] = MfkHelper::mfkEncode($attributes['comments']);

		$result = StudentSession::where('group_num','=',$group_num)
			->where('school_id','=',$school_id)
			->where('year','=',$year)
			->where('hmonth','=',$hmonth)
			->where('hday_num','=',$hday_num)
			->where('level_class_id','=',$level_class_id)
			->where('symbol','=',$symbol)
			->where('session_order','=',$session_order)
			->where('student_id','=',$student_id)
			->update($attributes);
		return $result;
	}

	public static function isExist(array $course_session,$student_id)
	{
		extract($course_session);
		$student_session = StudentSession::where('group_num','=',$group_num)
			->where('school_id','=',$school_id)
			->where('year','=',$year)
			->where('hmonth','=',$hmonth)
			->where('hday_num','=',$hday_num)
			->where('level_class_id','=',$level_class_id)
			->where('symbol','=',$symbol)
			->where('session_order','=',$session_order)
			->where('student_id','=',$student_id)
			->get();

		if($student_session->count() > 0)
			return $student_session;
		else
			return false;
	}
}
