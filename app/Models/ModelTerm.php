<?php namespace App\Models;



class ModelTerm extends BaseModel
{
    
	public $table = "model_term";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"end_study_hdate",
		"end_vacancy_hdate",
		"start_study_hdate",
		"start_vacancy_hdate",
		"school_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"end_study_hdate" => "string",
		"end_vacancy_hdate" => "string",
		"start_study_hdate" => "string",
		"start_vacancy_hdate" => "string",
		"school_id" => "integer"
    ];

	public static $rules = [
	    
	];

}
