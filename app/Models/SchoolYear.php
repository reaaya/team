<?php namespace App\Models;


class SchoolYear extends BaseModel
{
    
	public $table = "school_year";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"school_year_end_hdate",
		"school_year_start_hdate",
		"school_id",
		"school_year_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"school_year_end_hdate" => "string",
		"school_year_start_hdate" => "string",
		"school_id" => "integer",
		"school_year_name" => "string"
    ];

	public static $rules = [
	    
	];

}
