<?php namespace App\Models;



class SchoolMemberDay extends BaseModel
{
    
	public $table = "school_member_day";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"day_hdate",
		"coming_time",
		"exit_time",
		"coming_status_id",
		"exit_status_id",
		"rea_user_id",
		"school_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"day_hdate" => "string",
		"coming_time" => "string",
		"exit_time" => "string",
		"coming_status_id" => "integer",
		"exit_status_id" => "integer",
		"rea_user_id" => "integer",
		"school_id" => "integer"
    ];

	public static $rules = [
	    
	];

}
