<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Rating extends Model
{
    
	public $table = "rating";
    

	public $fillable = [
	    "lookup_code",
		"id_school",
		"max_pct",
		"min_pct",
		"rating_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "lookup_code" => "string",
		"id_school" => "integer",
		"rating_name" => "string"
    ];

	public static $rules = [
	    
	];

}
