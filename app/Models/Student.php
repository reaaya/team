<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Student extends BaseModel
{
    
	public $table = "student";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"father_rea_user_id",
		"mother_rea_user_id",
		"rea_user_id",
		"resp_rea_user_id",
		"paid",
		"resp_relationship",
		"student_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"father_rea_user_id" => "integer",
		"mother_rea_user_id" => "integer",
		"rea_user_id" => "integer",
		"resp_rea_user_id" => "integer",
		"paid" => "string",
		"resp_relationship" => "string",
		"student_name" => "string"
    ];

	public static $rules = [
	    
	];

	public static function getByResponsibleId($responsible_id)
	{
		return Student::select(DB::Raw('CONCAT(firstname," ",f_firstname," ",lastname) AS fullname'))
					->join('family_relation','family_relation.rea_user_id','=','student.rea_user_id')
					->where('family_relation.resp_rea_user_id','=',$responsible_id)
					->get();
	}
}
