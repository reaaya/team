<?php namespace App\Models;

class Country extends BaseModel
{
    
	public $table = "country";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"time_offset",
		"maintenance_end_time",
		"maintenance_start_time",
		"date_system_id",
		"abrev",
		"country_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"time_offset" => "integer",
		"maintenance_end_time" => "string",
		"maintenance_start_time" => "string",
		"date_system_id" => "integer",
		"abrev" => "string",
		"country_name" => "string"
    ];

	public static $rules = [
	    
	];

}
