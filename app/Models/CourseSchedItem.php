<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class CourseSchedItem extends Model
{
    
	public $table = "course_sched_item";
    

	public $fillable = [
	    "session_end_time",
		"session_start_time",
		"course_id",
		"level_class_id",
		"school_year_id",
		"symbol",
		"session_order",
		"wday_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"session_end_time" => "string",
		"session_start_time" => "string",
		"course_id" => "integer",
		"level_class_id" => "integer",
		"school_year_id" => "integer",
		"symbol" => "string"
    ];

	public static $rules = [
	    
	];

}
