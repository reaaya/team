<?php

namespace App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Dingo\Api\Auth\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class AppBaseController extends Controller
{
    public function __construct()
    {
        $Auth = Auth();
        $this->user = $Auth->user();
    }
    /**
     * Validate request for current resource.
     *
     * @param Request $request
     * @param array   $rules
     * @param array   $messages
     * @param array   $customAttributes
     */
    public function validateRequestOrFail($request, array $rules, $messages = [], $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            throw new HttpException(400, json_encode($validator->errors()->getMessages()));
        }
    }

    public function makeResponse($result, $message, $success)
    {
        return [
            'status'  => $success,
            'message' => $message,
            'data'    => $result,
        ];
    }

    public function sendResponse($result, $message,$success=true)
    {
        return Response::json($this->makeResponse($result, $message,$success));
    }
}
