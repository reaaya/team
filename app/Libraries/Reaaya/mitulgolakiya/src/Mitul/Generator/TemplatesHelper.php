<?php

namespace App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Generator;

class TemplatesHelper
{
    public function getTemplate($template, $type)
    {
        $path = base_path('resources/api-generator-templates/'.$type.'/'.$template.'.stub');

        if (!file_exists($path)) {
            $path = app_path('Libraries/Reaaya/mitulgolakiya/templates/').$type.'/' .$template.'.stub';
        }

        $fileData = file_get_contents($path);

        return $fileData;
    }
}
