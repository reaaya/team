<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web','auth']], function () {
    Route::get('/', function () {
        return view('layouts/app_front');
    });
});



Route::group(['middleware' => ['web']], function () {

    Route::auth();

});



Route::group([
    'version'   => 'v1',
    'prefix' => 'front',
    'namespace' => config('generator.namespace_front_controller'),
    'middleware' => ['web','auth']
], function ($front)
{
    include_once __DIR__ . '/front_routes.php';
});


Route::group(['middleware' => ['web','auth']], function () {

    Route::resource('reaUsers', 'ReaUserController');

    Route::get('reaUsers/{id}/delete', [
        'as' => 'reaUsers.delete',
        'uses' => 'ReaUserController@destroy',
    ]);

    //Route::resource('schools', 'SchoolController');
    Route::get('schools/generator', [
        'as' => 'schools.generator',
        'uses' => 'SchoolController@getSearchGenerator',
    ]);
    /*Route::post('schools/generator', [
        'as' => 'schools.generator',
        'uses' => 'SchoolController@postSearchGenerator',
    ]);*/
    Route::get('schools/{id}/delete', [
        'as' => 'schools.delete',
        'uses' => 'SchoolController@destroy',
    ]);

    Route::resource('schoolLevels', 'SchoolLevelController');

    Route::get('schoolLevels/{id}/delete', [
        'as' => 'schoolLevels.delete',
        'uses' => 'SchoolLevelController@destroy',
    ]);

});


/*
|--------------------------------------------------------------------------
| dingo/api and mitulgolakiya/laravel-api-generator api routes
|--------------------------------------------------------------------------
*/

$api = app('api.router');

$api->group([
	'version'   => 'v1',
    'prefix' => 'api',
	'namespace' => config('generator.namespace_api_controller'),
	'middleware' => ['cors']
], function ($api)
{

	include_once __DIR__ . '/api_routes.php';

	$api->get('errors/{id}', function ($id)
	{
		return \Mitul\Generator\Errors::getErrors([$id]);
	});

	$api->get('errors', function ()
	{
		return \Mitul\Generator\Errors::getErrors([], [], true);
	});

	$api->get('/', function ()
	{
		$links = [];

		return ['links' => $links];
	});

});





Route::resource('cities', 'CityController');

Route::get('cities/{id}/delete', [
    'as' => 'cities.delete',
    'uses' => 'CityController@destroy',
]);


Route::resource('classCourses', 'ClassCourseController');

Route::get('classCourses/{id}/delete', [
    'as' => 'classCourses.delete',
    'uses' => 'ClassCourseController@destroy',
]);


Route::resource('countries', 'CountryController');

Route::get('countries/{id}/delete', [
    'as' => 'countries.delete',
    'uses' => 'CountryController@destroy',
]);


Route::resource('courses', 'CourseController');

Route::get('courses/{id}/delete', [
    'as' => 'courses.delete',
    'uses' => 'CourseController@destroy',
]);


Route::resource('dateSystems', 'DateSystemController');

Route::get('dateSystems/{id}/delete', [
    'as' => 'dateSystems.delete',
    'uses' => 'DateSystemController@destroy',
]);


Route::resource('genres', 'GenreController');

Route::get('genres/{id}/delete', [
    'as' => 'genres.delete',
    'uses' => 'GenreController@destroy',
]);


Route::resource('genres', 'GenreController');

Route::get('genres/{id}/delete', [
    'as' => 'genres.delete',
    'uses' => 'GenreController@destroy',
]);


Route::resource('levelClasses', 'LevelClassController');

Route::get('levelClasses/{id}/delete', [
    'as' => 'levelClasses.delete',
    'uses' => 'LevelClassController@destroy',
]);


Route::resource('periods', 'PeriodController');

Route::get('periods/{id}/delete', [
    'as' => 'periods.delete',
    'uses' => 'PeriodController@destroy',
]);


Route::resource('schoolTypes', 'SchoolTypeController');

Route::get('schoolTypes/{id}/delete', [
    'as' => 'schoolTypes.delete',
    'uses' => 'SchoolTypeController@destroy',
]);


Route::resource('schoolYears', 'SchoolYearController');

Route::get('schoolYears/{id}/delete', [
    'as' => 'schoolYears.delete',
    'uses' => 'SchoolYearController@destroy',
]);


Route::resource('tServices', 'TServiceController');

Route::get('tServices/{id}/delete', [
    'as' => 'tServices.delete',
    'uses' => 'TServiceController@destroy',
]);


Route::resource('alerts', 'AlertController');

Route::get('alerts/{id}/delete', [
    'as' => 'alerts.delete',
    'uses' => 'AlertController@destroy',
]);


Route::resource('attendances', 'AttendanceController');

Route::get('attendances/{id}/delete', [
    'as' => 'attendances.delete',
    'uses' => 'AttendanceController@destroy',
]);


Route::resource('attendanceStatuses', 'AttendanceStatusController');

Route::get('attendanceStatuses/{id}/delete', [
    'as' => 'attendanceStatuses.delete',
    'uses' => 'AttendanceStatusController@destroy',
]);


Route::resource('attendanceTypes', 'AttendanceTypeController');

Route::get('attendanceTypes/{id}/delete', [
    'as' => 'attendanceTypes.delete',
    'uses' => 'AttendanceTypeController@destroy',
]);


Route::resource('classCourseExams', 'ClassCourseExamController');

Route::get('classCourseExams/{id}/delete', [
    'as' => 'classCourseExams.delete',
    'uses' => 'ClassCourseExamController@destroy',
]);


Route::resource('courseSessions', 'CourseSessionController');

Route::get('courseSessions/{id}/delete', [
    'as' => 'courseSessions.delete',
    'uses' => 'CourseSessionController@destroy',
]);


Route::resource('eFiles', 'EFileController');

Route::get('eFiles/{id}/delete', [
    'as' => 'eFiles.delete',
    'uses' => 'EFileController@destroy',
]);


Route::resource('examSessions', 'ExamSessionController');

Route::get('examSessions/{id}/delete', [
    'as' => 'examSessions.delete',
    'uses' => 'ExamSessionController@destroy',
]);


Route::resource('idnTypes', 'IdnTypeController');

Route::get('idnTypes/{id}/delete', [
    'as' => 'idnTypes.delete',
    'uses' => 'IdnTypeController@destroy',
]);


Route::resource('modelTerms', 'ModelTermController');

Route::get('modelTerms/{id}/delete', [
    'as' => 'modelTerms.delete',
    'uses' => 'ModelTermController@destroy',
]);


Route::resource('ratings', 'RatingController');

Route::get('ratings/{id}/delete', [
    'as' => 'ratings.delete',
    'uses' => 'RatingController@destroy',
]);


Route::resource('schoolClasses', 'SchoolClassController');

Route::get('schoolClasses/{id}/delete', [
    'as' => 'schoolClasses.delete',
    'uses' => 'SchoolClassController@destroy',
]);


Route::resource('schoolEmployees', 'SchoolEmployeeController');

Route::get('schoolEmployees/{id}/delete', [
    'as' => 'schoolEmployees.delete',
    'uses' => 'SchoolEmployeeController@destroy',
]);


Route::resource('schoolLevels', 'SchoolLevelController');

Route::get('schoolLevels/{id}/delete', [
    'as' => 'schoolLevels.delete',
    'uses' => 'SchoolLevelController@destroy',
]);


Route::resource('schoolMemberDays', 'SchoolMemberDayController');

Route::get('schoolMemberDays/{id}/delete', [
    'as' => 'schoolMemberDays.delete',
    'uses' => 'SchoolMemberDayController@destroy',
]);


Route::resource('schoolPeriods', 'SchoolPeriodController');

Route::get('schoolPeriods/{id}/delete', [
    'as' => 'schoolPeriods.delete',
    'uses' => 'SchoolPeriodController@destroy',
]);


Route::resource('schoolTerms', 'SchoolTermController');

Route::get('schoolTerms/{id}/delete', [
    'as' => 'schoolTerms.delete',
    'uses' => 'SchoolTermController@destroy',
]);


Route::resource('sessionStatuses', 'SessionStatusController');

Route::get('sessionStatuses/{id}/delete', [
    'as' => 'sessionStatuses.delete',
    'uses' => 'SessionStatusController@destroy',
]);


Route::resource('langs', 'LanguageController');

Route::get('langs/{id}/delete', [
    'as' => 'langs.delete',
    'uses' => 'LanguageController@destroy',
]);



Route::resource('ratings', 'RatingController');

Route::get('ratings/{id}/delete', [
    'as' => 'ratings.delete',
    'uses' => 'RatingController@destroy',
]);


Route::resource('hijraDateBases', 'HijraDateBaseController');

Route::get('hijraDateBases/{id}/delete', [
    'as' => 'hijraDateBases.delete',
    'uses' => 'HijraDateBaseController@destroy',
]);


Route::resource('courseSchedItems', 'CourseSchedItemController');

Route::get('courseSchedItems/{id}/delete', [
    'as' => 'courseSchedItems.delete',
    'uses' => 'CourseSchedItemController@destroy',
]);
