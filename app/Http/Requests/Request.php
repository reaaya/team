<?php

namespace App\Http\Requests;

use Dingo\Api\Http\FormRequest;
// use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    function authorize()
    {
        return true;
    }
}
