<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/



$api->resource("reaUsers", "ReaUserAPIController");

$api->resource("schoolLevels", "SchoolLevelAPIController");
//Function key: Check if rea user is registred
$api->get('users/checkRegistration/{idn_type}/{idn}', [
    "uses"  => "ReaUserAPIController@checkRegistration",
    "as"    => "api.users.check_registration",
]);
// Set our namespace for the underlying routes
$api->group(['namespace' => 'Auth'], function ($api) {
    // Login route

    $api->post('auth', 'AuthAPIController@authenticate');
    $api->post('register', 'AuthAPIController@register');
    // Dogs! All routes in here are protected and thus need a valid token
    //$api->group( [ 'protected' => true, 'middleware' => 'jwt.refresh' ], function ($api) {
    $api->group( [ 'middleware' => 'jwt.auth' ], function ($api) {
        $api->get('users/me', 'AuthAPIController@me');
        $api->get('validate_token', 'AuthAPIController@validateToken');
        // Function key : BF 228 / Link the account with the sim card
        $api->get('users/linkSimCard/{sim_card_sn}', 'AuthAPIController@LinkSimCard');
        // Set School in session
        $api->get('users/setCurrentSchoolId/{school_id}', [
            "as"    => "api.auth.setCurrentSchoolId",
            "uses"  => "AuthAPIController@setCurrentSchoolId",
        ]);

    });
});


$api->group( [ 'middleware' => ['web','auth_or_token'] ], function ($api) {

    //$api->resource("courseSession", "CourseSessionAPIController");
    //$api->resource("studentCourseSession", "StudentCourseSessionAPIController");

    //Function key: #/ Prof Course session list of one day
    $api->get(
        "courseSession/timeline/{date?}",
        [
            "as"=>"courseSession.timeline",
            "uses"=>"CourseSessionAPIController@timeline"
        ]);

    // Function key : BF 254 / Student session list
    $api->get(
        "studentCourseSession/list/{course_session_id}",
        [
            "as"=>"studentCourseSession.listByCourseSessionId",
            "uses"=>"StudentCourseSessionAPIController@listByCourseSessionId"
        ]);

    //Function Key: BF255 / Cancel Session
    $api->post('CourseSession/cancel', [
        "as"    => "courseSession.cancel",
        "uses"  => "CourseSessionAPIController@cancel",
    ]);

    //Function key: BF 256 / Update Only changed fields in StudentCourseSession
    $api->post(
        "StudentCourseSession/performUpdate/{course_session_id}",
        [
            "as"=>"studentCourseSession.performUpdate",
            "uses"=>"StudentCourseSessionAPIController@performUpdate"
        ]);

    //Function key: BF BF245 / Start Course Session
    $api->post('CourseSession/start', [
        "as"    => "courseSession.start",
        "uses"  => "CourseSessionAPIController@start",
        ]);

    //Function key: BF BF256 Update Students Course Session
    $api->post('CourseSession/update_students', [
        "as"    => "courseSession.update_students",
        "uses"  => "CourseSessionAPIController@update_students",
        ]);

    //Function key: BF BF246 Close Course Session
    $api->post('CourseSession/close', [
        "as"    => "api.courseSession.close",
        "uses"  => "CourseSessionAPIController@close"
        ]);
    
    //Function key: List students of one student session
    $api->post('Student/listStudentsByCourseSession', [
            "uses"  => "StudentAPIController@listStudentsByCourseSession",
            "as"    => "api.student.list_students_by_course_session",
        ]);

});

// BF264 Select API Functions
$api->group(['middleware' => ['web','auth']], function ($api) {
    //Function key: Q0007 Get
    $api->get('Course/getAllCourses', [
        "uses"  => "CourseAPIController@getAllCourses",
        "as"    => "api.course.get_all_courses",
    ]);

    //Function key: Q0007 Get
    $api->get('SchoolYear/getAllSchoolYearsBySchoolID/{school_id}', [
        "uses"  => "SchoolYearAPIController@getAllSchoolYearsBySchoolID",
        "as"    => "api.schoolYear.get_by_school_id",
    ]);

    //Function key: Get Weekend Days
    $api->get('School/GetWeekDays/{school_id}', [
        "uses"  => "SchoolAPIController@GetWeekDays",
        "as"    => "api.school.get_week_days",
    ]);

    //Function key: Q0008 Get
    $api->get('SchoolLevel/getAllSchoolLevelsBySchoolYearID/{school_year_id}', [
        "uses"  => "SchoolLevelAPIController@getAllSchoolLevelsBySchoolYearID",
        "as"    => "api.schoolLevel.get_by_school_year_id",
    ]);
    //Function key: Q0009 Get School Class
    $api->get('SchoolClass/getAllSchoolClassBySchoolYearIDAndLevelClassID/{school_year_id}/{level_class_id}', [
        "uses"  => "SchoolClassAPIController@getAllSchoolClassBySchoolYearIDAndLevelClassID",
        "as"    => "api.schoolClass.get_by_school_year_id",
    ]);
    //Function key: Q0010 Get Course Sched Item
    $api->post('CourseSchedItem/GetItems', [
        "uses"  => "CourseSchedItemAPIController@GetItems",
        "as"    => "api.courseSchedItem.get_items",
    ]);
    //Function key: Q0011 Get Default Template
    $api->get('WeekTemplate/GetDefaultTemplate', [
        "uses"  => "CourseSchedItemAPIController@GetDefaultWeekTemplate",
        "as"    => "api.course_sched.get_week_template_template",
    ]);
    //Function key: Q0012 Creat New CourseSched From Template
    $api->post('CourseSchedItem/CreatFromWeekTemplate', [
        "uses"  => "CourseSchedItemAPIController@CreatFromWeekTemplate",
        "as"    => "api.course_sched_item.creat_from_week_tempate",
    ]);
    //Function key: Save course_sched_items
    $api->post('CourseSchedItem/SaveItems', [
        "uses"  => "CourseSchedItemAPIController@SaveItems",
        "as"    => "api.course_sched_item.save",
    ]);
});


$api->resource("schools", "SchoolAPIController");

$api->resource("cities", "CityAPIController");

$api->resource("classCourses", "ClassCourseAPIController");

$api->resource("countries", "CountryAPIController");

$api->resource("courses", "CourseAPIController");

$api->resource("dateSystems", "DateSystemAPIController");

$api->resource("genres", "GenreAPIController");

$api->resource("genres", "GenreAPIController");

$api->resource("levelClasses", "LevelClassAPIController");

$api->resource("periods", "PeriodAPIController");

$api->resource("schoolTypes", "SchoolTypeAPIController");

$api->resource("schoolYears", "SchoolYearAPIController");

$api->resource("tServices", "TServiceAPIController");

$api->resource("alerts", "AlertAPIController");

$api->resource("attendances", "AttendanceAPIController");

$api->resource("attendanceStatuses", "AttendanceStatusAPIController");

$api->resource("attendanceTypes", "AttendanceTypeAPIController");

$api->resource("classCourseExams", "ClassCourseExamAPIController");



$api->resource("eFiles", "EFileAPIController");

$api->resource("examSessions", "ExamSessionAPIController");

$api->resource("idnTypes", "IdnTypeAPIController");

$api->resource("modelTerms", "ModelTermAPIController");

$api->resource("ratings", "RatingAPIController");

$api->resource("schoolClasses", "SchoolClassAPIController");

$api->resource("schoolEmployees", "SchoolEmployeeAPIController");

$api->resource("schoolLevels", "SchoolLevelAPIController");

$api->resource("schoolMemberDays", "SchoolMemberDayAPIController");

$api->resource("schoolPeriods", "SchoolPeriodAPIController");

$api->resource("schoolTerms", "SchoolTermAPIController");

$api->resource("sessionStatuses", "SessionStatusAPIController");

$api->resource("students", "StudentAPIController");

$api->resource("studentExams", "StudentExamAPIController");

$api->resource("studentFiles", "StudentFileAPIController");

$api->resource("studentFileStatuses", "StudentFileStatusAPIController");



$api->resource("languages", "LanguageAPIController");

$api->resource("schools", "SchoolAPIController");

$api->resource("posts", "PostAPIController");

$api->resource("hijraDateBases", "HijraDateBaseAPIController");





$api->resource("courseSchedItems", "CourseSchedItemAPIController");
