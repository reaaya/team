<?php

/*
|--------------------------------------------------------------------------
| FRONT Routes
|--------------------------------------------------------------------------
|
| Here is where all FRONT routes are defined.
|
*/

$front->group(['prefix' => 'tpl'], function ($front) {
    
    Blade::setContentTags('<%', '%>');
    Blade::setEscapedContentTags('<%%', '%%>');

    $front->get('admin', function () {
        return view('front/admin');
    });

    Route::get('admin/blocks/header', function () {
        return view('front/blocks/header');
    });

    Route::get('admin/blocks/aside', function () {
        return view('front/blocks/aside');
    });

    Route::get('admin/blocks/nav', function () {
        return view('front/blocks/nav');
    });

    $front->get('dashbord', function () {
        echo "Dashbord Admin";
    });


    $front->get('schools/generator', [
            'as' => 'schools.generator',
            'uses' => 'SchoolController@getSearchGeneratorFrontTpl',
    ]);


    $front->get('coursesession/timeline', [
            'as' => 'coursesession.timeline',
            'uses' => 'CourseSessionController@getTimelineFrontTpl',
    ]);


});

