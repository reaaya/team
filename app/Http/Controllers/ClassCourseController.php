<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateClassCourseRequest;
use App\Http\Requests\UpdateClassCourseRequest;
use App\Libraries\Repositories\ClassCourseRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ClassCourseController extends AppBaseController
{

	/** @var  ClassCourseRepository */
	private $classCourseRepository;

	function __construct(ClassCourseRepository $classCourseRepo)
	{
		$this->classCourseRepository = $classCourseRepo;
	}

	/**
	 * Display a listing of the ClassCourse.
	 *
	 * @return Response
	 */
	public function index()
	{
		$classCourses = $this->classCourseRepository->paginate(10);

		return view('classCourses.index')
			->with('classCourses', $classCourses);
	}

	/**
	 * Show the form for creating a new ClassCourse.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('classCourses.create');
	}

	/**
	 * Store a newly created ClassCourse in storage.
	 *
	 * @param CreateClassCourseRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateClassCourseRequest $request)
	{
		$input = $request->all();

		$classCourse = $this->classCourseRepository->create($input);

		Flash::success('ClassCourse saved successfully.');

		return redirect(route('classCourses.index'));
	}

	/**
	 * Display the specified ClassCourse.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$classCourse = $this->classCourseRepository->find($id);

		if(empty($classCourse))
		{
			Flash::error('ClassCourse not found');

			return redirect(route('classCourses.index'));
		}

		return view('classCourses.show')->with('classCourse', $classCourse);
	}

	/**
	 * Show the form for editing the specified ClassCourse.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$classCourse = $this->classCourseRepository->find($id);

		if(empty($classCourse))
		{
			Flash::error('ClassCourse not found');

			return redirect(route('classCourses.index'));
		}

		return view('classCourses.edit')->with('classCourse', $classCourse);
	}

	/**
	 * Update the specified ClassCourse in storage.
	 *
	 * @param  int              $id
	 * @param UpdateClassCourseRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateClassCourseRequest $request)
	{
		$classCourse = $this->classCourseRepository->find($id);

		if(empty($classCourse))
		{
			Flash::error('ClassCourse not found');

			return redirect(route('classCourses.index'));
		}

		$this->classCourseRepository->updateRich($request->all(), $id);

		Flash::success('ClassCourse updated successfully.');

		return redirect(route('classCourses.index'));
	}

	/**
	 * Remove the specified ClassCourse from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$classCourse = $this->classCourseRepository->find($id);

		if(empty($classCourse))
		{
			Flash::error('ClassCourse not found');

			return redirect(route('classCourses.index'));
		}

		$this->classCourseRepository->delete($id);

		Flash::success('ClassCourse deleted successfully.');

		return redirect(route('classCourses.index'));
	}
}
