<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateReaUserRequest;
use App\Http\Requests\UpdateReaUserRequest;
use App\Libraries\Repositories\ReaUserRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ReaUserController extends AppBaseController
{

	/** @var  ReaUserRepository */
	private $reaUserRepository;

	function __construct(ReaUserRepository $reaUserRepo)
	{
		$this->reaUserRepository = $reaUserRepo;
	}

	/**
	 * Display a listing of the ReaUser.
	 *
	 * @return Response
	 */
	public function index()
	{
		$reaUsers = $this->reaUserRepository->paginate(10);

		return view('reaUsers.index')
			->with('reaUsers', $reaUsers);
	}

	/**
	 * Show the form for creating a new ReaUser.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('reaUsers.create');
	}

	/**
	 * Store a newly created ReaUser in storage.
	 *
	 * @param CreateReaUserRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateReaUserRequest $request)
	{
		$input = $request->all();

		$reaUser = $this->reaUserRepository->create($input);

		Flash::success('ReaUser saved successfully.');

		return redirect(route('reaUsers.index'));
	}

	/**
	 * Display the specified ReaUser.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$reaUser = $this->reaUserRepository->find($id);

		if(empty($reaUser))
		{
			Flash::error('ReaUser not found');

			return redirect(route('reaUsers.index'));
		}

		return view('reaUsers.show')->with('reaUser', $reaUser);
	}

	/**
	 * Show the form for editing the specified ReaUser.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$reaUser = $this->reaUserRepository->find($id);

		if(empty($reaUser))
		{
			Flash::error('ReaUser not found');

			return redirect(route('reaUsers.index'));
		}

		return view('reaUsers.edit')->with('reaUser', $reaUser);
	}

	/**
	 * Update the specified ReaUser in storage.
	 *
	 * @param  int              $id
	 * @param UpdateReaUserRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateReaUserRequest $request)
	{
		$reaUser = $this->reaUserRepository->find($id);

		if(empty($reaUser))
		{
			Flash::error('ReaUser not found');

			return redirect(route('reaUsers.index'));
		}

		$this->reaUserRepository->updateRich($request->all(), $id);

		Flash::success('ReaUser updated successfully.');

		return redirect(route('reaUsers.index'));
	}

	/**
	 * Remove the specified ReaUser from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$reaUser = $this->reaUserRepository->find($id);

		if(empty($reaUser))
		{
			Flash::error('ReaUser not found');

			return redirect(route('reaUsers.index'));
		}

		$this->reaUserRepository->delete($id);

		Flash::success('ReaUser deleted successfully.');

		return redirect(route('reaUsers.index'));
	}
}
