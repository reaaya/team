<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateModelTermRequest;
use App\Http\Requests\UpdateModelTermRequest;
use App\Libraries\Repositories\ModelTermRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ModelTermController extends AppBaseController
{

	/** @var  ModelTermRepository */
	private $modelTermRepository;

	function __construct(ModelTermRepository $modelTermRepo)
	{
		$this->modelTermRepository = $modelTermRepo;
	}

	/**
	 * Display a listing of the ModelTerm.
	 *
	 * @return Response
	 */
	public function index()
	{
		$modelTerms = $this->modelTermRepository->paginate(10);

		return view('modelTerms.index')
			->with('modelTerms', $modelTerms);
	}

	/**
	 * Show the form for creating a new ModelTerm.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('modelTerms.create');
	}

	/**
	 * Store a newly created ModelTerm in storage.
	 *
	 * @param CreateModelTermRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateModelTermRequest $request)
	{
		$input = $request->all();

		$modelTerm = $this->modelTermRepository->create($input);

		Flash::success('ModelTerm saved successfully.');

		return redirect(route('modelTerms.index'));
	}

	/**
	 * Display the specified ModelTerm.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$modelTerm = $this->modelTermRepository->find($id);

		if(empty($modelTerm))
		{
			Flash::error('ModelTerm not found');

			return redirect(route('modelTerms.index'));
		}

		return view('modelTerms.show')->with('modelTerm', $modelTerm);
	}

	/**
	 * Show the form for editing the specified ModelTerm.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$modelTerm = $this->modelTermRepository->find($id);

		if(empty($modelTerm))
		{
			Flash::error('ModelTerm not found');

			return redirect(route('modelTerms.index'));
		}

		return view('modelTerms.edit')->with('modelTerm', $modelTerm);
	}

	/**
	 * Update the specified ModelTerm in storage.
	 *
	 * @param  int              $id
	 * @param UpdateModelTermRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateModelTermRequest $request)
	{
		$modelTerm = $this->modelTermRepository->find($id);

		if(empty($modelTerm))
		{
			Flash::error('ModelTerm not found');

			return redirect(route('modelTerms.index'));
		}

		$this->modelTermRepository->updateRich($request->all(), $id);

		Flash::success('ModelTerm updated successfully.');

		return redirect(route('modelTerms.index'));
	}

	/**
	 * Remove the specified ModelTerm from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$modelTerm = $this->modelTermRepository->find($id);

		if(empty($modelTerm))
		{
			Flash::error('ModelTerm not found');

			return redirect(route('modelTerms.index'));
		}

		$this->modelTermRepository->delete($id);

		Flash::success('ModelTerm deleted successfully.');

		return redirect(route('modelTerms.index'));
	}
}
