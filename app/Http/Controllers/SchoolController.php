<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSchoolRequest;
use App\Http\Requests\UpdateSchoolRequest;
use App\Libraries\Repositories\SchoolRepository;
use App\Models\CourseSchedItem;
use App\Models\School;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use Dingo\Api\Http\Request;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Session;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
class SchoolController extends AppBaseController
{

	/** @var  SchoolRepository */
	private $schoolRepository;

	function __construct(SchoolRepository $schoolRepo)
	{
		$this->schoolRepository = $schoolRepo;
	}

	/**
	 * Display the course session generator form of the School.
	 *
	 * @return Response
	 */
	public function getFormSearchGenerator()
	{
		$user_id = Auth::user()->id;
		$user_lang = app()->getLocale();



		$items['schools'] = School::join('school_employee','school_employee.school_id','=','school.id')
			->where('school_employee.rea_user_id','=',$user_id)
			->lists('school.school_name_'.$user_lang,'school.id','school.id')->toArray();
		return $items;
	}

	public function getSearchGenerator()
	{

		$items = $this->getFormSearchGenerator();
		extract($items);
		return view('schools.generator_search')->with(compact('schools','school_years','school_levels','school_level_class'));
	}


	public function getSearchGeneratorFrontTpl()
	{
		$items = $this->getFormSearchGenerator();
		extract($items);
		return view('front.schools.generator_search')->with(compact('schools'));
	}

	/**
	 * Display the course session generator form of the School.
	 *
	 * @return Response
	 */
	public function postSearchGenerator()
	{
		$elements = $this->getFormSearchGenerator();
		extract($elements);
		$input = Input::all();
		extract($input);
		$symbol = SchoolClass::select('symbol')->where('id','=',$symbol_id)->value('symbol');

		$items = CourseSchedItem::select('wday_id','session_order','session_start_time','session_end_time','course_id')
									->where('school_year_id','=',$school_year_id)
									->where('level_class_id','=',$level_class_id)
									->where('symbol','=',$symbol)
									->get()->toArray();
		if($items){
			foreach($items as $item){
				$k = $item['wday_id'];
				$v = $item['session_order'];
				unset($item['wday_id']) ;
				unset($item['session_order']) ;
				$days_courses[$v][$k] = $item;
			}
			return view('schools.generator_search')->with(compact('days_courses','schools','school_years','school_levels','school_level_class'));
		}else{
			$new = true;
			return view('schools.generator_search')->with(compact('days_courses','schools','school_years','school_levels','school_level_class','new'));
		}
	}



}
