<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateLevelClassRequest;
use App\Http\Requests\UpdateLevelClassRequest;
use App\Libraries\Repositories\LevelClassRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class LevelClassController extends AppBaseController
{

	/** @var  LevelClassRepository */
	private $levelClassRepository;

	function __construct(LevelClassRepository $levelClassRepo)
	{
		$this->levelClassRepository = $levelClassRepo;
	}

	/**
	 * Display a listing of the LevelClass.
	 *
	 * @return Response
	 */
	public function index()
	{
		$levelClasses = $this->levelClassRepository->paginate(10);

		return view('levelClasses.index')
			->with('levelClasses', $levelClasses);
	}

	/**
	 * Show the form for creating a new LevelClass.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('levelClasses.create');
	}

	/**
	 * Store a newly created LevelClass in storage.
	 *
	 * @param CreateLevelClassRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateLevelClassRequest $request)
	{
		$input = $request->all();

		$levelClass = $this->levelClassRepository->create($input);

		Flash::success('LevelClass saved successfully.');

		return redirect(route('levelClasses.index'));
	}

	/**
	 * Display the specified LevelClass.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$levelClass = $this->levelClassRepository->find($id);

		if(empty($levelClass))
		{
			Flash::error('LevelClass not found');

			return redirect(route('levelClasses.index'));
		}

		return view('levelClasses.show')->with('levelClass', $levelClass);
	}

	/**
	 * Show the form for editing the specified LevelClass.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$levelClass = $this->levelClassRepository->find($id);

		if(empty($levelClass))
		{
			Flash::error('LevelClass not found');

			return redirect(route('levelClasses.index'));
		}

		return view('levelClasses.edit')->with('levelClass', $levelClass);
	}

	/**
	 * Update the specified LevelClass in storage.
	 *
	 * @param  int              $id
	 * @param UpdateLevelClassRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateLevelClassRequest $request)
	{
		$levelClass = $this->levelClassRepository->find($id);

		if(empty($levelClass))
		{
			Flash::error('LevelClass not found');

			return redirect(route('levelClasses.index'));
		}

		$this->levelClassRepository->updateRich($request->all(), $id);

		Flash::success('LevelClass updated successfully.');

		return redirect(route('levelClasses.index'));
	}

	/**
	 * Remove the specified LevelClass from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$levelClass = $this->levelClassRepository->find($id);

		if(empty($levelClass))
		{
			Flash::error('LevelClass not found');

			return redirect(route('levelClasses.index'));
		}

		$this->levelClassRepository->delete($id);

		Flash::success('LevelClass deleted successfully.');

		return redirect(route('levelClasses.index'));
	}
}
