<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateAttendanceTypeRequest;
use App\Http\Requests\UpdateAttendanceTypeRequest;
use App\Libraries\Repositories\AttendanceTypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class AttendanceTypeController extends AppBaseController
{

	/** @var  AttendanceTypeRepository */
	private $attendanceTypeRepository;

	function __construct(AttendanceTypeRepository $attendanceTypeRepo)
	{
		$this->attendanceTypeRepository = $attendanceTypeRepo;
	}

	/**
	 * Display a listing of the AttendanceType.
	 *
	 * @return Response
	 */
	public function index()
	{
		$attendanceTypes = $this->attendanceTypeRepository->paginate(10);

		return view('attendanceTypes.index')
			->with('attendanceTypes', $attendanceTypes);
	}

	/**
	 * Show the form for creating a new AttendanceType.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('attendanceTypes.create');
	}

	/**
	 * Store a newly created AttendanceType in storage.
	 *
	 * @param CreateAttendanceTypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateAttendanceTypeRequest $request)
	{
		$input = $request->all();

		$attendanceType = $this->attendanceTypeRepository->create($input);

		Flash::success('AttendanceType saved successfully.');

		return redirect(route('attendanceTypes.index'));
	}

	/**
	 * Display the specified AttendanceType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$attendanceType = $this->attendanceTypeRepository->find($id);

		if(empty($attendanceType))
		{
			Flash::error('AttendanceType not found');

			return redirect(route('attendanceTypes.index'));
		}

		return view('attendanceTypes.show')->with('attendanceType', $attendanceType);
	}

	/**
	 * Show the form for editing the specified AttendanceType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$attendanceType = $this->attendanceTypeRepository->find($id);

		if(empty($attendanceType))
		{
			Flash::error('AttendanceType not found');

			return redirect(route('attendanceTypes.index'));
		}

		return view('attendanceTypes.edit')->with('attendanceType', $attendanceType);
	}

	/**
	 * Update the specified AttendanceType in storage.
	 *
	 * @param  int              $id
	 * @param UpdateAttendanceTypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateAttendanceTypeRequest $request)
	{
		$attendanceType = $this->attendanceTypeRepository->find($id);

		if(empty($attendanceType))
		{
			Flash::error('AttendanceType not found');

			return redirect(route('attendanceTypes.index'));
		}

		$this->attendanceTypeRepository->updateRich($request->all(), $id);

		Flash::success('AttendanceType updated successfully.');

		return redirect(route('attendanceTypes.index'));
	}

	/**
	 * Remove the specified AttendanceType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$attendanceType = $this->attendanceTypeRepository->find($id);

		if(empty($attendanceType))
		{
			Flash::error('AttendanceType not found');

			return redirect(route('attendanceTypes.index'));
		}

		$this->attendanceTypeRepository->delete($id);

		Flash::success('AttendanceType deleted successfully.');

		return redirect(route('attendanceTypes.index'));
	}
}
