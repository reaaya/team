<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSchoolTermRequest;
use App\Http\Requests\UpdateSchoolTermRequest;
use App\Libraries\Repositories\SchoolTermRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolTermController extends AppBaseController
{

	/** @var  SchoolTermRepository */
	private $schoolTermRepository;

	function __construct(SchoolTermRepository $schoolTermRepo)
	{
		$this->schoolTermRepository = $schoolTermRepo;
	}

	/**
	 * Display a listing of the SchoolTerm.
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolTerms = $this->schoolTermRepository->paginate(10);

		return view('schoolTerms.index')
			->with('schoolTerms', $schoolTerms);
	}

	/**
	 * Show the form for creating a new SchoolTerm.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schoolTerms.create');
	}

	/**
	 * Store a newly created SchoolTerm in storage.
	 *
	 * @param CreateSchoolTermRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSchoolTermRequest $request)
	{
		$input = $request->all();

		$schoolTerm = $this->schoolTermRepository->create($input);

		Flash::success('SchoolTerm saved successfully.');

		return redirect(route('schoolTerms.index'));
	}

	/**
	 * Display the specified SchoolTerm.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolTerm = $this->schoolTermRepository->find($id);

		if(empty($schoolTerm))
		{
			Flash::error('SchoolTerm not found');

			return redirect(route('schoolTerms.index'));
		}

		return view('schoolTerms.show')->with('schoolTerm', $schoolTerm);
	}

	/**
	 * Show the form for editing the specified SchoolTerm.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$schoolTerm = $this->schoolTermRepository->find($id);

		if(empty($schoolTerm))
		{
			Flash::error('SchoolTerm not found');

			return redirect(route('schoolTerms.index'));
		}

		return view('schoolTerms.edit')->with('schoolTerm', $schoolTerm);
	}

	/**
	 * Update the specified SchoolTerm in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSchoolTermRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSchoolTermRequest $request)
	{
		$schoolTerm = $this->schoolTermRepository->find($id);

		if(empty($schoolTerm))
		{
			Flash::error('SchoolTerm not found');

			return redirect(route('schoolTerms.index'));
		}

		$this->schoolTermRepository->updateRich($request->all(), $id);

		Flash::success('SchoolTerm updated successfully.');

		return redirect(route('schoolTerms.index'));
	}

	/**
	 * Remove the specified SchoolTerm from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$schoolTerm = $this->schoolTermRepository->find($id);

		if(empty($schoolTerm))
		{
			Flash::error('SchoolTerm not found');

			return redirect(route('schoolTerms.index'));
		}

		$this->schoolTermRepository->delete($id);

		Flash::success('SchoolTerm deleted successfully.');

		return redirect(route('schoolTerms.index'));
	}
}
