<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateExamSessionRequest;
use App\Http\Requests\UpdateExamSessionRequest;
use App\Libraries\Repositories\ExamSessionRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ExamSessionController extends AppBaseController
{

	/** @var  ExamSessionRepository */
	private $examSessionRepository;

	function __construct(ExamSessionRepository $examSessionRepo)
	{
		$this->examSessionRepository = $examSessionRepo;
	}

	/**
	 * Display a listing of the ExamSession.
	 *
	 * @return Response
	 */
	public function index()
	{
		$examSessions = $this->examSessionRepository->paginate(10);

		return view('examSessions.index')
			->with('examSessions', $examSessions);
	}

	/**
	 * Show the form for creating a new ExamSession.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('examSessions.create');
	}

	/**
	 * Store a newly created ExamSession in storage.
	 *
	 * @param CreateExamSessionRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateExamSessionRequest $request)
	{
		$input = $request->all();

		$examSession = $this->examSessionRepository->create($input);

		Flash::success('ExamSession saved successfully.');

		return redirect(route('examSessions.index'));
	}

	/**
	 * Display the specified ExamSession.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$examSession = $this->examSessionRepository->find($id);

		if(empty($examSession))
		{
			Flash::error('ExamSession not found');

			return redirect(route('examSessions.index'));
		}

		return view('examSessions.show')->with('examSession', $examSession);
	}

	/**
	 * Show the form for editing the specified ExamSession.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$examSession = $this->examSessionRepository->find($id);

		if(empty($examSession))
		{
			Flash::error('ExamSession not found');

			return redirect(route('examSessions.index'));
		}

		return view('examSessions.edit')->with('examSession', $examSession);
	}

	/**
	 * Update the specified ExamSession in storage.
	 *
	 * @param  int              $id
	 * @param UpdateExamSessionRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateExamSessionRequest $request)
	{
		$examSession = $this->examSessionRepository->find($id);

		if(empty($examSession))
		{
			Flash::error('ExamSession not found');

			return redirect(route('examSessions.index'));
		}

		$this->examSessionRepository->updateRich($request->all(), $id);

		Flash::success('ExamSession updated successfully.');

		return redirect(route('examSessions.index'));
	}

	/**
	 * Remove the specified ExamSession from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$examSession = $this->examSessionRepository->find($id);

		if(empty($examSession))
		{
			Flash::error('ExamSession not found');

			return redirect(route('examSessions.index'));
		}

		$this->examSessionRepository->delete($id);

		Flash::success('ExamSession deleted successfully.');

		return redirect(route('examSessions.index'));
	}
}
