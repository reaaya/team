<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateLanguageRequest;
use App\Http\Requests\UpdateLanguageRequest;
use App\Libraries\Repositories\LanguageRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class LanguageController extends AppBaseController
{

	/** @var  LanguageRepository */
	private $languageRepository;

	function __construct(LanguageRepository $languageRepo)
	{
		$this->languageRepository = $languageRepo;
	}

	/**
	 * Display a listing of the Language.
	 *
	 * @return Response
	 */
	public function index()
	{
		$languages = $this->languageRepository->paginate(10);

		return view('languages.index')
			->with('languages', $languages);
	}

	/**
	 * Show the form for creating a new Language.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('languages.create');
	}

	/**
	 * Store a newly created Language in storage.
	 *
	 * @param CreateLanguageRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateLanguageRequest $request)
	{
		$input = $request->all();

		$language = $this->languageRepository->create($input);

		Flash::success('Language saved successfully.');

		return redirect(route('languages.index'));
	}

	/**
	 * Display the specified Language.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$language = $this->languageRepository->find($id);

		if(empty($language))
		{
			Flash::error('Language not found');

			return redirect(route('languages.index'));
		}

		return view('languages.show')->with('language', $language);
	}

	/**
	 * Show the form for editing the specified Language.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$language = $this->languageRepository->find($id);

		if(empty($language))
		{
			Flash::error('Language not found');

			return redirect(route('languages.index'));
		}

		return view('languages.edit')->with('language', $language);
	}

	/**
	 * Update the specified Language in storage.
	 *
	 * @param  int              $id
	 * @param UpdateLanguageRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateLanguageRequest $request)
	{
		$language = $this->languageRepository->find($id);

		if(empty($language))
		{
			Flash::error('Language not found');

			return redirect(route('languages.index'));
		}

		$this->languageRepository->updateRich($request->all(), $id);

		Flash::success('Language updated successfully.');

		return redirect(route('languages.index'));
	}

	/**
	 * Remove the specified Language from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$language = $this->languageRepository->find($id);

		if(empty($language))
		{
			Flash::error('Language not found');

			return redirect(route('languages.index'));
		}

		$this->languageRepository->delete($id);

		Flash::success('Language deleted successfully.');

		return redirect(route('languages.index'));
	}
}
