<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSchoolTypeRequest;
use App\Http\Requests\UpdateSchoolTypeRequest;
use App\Libraries\Repositories\SchoolTypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolTypeController extends AppBaseController
{

	/** @var  SchoolTypeRepository */
	private $schoolTypeRepository;

	function __construct(SchoolTypeRepository $schoolTypeRepo)
	{
		$this->schoolTypeRepository = $schoolTypeRepo;
	}

	/**
	 * Display a listing of the SchoolType.
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolTypes = $this->schoolTypeRepository->paginate(10);

		return view('schoolTypes.index')
			->with('schoolTypes', $schoolTypes);
	}

	/**
	 * Show the form for creating a new SchoolType.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schoolTypes.create');
	}

	/**
	 * Store a newly created SchoolType in storage.
	 *
	 * @param CreateSchoolTypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSchoolTypeRequest $request)
	{
		$input = $request->all();

		$schoolType = $this->schoolTypeRepository->create($input);

		Flash::success('SchoolType saved successfully.');

		return redirect(route('schoolTypes.index'));
	}

	/**
	 * Display the specified SchoolType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolType = $this->schoolTypeRepository->find($id);

		if(empty($schoolType))
		{
			Flash::error('SchoolType not found');

			return redirect(route('schoolTypes.index'));
		}

		return view('schoolTypes.show')->with('schoolType', $schoolType);
	}

	/**
	 * Show the form for editing the specified SchoolType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$schoolType = $this->schoolTypeRepository->find($id);

		if(empty($schoolType))
		{
			Flash::error('SchoolType not found');

			return redirect(route('schoolTypes.index'));
		}

		return view('schoolTypes.edit')->with('schoolType', $schoolType);
	}

	/**
	 * Update the specified SchoolType in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSchoolTypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSchoolTypeRequest $request)
	{
		$schoolType = $this->schoolTypeRepository->find($id);

		if(empty($schoolType))
		{
			Flash::error('SchoolType not found');

			return redirect(route('schoolTypes.index'));
		}

		$this->schoolTypeRepository->updateRich($request->all(), $id);

		Flash::success('SchoolType updated successfully.');

		return redirect(route('schoolTypes.index'));
	}

	/**
	 * Remove the specified SchoolType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$schoolType = $this->schoolTypeRepository->find($id);

		if(empty($schoolType))
		{
			Flash::error('SchoolType not found');

			return redirect(route('schoolTypes.index'));
		}

		$this->schoolTypeRepository->delete($id);

		Flash::success('SchoolType deleted successfully.');

		return redirect(route('schoolTypes.index'));
	}
}
