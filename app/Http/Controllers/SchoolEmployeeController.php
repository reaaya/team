<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSchoolEmployeeRequest;
use App\Http\Requests\UpdateSchoolEmployeeRequest;
use App\Libraries\Repositories\SchoolEmployeeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolEmployeeController extends AppBaseController
{

	/** @var  SchoolEmployeeRepository */
	private $schoolEmployeeRepository;

	function __construct(SchoolEmployeeRepository $schoolEmployeeRepo)
	{
		$this->schoolEmployeeRepository = $schoolEmployeeRepo;
	}

	/**
	 * Display a listing of the SchoolEmployee.
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolEmployees = $this->schoolEmployeeRepository->paginate(10);

		return view('schoolEmployees.index')
			->with('schoolEmployees', $schoolEmployees);
	}

	/**
	 * Show the form for creating a new SchoolEmployee.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schoolEmployees.create');
	}

	/**
	 * Store a newly created SchoolEmployee in storage.
	 *
	 * @param CreateSchoolEmployeeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSchoolEmployeeRequest $request)
	{
		$input = $request->all();

		$schoolEmployee = $this->schoolEmployeeRepository->create($input);

		Flash::success('SchoolEmployee saved successfully.');

		return redirect(route('schoolEmployees.index'));
	}

	/**
	 * Display the specified SchoolEmployee.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolEmployee = $this->schoolEmployeeRepository->find($id);

		if(empty($schoolEmployee))
		{
			Flash::error('SchoolEmployee not found');

			return redirect(route('schoolEmployees.index'));
		}

		return view('schoolEmployees.show')->with('schoolEmployee', $schoolEmployee);
	}

	/**
	 * Show the form for editing the specified SchoolEmployee.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$schoolEmployee = $this->schoolEmployeeRepository->find($id);

		if(empty($schoolEmployee))
		{
			Flash::error('SchoolEmployee not found');

			return redirect(route('schoolEmployees.index'));
		}

		return view('schoolEmployees.edit')->with('schoolEmployee', $schoolEmployee);
	}

	/**
	 * Update the specified SchoolEmployee in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSchoolEmployeeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSchoolEmployeeRequest $request)
	{
		$schoolEmployee = $this->schoolEmployeeRepository->find($id);

		if(empty($schoolEmployee))
		{
			Flash::error('SchoolEmployee not found');

			return redirect(route('schoolEmployees.index'));
		}

		$this->schoolEmployeeRepository->updateRich($request->all(), $id);

		Flash::success('SchoolEmployee updated successfully.');

		return redirect(route('schoolEmployees.index'));
	}

	/**
	 * Remove the specified SchoolEmployee from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$schoolEmployee = $this->schoolEmployeeRepository->find($id);

		if(empty($schoolEmployee))
		{
			Flash::error('SchoolEmployee not found');

			return redirect(route('schoolEmployees.index'));
		}

		$this->schoolEmployeeRepository->delete($id);

		Flash::success('SchoolEmployee deleted successfully.');

		return redirect(route('schoolEmployees.index'));
	}
}
