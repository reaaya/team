<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateAttendanceRequest;
use App\Http\Requests\UpdateAttendanceRequest;
use App\Libraries\Repositories\AttendanceRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class AttendanceController extends AppBaseController
{

	/** @var  AttendanceRepository */
	private $attendanceRepository;

	function __construct(AttendanceRepository $attendanceRepo)
	{
		$this->attendanceRepository = $attendanceRepo;
	}

	/**
	 * Display a listing of the Attendance.
	 *
	 * @return Response
	 */
	public function index()
	{
		$attendances = $this->attendanceRepository->paginate(10);

		return view('attendances.index')
			->with('attendances', $attendances);
	}

	/**
	 * Show the form for creating a new Attendance.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('attendances.create');
	}

	/**
	 * Store a newly created Attendance in storage.
	 *
	 * @param CreateAttendanceRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateAttendanceRequest $request)
	{
		$input = $request->all();

		$attendance = $this->attendanceRepository->create($input);

		Flash::success('Attendance saved successfully.');

		return redirect(route('attendances.index'));
	}

	/**
	 * Display the specified Attendance.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$attendance = $this->attendanceRepository->find($id);

		if(empty($attendance))
		{
			Flash::error('Attendance not found');

			return redirect(route('attendances.index'));
		}

		return view('attendances.show')->with('attendance', $attendance);
	}

	/**
	 * Show the form for editing the specified Attendance.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$attendance = $this->attendanceRepository->find($id);

		if(empty($attendance))
		{
			Flash::error('Attendance not found');

			return redirect(route('attendances.index'));
		}

		return view('attendances.edit')->with('attendance', $attendance);
	}

	/**
	 * Update the specified Attendance in storage.
	 *
	 * @param  int              $id
	 * @param UpdateAttendanceRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateAttendanceRequest $request)
	{
		$attendance = $this->attendanceRepository->find($id);

		if(empty($attendance))
		{
			Flash::error('Attendance not found');

			return redirect(route('attendances.index'));
		}

		$this->attendanceRepository->updateRich($request->all(), $id);

		Flash::success('Attendance updated successfully.');

		return redirect(route('attendances.index'));
	}

	/**
	 * Remove the specified Attendance from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$attendance = $this->attendanceRepository->find($id);

		if(empty($attendance))
		{
			Flash::error('Attendance not found');

			return redirect(route('attendances.index'));
		}

		$this->attendanceRepository->delete($id);

		Flash::success('Attendance deleted successfully.');

		return redirect(route('attendances.index'));
	}
}
