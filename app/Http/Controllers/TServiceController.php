<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTServiceRequest;
use App\Http\Requests\UpdateTServiceRequest;
use App\Libraries\Repositories\TServiceRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TServiceController extends AppBaseController
{

	/** @var  TServiceRepository */
	private $tServiceRepository;

	function __construct(TServiceRepository $tServiceRepo)
	{
		$this->tServiceRepository = $tServiceRepo;
	}

	/**
	 * Display a listing of the TService.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tServices = $this->tServiceRepository->paginate(10);

		return view('tServices.index')
			->with('tServices', $tServices);
	}

	/**
	 * Show the form for creating a new TService.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('tServices.create');
	}

	/**
	 * Store a newly created TService in storage.
	 *
	 * @param CreateTServiceRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTServiceRequest $request)
	{
		$input = $request->all();

		$tService = $this->tServiceRepository->create($input);

		Flash::success('TService saved successfully.');

		return redirect(route('tServices.index'));
	}

	/**
	 * Display the specified TService.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tService = $this->tServiceRepository->find($id);

		if(empty($tService))
		{
			Flash::error('TService not found');

			return redirect(route('tServices.index'));
		}

		return view('tServices.show')->with('tService', $tService);
	}

	/**
	 * Show the form for editing the specified TService.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$tService = $this->tServiceRepository->find($id);

		if(empty($tService))
		{
			Flash::error('TService not found');

			return redirect(route('tServices.index'));
		}

		return view('tServices.edit')->with('tService', $tService);
	}

	/**
	 * Update the specified TService in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTServiceRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTServiceRequest $request)
	{
		$tService = $this->tServiceRepository->find($id);

		if(empty($tService))
		{
			Flash::error('TService not found');

			return redirect(route('tServices.index'));
		}

		$this->tServiceRepository->updateRich($request->all(), $id);

		Flash::success('TService updated successfully.');

		return redirect(route('tServices.index'));
	}

	/**
	 * Remove the specified TService from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$tService = $this->tServiceRepository->find($id);

		if(empty($tService))
		{
			Flash::error('TService not found');

			return redirect(route('tServices.index'));
		}

		$this->tServiceRepository->delete($id);

		Flash::success('TService deleted successfully.');

		return redirect(route('tServices.index'));
	}
}
