<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSchoolLevelRequest;
use App\Http\Requests\UpdateSchoolLevelRequest;
use App\Libraries\Repositories\SchoolLevelRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolLevelController extends AppBaseController
{

	/** @var  SchoolLevelRepository */
	private $schoolLevelRepository;

	function __construct(SchoolLevelRepository $schoolLevelRepo)
	{
		$this->schoolLevelRepository = $schoolLevelRepo;
	}

	/**
	 * Display a listing of the SchoolLevel.
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolLevels = $this->schoolLevelRepository->paginate(10);

		return view('schoolLevels.index')
			->with('schoolLevels', $schoolLevels);
	}

	/**
	 * Show the form for creating a new SchoolLevel.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schoolLevels.create');
	}

	/**
	 * Store a newly created SchoolLevel in storage.
	 *
	 * @param CreateSchoolLevelRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSchoolLevelRequest $request)
	{
		$input = $request->all();

		$schoolLevel = $this->schoolLevelRepository->create($input);

		Flash::success('SchoolLevel saved successfully.');

		return redirect(route('schoolLevels.index'));
	}

	/**
	 * Display the specified SchoolLevel.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolLevel = $this->schoolLevelRepository->find($id);

		if(empty($schoolLevel))
		{
			Flash::error('SchoolLevel not found');

			return redirect(route('schoolLevels.index'));
		}

		return view('schoolLevels.show')->with('schoolLevel', $schoolLevel);
	}

	/**
	 * Show the form for editing the specified SchoolLevel.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$schoolLevel = $this->schoolLevelRepository->find($id);

		if(empty($schoolLevel))
		{
			Flash::error('SchoolLevel not found');

			return redirect(route('schoolLevels.index'));
		}

		return view('schoolLevels.edit')->with('schoolLevel', $schoolLevel);
	}

	/**
	 * Update the specified SchoolLevel in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSchoolLevelRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSchoolLevelRequest $request)
	{
		$schoolLevel = $this->schoolLevelRepository->find($id);

		if(empty($schoolLevel))
		{
			Flash::error('SchoolLevel not found');

			return redirect(route('schoolLevels.index'));
		}

		$this->schoolLevelRepository->updateRich($request->all(), $id);

		Flash::success('SchoolLevel updated successfully.');

		return redirect(route('schoolLevels.index'));
	}

	/**
	 * Remove the specified SchoolLevel from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$schoolLevel = $this->schoolLevelRepository->find($id);

		if(empty($schoolLevel))
		{
			Flash::error('SchoolLevel not found');

			return redirect(route('schoolLevels.index'));
		}

		$this->schoolLevelRepository->delete($id);

		Flash::success('SchoolLevel deleted successfully.');

		return redirect(route('schoolLevels.index'));
	}
}
