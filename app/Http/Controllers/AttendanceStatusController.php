<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateAttendanceStatusRequest;
use App\Http\Requests\UpdateAttendanceStatusRequest;
use App\Libraries\Repositories\AttendanceStatusRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class AttendanceStatusController extends AppBaseController
{

	/** @var  AttendanceStatusRepository */
	private $attendanceStatusRepository;

	function __construct(AttendanceStatusRepository $attendanceStatusRepo)
	{
		$this->attendanceStatusRepository = $attendanceStatusRepo;
	}

	/**
	 * Display a listing of the AttendanceStatus.
	 *
	 * @return Response
	 */
	public function index()
	{
		$attendanceStatuses = $this->attendanceStatusRepository->paginate(10);

		return view('attendanceStatuses.index')
			->with('attendanceStatuses', $attendanceStatuses);
	}

	/**
	 * Show the form for creating a new AttendanceStatus.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('attendanceStatuses.create');
	}

	/**
	 * Store a newly created AttendanceStatus in storage.
	 *
	 * @param CreateAttendanceStatusRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateAttendanceStatusRequest $request)
	{
		$input = $request->all();

		$attendanceStatus = $this->attendanceStatusRepository->create($input);

		Flash::success('AttendanceStatus saved successfully.');

		return redirect(route('attendanceStatuses.index'));
	}

	/**
	 * Display the specified AttendanceStatus.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$attendanceStatus = $this->attendanceStatusRepository->find($id);

		if(empty($attendanceStatus))
		{
			Flash::error('AttendanceStatus not found');

			return redirect(route('attendanceStatuses.index'));
		}

		return view('attendanceStatuses.show')->with('attendanceStatus', $attendanceStatus);
	}

	/**
	 * Show the form for editing the specified AttendanceStatus.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$attendanceStatus = $this->attendanceStatusRepository->find($id);

		if(empty($attendanceStatus))
		{
			Flash::error('AttendanceStatus not found');

			return redirect(route('attendanceStatuses.index'));
		}

		return view('attendanceStatuses.edit')->with('attendanceStatus', $attendanceStatus);
	}

	/**
	 * Update the specified AttendanceStatus in storage.
	 *
	 * @param  int              $id
	 * @param UpdateAttendanceStatusRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateAttendanceStatusRequest $request)
	{
		$attendanceStatus = $this->attendanceStatusRepository->find($id);

		if(empty($attendanceStatus))
		{
			Flash::error('AttendanceStatus not found');

			return redirect(route('attendanceStatuses.index'));
		}

		$this->attendanceStatusRepository->updateRich($request->all(), $id);

		Flash::success('AttendanceStatus updated successfully.');

		return redirect(route('attendanceStatuses.index'));
	}

	/**
	 * Remove the specified AttendanceStatus from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$attendanceStatus = $this->attendanceStatusRepository->find($id);

		if(empty($attendanceStatus))
		{
			Flash::error('AttendanceStatus not found');

			return redirect(route('attendanceStatuses.index'));
		}

		$this->attendanceStatusRepository->delete($id);

		Flash::success('AttendanceStatus deleted successfully.');

		return redirect(route('attendanceStatuses.index'));
	}
}
