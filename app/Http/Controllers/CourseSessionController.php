<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCourseSessionRequest;
use App\Http\Requests\UpdateCourseSessionRequest;
use App\Libraries\Repositories\CourseSessionRepository;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CourseSessionController extends AppBaseController
{

	/** @var  CourseSessionRepository */
	private $courseSessionRepository;

	function __construct(CourseSessionRepository $courseSessionRepo)
	{
		$this->courseSessionRepository = $courseSessionRepo;
	}



	/**
	 * Created By Achraf: 21/03/2016
	 * Get Angular Template Front for timeline course session
	 * Get
	 *
	 * @return View
	 */
	public function getTimelineFrontTpl()
	{
		return view('front.coursesession.timeline');
	}

}
