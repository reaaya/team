<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSchoolYearRequest;
use App\Http\Requests\UpdateSchoolYearRequest;
use App\Libraries\Repositories\SchoolYearRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolYearController extends AppBaseController
{

	/** @var  SchoolYearRepository */
	private $schoolYearRepository;

	function __construct(SchoolYearRepository $schoolYearRepo)
	{
		$this->schoolYearRepository = $schoolYearRepo;
	}

	/**
	 * Display a listing of the SchoolYear.
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolYears = $this->schoolYearRepository->paginate(10);

		return view('schoolYears.index')
			->with('schoolYears', $schoolYears);
	}

	/**
	 * Show the form for creating a new SchoolYear.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schoolYears.create');
	}

	/**
	 * Store a newly created SchoolYear in storage.
	 *
	 * @param CreateSchoolYearRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSchoolYearRequest $request)
	{
		$input = $request->all();

		$schoolYear = $this->schoolYearRepository->create($input);

		Flash::success('SchoolYear saved successfully.');

		return redirect(route('schoolYears.index'));
	}

	/**
	 * Display the specified SchoolYear.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolYear = $this->schoolYearRepository->find($id);

		if(empty($schoolYear))
		{
			Flash::error('SchoolYear not found');

			return redirect(route('schoolYears.index'));
		}

		return view('schoolYears.show')->with('schoolYear', $schoolYear);
	}

	/**
	 * Show the form for editing the specified SchoolYear.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$schoolYear = $this->schoolYearRepository->find($id);

		if(empty($schoolYear))
		{
			Flash::error('SchoolYear not found');

			return redirect(route('schoolYears.index'));
		}

		return view('schoolYears.edit')->with('schoolYear', $schoolYear);
	}

	/**
	 * Update the specified SchoolYear in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSchoolYearRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSchoolYearRequest $request)
	{
		$schoolYear = $this->schoolYearRepository->find($id);

		if(empty($schoolYear))
		{
			Flash::error('SchoolYear not found');

			return redirect(route('schoolYears.index'));
		}

		$this->schoolYearRepository->updateRich($request->all(), $id);

		Flash::success('SchoolYear updated successfully.');

		return redirect(route('schoolYears.index'));
	}

	/**
	 * Remove the specified SchoolYear from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$schoolYear = $this->schoolYearRepository->find($id);

		if(empty($schoolYear))
		{
			Flash::error('SchoolYear not found');

			return redirect(route('schoolYears.index'));
		}

		$this->schoolYearRepository->delete($id);

		Flash::success('SchoolYear deleted successfully.');

		return redirect(route('schoolYears.index'));
	}
}
