<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateIdnTypeRequest;
use App\Http\Requests\UpdateIdnTypeRequest;
use App\Libraries\Repositories\IdnTypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class IdnTypeController extends AppBaseController
{

	/** @var  IdnTypeRepository */
	private $idnTypeRepository;

	function __construct(IdnTypeRepository $idnTypeRepo)
	{
		$this->idnTypeRepository = $idnTypeRepo;
	}

	/**
	 * Display a listing of the IdnType.
	 *
	 * @return Response
	 */
	public function index()
	{
		$idnTypes = $this->idnTypeRepository->paginate(10);

		return view('idnTypes.index')
			->with('idnTypes', $idnTypes);
	}

	/**
	 * Show the form for creating a new IdnType.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('idnTypes.create');
	}

	/**
	 * Store a newly created IdnType in storage.
	 *
	 * @param CreateIdnTypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateIdnTypeRequest $request)
	{
		$input = $request->all();

		$idnType = $this->idnTypeRepository->create($input);

		Flash::success('IdnType saved successfully.');

		return redirect(route('idnTypes.index'));
	}

	/**
	 * Display the specified IdnType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$idnType = $this->idnTypeRepository->find($id);

		if(empty($idnType))
		{
			Flash::error('IdnType not found');

			return redirect(route('idnTypes.index'));
		}

		return view('idnTypes.show')->with('idnType', $idnType);
	}

	/**
	 * Show the form for editing the specified IdnType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$idnType = $this->idnTypeRepository->find($id);

		if(empty($idnType))
		{
			Flash::error('IdnType not found');

			return redirect(route('idnTypes.index'));
		}

		return view('idnTypes.edit')->with('idnType', $idnType);
	}

	/**
	 * Update the specified IdnType in storage.
	 *
	 * @param  int              $id
	 * @param UpdateIdnTypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateIdnTypeRequest $request)
	{
		$idnType = $this->idnTypeRepository->find($id);

		if(empty($idnType))
		{
			Flash::error('IdnType not found');

			return redirect(route('idnTypes.index'));
		}

		$this->idnTypeRepository->updateRich($request->all(), $id);

		Flash::success('IdnType updated successfully.');

		return redirect(route('idnTypes.index'));
	}

	/**
	 * Remove the specified IdnType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$idnType = $this->idnTypeRepository->find($id);

		if(empty($idnType))
		{
			Flash::error('IdnType not found');

			return redirect(route('idnTypes.index'));
		}

		$this->idnTypeRepository->delete($id);

		Flash::success('IdnType deleted successfully.');

		return redirect(route('idnTypes.index'));
	}
}
