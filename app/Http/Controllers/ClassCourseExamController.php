<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateClassCourseExamRequest;
use App\Http\Requests\UpdateClassCourseExamRequest;
use App\Libraries\Repositories\ClassCourseExamRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ClassCourseExamController extends AppBaseController
{

	/** @var  ClassCourseExamRepository */
	private $classCourseExamRepository;

	function __construct(ClassCourseExamRepository $classCourseExamRepo)
	{
		$this->classCourseExamRepository = $classCourseExamRepo;
	}

	/**
	 * Display a listing of the ClassCourseExam.
	 *
	 * @return Response
	 */
	public function index()
	{
		$classCourseExams = $this->classCourseExamRepository->paginate(10);

		return view('classCourseExams.index')
			->with('classCourseExams', $classCourseExams);
	}

	/**
	 * Show the form for creating a new ClassCourseExam.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('classCourseExams.create');
	}

	/**
	 * Store a newly created ClassCourseExam in storage.
	 *
	 * @param CreateClassCourseExamRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateClassCourseExamRequest $request)
	{
		$input = $request->all();

		$classCourseExam = $this->classCourseExamRepository->create($input);

		Flash::success('ClassCourseExam saved successfully.');

		return redirect(route('classCourseExams.index'));
	}

	/**
	 * Display the specified ClassCourseExam.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$classCourseExam = $this->classCourseExamRepository->find($id);

		if(empty($classCourseExam))
		{
			Flash::error('ClassCourseExam not found');

			return redirect(route('classCourseExams.index'));
		}

		return view('classCourseExams.show')->with('classCourseExam', $classCourseExam);
	}

	/**
	 * Show the form for editing the specified ClassCourseExam.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$classCourseExam = $this->classCourseExamRepository->find($id);

		if(empty($classCourseExam))
		{
			Flash::error('ClassCourseExam not found');

			return redirect(route('classCourseExams.index'));
		}

		return view('classCourseExams.edit')->with('classCourseExam', $classCourseExam);
	}

	/**
	 * Update the specified ClassCourseExam in storage.
	 *
	 * @param  int              $id
	 * @param UpdateClassCourseExamRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateClassCourseExamRequest $request)
	{
		$classCourseExam = $this->classCourseExamRepository->find($id);

		if(empty($classCourseExam))
		{
			Flash::error('ClassCourseExam not found');

			return redirect(route('classCourseExams.index'));
		}

		$this->classCourseExamRepository->updateRich($request->all(), $id);

		Flash::success('ClassCourseExam updated successfully.');

		return redirect(route('classCourseExams.index'));
	}

	/**
	 * Remove the specified ClassCourseExam from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$classCourseExam = $this->classCourseExamRepository->find($id);

		if(empty($classCourseExam))
		{
			Flash::error('ClassCourseExam not found');

			return redirect(route('classCourseExams.index'));
		}

		$this->classCourseExamRepository->delete($id);

		Flash::success('ClassCourseExam deleted successfully.');

		return redirect(route('classCourseExams.index'));
	}
}
