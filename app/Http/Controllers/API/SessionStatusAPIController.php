<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SessionStatusRepository;
use App\Models\SessionStatus;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SessionStatusAPIController extends AppBaseController
{
	/** @var  SessionStatusRepository */
	private $sessionStatusRepository;

	function __construct(SessionStatusRepository $sessionStatusRepo)
	{
		$this->sessionStatusRepository = $sessionStatusRepo;
	}

	/**
	 * Display a listing of the SessionStatus.
	 * GET|HEAD /sessionStatuses
	 *
	 * @return Response
	 */
	public function index()
	{
		$sessionStatuses = $this->sessionStatusRepository->all();

		return $this->sendResponse($sessionStatuses->toArray(), "SessionStatuses retrieved successfully");
	}

	/**
	 * Show the form for creating a new SessionStatus.
	 * GET|HEAD /sessionStatuses/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created SessionStatus in storage.
	 * POST /sessionStatuses
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(SessionStatus::$rules) > 0)
			$this->validateRequestOrFail($request, SessionStatus::$rules);

		$input = $request->all();

		$sessionStatuses = $this->sessionStatusRepository->create($input);

		return $this->sendResponse($sessionStatuses->toArray(), "SessionStatus saved successfully");
	}

	/**
	 * Display the specified SessionStatus.
	 * GET|HEAD /sessionStatuses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$sessionStatus = $this->sessionStatusRepository->apiFindOrFail($id);

		return $this->sendResponse($sessionStatus->toArray(), "SessionStatus retrieved successfully");
	}

	/**
	 * Show the form for editing the specified SessionStatus.
	 * GET|HEAD /sessionStatuses/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified SessionStatus in storage.
	 * PUT/PATCH /sessionStatuses/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var SessionStatus $sessionStatus */
		$sessionStatus = $this->sessionStatusRepository->apiFindOrFail($id);

		$result = $this->sessionStatusRepository->updateRich($input, $id);

		$sessionStatus = $sessionStatus->fresh();

		return $this->sendResponse($sessionStatus->toArray(), "SessionStatus updated successfully");
	}

	/**
	 * Remove the specified SessionStatus from storage.
	 * DELETE /sessionStatuses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->sessionStatusRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "SessionStatus deleted successfully");
	}
}
