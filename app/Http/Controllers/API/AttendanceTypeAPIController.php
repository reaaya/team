<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AttendanceTypeRepository;
use App\Models\AttendanceType;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class AttendanceTypeAPIController extends AppBaseController
{
	/** @var  AttendanceTypeRepository */
	private $attendanceTypeRepository;

	function __construct(AttendanceTypeRepository $attendanceTypeRepo)
	{
		$this->attendanceTypeRepository = $attendanceTypeRepo;
	}

	/**
	 * Display a listing of the AttendanceType.
	 * GET|HEAD /attendanceTypes
	 *
	 * @return Response
	 */
	public function index()
	{
		$attendanceTypes = $this->attendanceTypeRepository->all();

		return $this->sendResponse($attendanceTypes->toArray(), "AttendanceTypes retrieved successfully");
	}

	/**
	 * Show the form for creating a new AttendanceType.
	 * GET|HEAD /attendanceTypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created AttendanceType in storage.
	 * POST /attendanceTypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(AttendanceType::$rules) > 0)
			$this->validateRequestOrFail($request, AttendanceType::$rules);

		$input = $request->all();

		$attendanceTypes = $this->attendanceTypeRepository->create($input);

		return $this->sendResponse($attendanceTypes->toArray(), "AttendanceType saved successfully");
	}

	/**
	 * Display the specified AttendanceType.
	 * GET|HEAD /attendanceTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$attendanceType = $this->attendanceTypeRepository->apiFindOrFail($id);

		return $this->sendResponse($attendanceType->toArray(), "AttendanceType retrieved successfully");
	}

	/**
	 * Show the form for editing the specified AttendanceType.
	 * GET|HEAD /attendanceTypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified AttendanceType in storage.
	 * PUT/PATCH /attendanceTypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var AttendanceType $attendanceType */
		$attendanceType = $this->attendanceTypeRepository->apiFindOrFail($id);

		$result = $this->attendanceTypeRepository->updateRich($input, $id);

		$attendanceType = $attendanceType->fresh();

		return $this->sendResponse($attendanceType->toArray(), "AttendanceType updated successfully");
	}

	/**
	 * Remove the specified AttendanceType from storage.
	 * DELETE /attendanceTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->attendanceTypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "AttendanceType deleted successfully");
	}
}
