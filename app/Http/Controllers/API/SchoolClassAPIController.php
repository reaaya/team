<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolClassRepository;
use App\Models\SchoolClass;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class SchoolClassAPIController extends AppBaseController
{
	/** @var  SchoolClassRepository */
	private $schoolClassRepository;

	function __construct(SchoolClassRepository $schoolClassRepo)
	{
		$this->schoolClassRepository = $schoolClassRepo;
	}

	/**
	 * Display a listing of the SchoolClass.
	 * GET|HEAD /schoolClasses
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolClasses = $this->schoolClassRepository->all();

		return $this->sendResponse($schoolClasses->toArray(), "SchoolClasses retrieved successfully");
	}

	/**
	 * Show the form for creating a new SchoolClass.
	 * GET|HEAD /schoolClasses/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created SchoolClass in storage.
	 * POST /schoolClasses
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(SchoolClass::$rules) > 0)
			$this->validateRequestOrFail($request, SchoolClass::$rules);

		$input = $request->all();

		$schoolClasses = $this->schoolClassRepository->create($input);

		return $this->sendResponse($schoolClasses->toArray(), "SchoolClass saved successfully");
	}

	/**
	 * Display the specified SchoolClass.
	 * GET|HEAD /schoolClasses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolClass = $this->schoolClassRepository->apiFindOrFail($id);

		return $this->sendResponse($schoolClass->toArray(), "SchoolClass retrieved successfully");
	}

	/**
	 * Show the form for editing the specified SchoolClass.
	 * GET|HEAD /schoolClasses/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified SchoolClass in storage.
	 * PUT/PATCH /schoolClasses/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var SchoolClass $schoolClass */
		$schoolClass = $this->schoolClassRepository->apiFindOrFail($id);

		$result = $this->schoolClassRepository->updateRich($input, $id);

		$schoolClass = $schoolClass->fresh();

		return $this->sendResponse($schoolClass->toArray(), "SchoolClass updated successfully");
	}

	/**
	 * Remove the specified SchoolClass from storage.
	 * DELETE /schoolClasses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->schoolClassRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "SchoolClass deleted successfully");
	}



	/**
	*	Created by Achraf
	*	Created at 07/03/2016
	*	Function key: BF264 / Q0009
	*/
	public function getAllSchoolClassBySchoolYearIDAndLevelClassID($school_year_id,$level_class_id)
	{

		$result = SchoolClass::select('school_class.id','school_class.symbol')
			->where('school_class.active','=',"Y")
			->where('school_class.school_year_id',"=",$school_year_id)
			->where('school_class.level_class_id',"=",$level_class_id)
			->get();

		if(!$result){
			return $this->sendResponse(null, Lang::get('school_year.errors.empty_school_class'), false);	
		}

		return $this->sendResponse($result, Lang::get('school_year.success.valide_school_class'), true);	
	}




}
