<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\EFileRepository;
use App\Models\EFile;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class EFileAPIController extends AppBaseController
{
	/** @var  EFileRepository */
	private $eFileRepository;

	function __construct(EFileRepository $eFileRepo)
	{
		$this->eFileRepository = $eFileRepo;
	}

	/**
	 * Display a listing of the EFile.
	 * GET|HEAD /eFiles
	 *
	 * @return Response
	 */
	public function index()
	{
		$eFiles = $this->eFileRepository->all();

		return $this->sendResponse($eFiles->toArray(), "EFiles retrieved successfully");
	}

	/**
	 * Show the form for creating a new EFile.
	 * GET|HEAD /eFiles/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created EFile in storage.
	 * POST /eFiles
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(EFile::$rules) > 0)
			$this->validateRequestOrFail($request, EFile::$rules);

		$input = $request->all();

		$eFiles = $this->eFileRepository->create($input);

		return $this->sendResponse($eFiles->toArray(), "EFile saved successfully");
	}

	/**
	 * Display the specified EFile.
	 * GET|HEAD /eFiles/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$eFile = $this->eFileRepository->apiFindOrFail($id);

		return $this->sendResponse($eFile->toArray(), "EFile retrieved successfully");
	}

	/**
	 * Show the form for editing the specified EFile.
	 * GET|HEAD /eFiles/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified EFile in storage.
	 * PUT/PATCH /eFiles/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var EFile $eFile */
		$eFile = $this->eFileRepository->apiFindOrFail($id);

		$result = $this->eFileRepository->updateRich($input, $id);

		$eFile = $eFile->fresh();

		return $this->sendResponse($eFile->toArray(), "EFile updated successfully");
	}

	/**
	 * Remove the specified EFile from storage.
	 * DELETE /eFiles/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->eFileRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "EFile deleted successfully");
	}
}
