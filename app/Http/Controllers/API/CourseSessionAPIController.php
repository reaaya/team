<?php namespace App\Http\Controllers\API;

use App\Helpers\Server;
use App\Http\Requests;
use App\Libraries\Repositories\CourseSessionRepository;
use App\Models\CourseSession;
use App\Models\StudentSession;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use App\Helpers\HijriDateHelper;
use Illuminate\Support\Facades\Lang;


class CourseSessionAPIController extends AppBaseController
{
	/** @var  CourseSessionRepository */
	private $courseSessionRepository;
	//private $session_status = config('lookup.session_status');
	private $session_status;

	const REGEXP_DATE = "/^[0-9]{4}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$/";

	function __construct(Request $request,CourseSessionRepository $courseSessionRepo)
	{
		parent::__construct();
        $this->group_num = Server::get('SCHOOLS',$this->user->current_school_id,'group_num');
		$this->school_id = $this->user->current_school_id;
		$this->prof_id = $this->user->id;
		$this->courseSessionRepository = $courseSessionRepo;
		$this->session_status = config('lookup.session_status');
	}

	/**
	 * Display a listing of the CourseSession of one day.
	 * GET|HEAD /courseSessions/timeline/{day}
	 *
	 * @return Response
	 */
	public function timeline($date=null)
	{
        if(!$date)
			$date = date('Ymd');

		$dateValidator = Validator::make(compact('day'),[ 'day' => ['regex:'.static::REGEXP_DATE]]);

		if($dateValidator->passes()){
			$date = HijriDateHelper::to_hijri($date);

			$items = CourseSession::select(
										DB::Raw('course.lookup_code AS course,course_session.level_class_id,course_session.session_hdate, course_session.hmonth'),
										DB::Raw('course_session.hday_num,level_class.lookup_code AS level_class,school_level.school_level_name_'.app()->getLocale().' AS school_level,course_session.symbol'),
										DB::Raw('session_status_id,session_status.lookup_code AS session_status,course_session.session_order'),
										DB::Raw('course_session.year,session_start_time,session_end_time')
									)
									->join('school_employee','school_employee.rea_user_id','=','course_session.prof_id')
									->join('course','course.id','=','course_session.course_id')
									->join('level_class','level_class.id','=','course_session.level_class_id')
									->join('session_status','session_status.id','=','course_session.session_status_id')
									->join('school_level','school_level.id','=','level_class.school_level_id')
									->where('course_session.group_num','=',$this->group_num)
									->where('course_session.school_id','=',$this->school_id)
									->where('course_session.prof_id','=',$this->prof_id)
									->where('school_employee.school_job_mfk','like','%,'.School_Job_Teacher.',%')
									->where('course_session.session_hdate','=',$date)
									->where('course_session.active','=','Y')
									->whereIn('course_session.session_status_id',[Session_status_Coming_session,Session_status_Current_session])
									->orderBy('course_session.session_start_time','asc')
									->get();

			if($items->count() >0)
				return $this->sendResponse($items,null);
			else
				return $this->sendResponse(null,'course_session.any_course_session',false);

		}else{
			return $this->sendResponse(null,Lang::get('course_session.invalid_date'),false);
		}

	}

	/**
	 * Function Key: BF255
	 * Created By Achraf: 03/03/2016
	 * Optimized By Aziz: 18/03/2016
	 *
	 * Cancel Course Session
	 * POST|HEAD /courseSessions/cancel
	 *
	 * @return Response
	 */

	public function cancel(Request $request)
	{
		$session_status_comment = $request->get('session_status_comment',null);
		$request->merge([
			'group_num'=>$this->group_num,
			'school_id'=>$this->school_id,
			'prof_id'=>$this->prof_id,
			'session_status_commment' => $session_status_comment
		]);
		$course_session = $request->all();
		extract($course_session);

		$check = $this->checkIfIsOwnerAndTeacher($course_session);

		$data = [
			'session_status_id'=>Session_status_Canceled_session,
			'session_status_comment'=>$session_status_comment
		];
		$updateCourseSession = CourseSession::updateCourseSession($course_session,$data);
		if($updateCourseSession)
			return $this->sendResponse(null, Lang::get('course_session.messages.cancel_course_session_with_success'));
		else
			return $this->sendResponse(null, Lang::get('course_session.errors.cancel_course_session_failed'),false);
	}

	/**
	 * Function Key: BF245
	 * Created By Achraf: 05/03/2016
	 * Optimized By Aziz: 17/03/2016
	 *
	 * Save presence students array And Open Course Session
	 * POST|HEAD /courseSessions/start
	 *
	 * @return Response
	 */
	public function start(Request $request)
	{

		$attendances = $request->all();
		extract($attendances);
		$course_session +=[
			'group_num'=>$this->group_num,
			'school_id'=>$this->school_id,
			'prof_id'=>$this->prof_id
		];

		/* Check If Teacher is Owner Of Course session */
		$this->checkIfIsOwnerAndTeacher($course_session);

		/* Check If Teacher Dont have CURRENT_COURSE_SESSION_NAME */
		$CURRENT_COURSE_SESSION_NAME = CourseSession::checkStatusCourseSession(Session_status_Current_session,$course_session);
		if($CURRENT_COURSE_SESSION_NAME){
			return $this->sendResponse(null, 'course_session.errors.teacher_have_current_course_session', false);
		}

		/* Check If Teacher have NEXT_COURSE_SESSION_NAME */
		$NEXT_COURSE_SESSION_NAME = CourseSession::checkStatusCourseSession(Session_status_Coming_session,$course_session);
		if(!$NEXT_COURSE_SESSION_NAME){
			return $this->sendResponse(null, 'course_session.errors.teacher_dont_have_next_course_session', false);
		}

		$data = ['session_status_id' => Session_status_Current_session];
		$updateCourseSession = CourseSession::updateCourseSession($course_session,$data);


		$this->updateStudentsSession($course_session,$students_session);
		return $this->sendResponse(null, Lang::get('course_session.success.course_session_is_open'));

	}

	/**
	 * Function Key: BF246
	 * Optimized By Aziz: 20/03/2016
	 *
	 * Close a Course Session
	 * POST|HEAD /courseSessions/close
	 *
	 * @return Response
	 */
	public function close(Request $request)
	{

		$request->merge([
			'group_num'=>$this->group_num,
			'school_id'=>$this->school_id,
			'prof_id'=>$this->prof_id
		]);
		$course_session = $request->all();
		extract($course_session);

		$check = $this->checkIfIsOwnerAndTeacher($course_session);

		$data = [
			'session_status_id'=>Session_status_Closed_session
		];
		$updateCourseSession = CourseSession::updateCourseSession($course_session,$data);
		if($updateCourseSession)
			return $this->sendResponse(null, Lang::get('course_session.messages.close_course_session_with_success'));
		else
			return $this->sendResponse(null, Lang::get('course_session.errors.close_course_session_failed'),false);
	}
	public function saveStudentsSession(Request $request)
	{
		extract($request->all());
		$course_session +=[
			'group_num'=>$this->group_num,
			'school_id'=>$this->school_id,
			'prof_id'=>$this->prof_id
		];
		$update = $this->updateStudentsSession($course_session,$students_session);
		if($update)
			return $this->sendResponse(null, Lang::get('course_session.messages.update_student_session_with_success'));
		else
			return $this->sendResponse(null, Lang::get('course_session.errors.update_student_session_failed'),false);
	}
	public function updateStudentsSession($course_session,$students_session)
	{
		foreach ($students_session as $student_session) {
			if(!isset($student_session['student_id']) OR empty($student_session['student_id'])){
				return $this->sendResponse(null, Lang::get('course_session.errors.please_check_students_data'),null);
			}
			extract($student_session);

			// Save Perfome StudentCourseSessionModel
			$StudentSessionModel = StudentSession::updateStudentSession($course_session,$student_id,$attributes);

		}

		return true;
	}

	public function checkIfIsOwnerAndTeacher($course_session)
	{
		/* Check If Teacher is Owner Of Course session AND Is Next */
		$IsOwnerAndTeacher = CourseSession::checkIfIsOwnerAnTeacher($course_session);
		if(!$IsOwnerAndTeacher){
			return $this->sendResponse(null, 'course_session.errors.invalide_teacher_for_course_session_id', false);
		}
		return true;
	}


}
