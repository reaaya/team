<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AttendanceRepository;
use App\Models\Attendance;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class AttendanceAPIController extends AppBaseController
{
	/** @var  AttendanceRepository */
	private $attendanceRepository;

	function __construct(AttendanceRepository $attendanceRepo)
	{
		$this->attendanceRepository = $attendanceRepo;
	}

	/**
	 * Display a listing of the Attendance.
	 * GET|HEAD /attendances
	 *
	 * @return Response
	 */
	public function index()
	{
		$attendances = $this->attendanceRepository->all();

		return $this->sendResponse($attendances->toArray(), "Attendances retrieved successfully");
	}

	/**
	 * Show the form for creating a new Attendance.
	 * GET|HEAD /attendances/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Attendance in storage.
	 * POST /attendances
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Attendance::$rules) > 0)
			$this->validateRequestOrFail($request, Attendance::$rules);

		$input = $request->all();

		$attendances = $this->attendanceRepository->create($input);

		return $this->sendResponse($attendances->toArray(), "Attendance saved successfully");
	}

	/**
	 * Display the specified Attendance.
	 * GET|HEAD /attendances/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$attendance = $this->attendanceRepository->apiFindOrFail($id);

		return $this->sendResponse($attendance->toArray(), "Attendance retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Attendance.
	 * GET|HEAD /attendances/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Attendance in storage.
	 * PUT/PATCH /attendances/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Attendance $attendance */
		$attendance = $this->attendanceRepository->apiFindOrFail($id);

		$result = $this->attendanceRepository->updateRich($input, $id);

		$attendance = $attendance->fresh();

		return $this->sendResponse($attendance->toArray(), "Attendance updated successfully");
	}

	/**
	 * Remove the specified Attendance from storage.
	 * DELETE /attendances/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->attendanceRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Attendance deleted successfully");
	}
}
