<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolLevelRepository;
use App\Models\SchoolLevel;
use App\Models\SchoolClass;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class SchoolLevelAPIController extends AppBaseController
{
	/** @var  SchoolLevelRepository */
	private $schoolLevelRepository;

	function __construct(SchoolLevelRepository $schoolLevelRepo)
	{
		$this->schoolLevelRepository = $schoolLevelRepo;
	}

	/**
	 * Display a listing of the SchoolLevel.
	 * GET|HEAD /schoolLevels
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolLevels = $this->schoolLevelRepository->all();

		return $this->sendResponse($schoolLevels->toArray(), "SchoolLevels retrieved successfully");
	}

	/**
	 * Show the form for creating a new SchoolLevel.
	 * GET|HEAD /schoolLevels/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created SchoolLevel in storage.
	 * POST /schoolLevels
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(SchoolLevel::$rules) > 0)
			$this->validateRequestOrFail($request, SchoolLevel::$rules);

		$input = $request->all();

		$schoolLevels = $this->schoolLevelRepository->create($input);

		return $this->sendResponse($schoolLevels->toArray(), "SchoolLevel saved successfully");
	}

	/**
	 * Display the specified SchoolLevel.
	 * GET|HEAD /schoolLevels/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolLevel = $this->schoolLevelRepository->apiFindOrFail($id);

		return $this->sendResponse($schoolLevel->toArray(), "SchoolLevel retrieved successfully");
	}

	/**
	 * Show the form for editing the specified SchoolLevel.
	 * GET|HEAD /schoolLevels/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified SchoolLevel in storage.
	 * PUT/PATCH /schoolLevels/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var SchoolLevel $schoolLevel */
		$schoolLevel = $this->schoolLevelRepository->apiFindOrFail($id);

		$result = $this->schoolLevelRepository->updateRich($input, $id);

		$schoolLevel = $schoolLevel->fresh();

		return $this->sendResponse($schoolLevel->toArray(), "SchoolLevel updated successfully");
	}

	/**
	 * Remove the specified SchoolLevel from storage.
	 * DELETE /schoolLevels/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->schoolLevelRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "SchoolLevel deleted successfully");
	}




	/**
	*	Created by Achraf
	*	Created at 07/03/2016
	*	Function key: BF264 / Q0008
	*/
	public function getAllSchoolLevelsBySchoolYearID($school_year_id)
	{
		$user_lang = app()->getLocale();

		$result = SchoolClass::select('level_class.id','level_class.level_class_name_'.$user_lang.' as level_class_name')
		->join('level_class','level_class.id','=','school_class.level_class_id')
			->where('school_class.active','=',"Y")
			->where('school_class.school_year_id',"=",$school_year_id)
			->groupBy('level_class.id')
			->get();

		if(!$result){
			return $this->sendResponse(null, Lang::get('school_year.errors.empty_school_levels'), false);	
		}

		return $this->sendResponse($result, Lang::get('school_year.success.valide_school_levels'), true);	
	}


	

	




}
