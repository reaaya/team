<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolYearRepository;
use App\Models\SchoolYear;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use App\Helpers\HijriDateHelper;

class SchoolYearAPIController extends AppBaseController
{
	/** @var  SchoolYearRepository */
	private $schoolYearRepository;

	function __construct(SchoolYearRepository $schoolYearRepo)
	{
		$this->schoolYearRepository = $schoolYearRepo;
	}

	/**
	 * Display a listing of the SchoolYear.
	 * GET|HEAD /schoolYears
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolYears = $this->schoolYearRepository->all();

		return $this->sendResponse($schoolYears->toArray(), "SchoolYears retrieved successfully");
	}

	/**
	 * Show the form for creating a new SchoolYear.
	 * GET|HEAD /schoolYears/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created SchoolYear in storage.
	 * POST /schoolYears
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(SchoolYear::$rules) > 0)
			$this->validateRequestOrFail($request, SchoolYear::$rules);

		$input = $request->all();

		$schoolYears = $this->schoolYearRepository->create($input);

		return $this->sendResponse($schoolYears->toArray(), "SchoolYear saved successfully");
	}

	/**
	 * Display the specified SchoolYear.
	 * GET|HEAD /schoolYears/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolYear = $this->schoolYearRepository->apiFindOrFail($id);

		return $this->sendResponse($schoolYear->toArray(), "SchoolYear retrieved successfully");
	}

	/**
	 * Show the form for editing the specified SchoolYear.
	 * GET|HEAD /schoolYears/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified SchoolYear in storage.
	 * PUT/PATCH /schoolYears/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var SchoolYear $schoolYear */
		$schoolYear = $this->schoolYearRepository->apiFindOrFail($id);

		$result = $this->schoolYearRepository->updateRich($input, $id);

		$schoolYear = $schoolYear->fresh();

		return $this->sendResponse($schoolYear->toArray(), "SchoolYear updated successfully");
	}

	/**
	 * Remove the specified SchoolYear from storage.
	 * DELETE /schoolYears/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->schoolYearRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "SchoolYear deleted successfully");
	}





	/**
	*	Created by Achraf
	*	Created at 06/03/2016
	*	Function key: BF264 / Q0007
	*/
	public function getAllSchoolYearsBySchoolID($school_id)
	{
		$current_day = HijriDateHelper::to_hijri(date('Y-m-d'));
		$result = SchoolYear::select('school_year.id','school_year.school_year_name')
		->where('school_id',$school_id)
		->where('school_year_end_hdate', '>=', $current_day)
		->get();
		if(!$result){
			return $this->sendResponse(null, Lang::get('school_year.errors.empty_school_years'), false);	
		}

		return $this->sendResponse($result, Lang::get('school_year.success.valide_school_years'), true);	
	}



}




