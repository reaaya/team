<?php

namespace App\Http\Controllers\API\Auth;

use App\Helpers\MfkHelper;
use App\Models\Language;
use App\Models\ReaUser;
use App\Models\School;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Lang;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\Auth\CreateToken;
use Illuminate\Support\Facades\Validator;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use App\Helpers\HijriDateHelper;
class AuthAPIController extends AppBaseController
{

    const PHONE_RE = '/(05)\d{8}/';

    public function __construct()
    {
        $this->middleware('api.auth', ['except' => ['authenticate']]);
    }

    public function me(Request $request)
    {
        return JWTAuth::parseToken()->authenticate();
    }

    public function setCurrentSchoolId($school_id)
    {
        $this->user->current_school_id = $school_id;
        return $this->sendResponse(null,Lang::get('auth.update_current_school_success'));
    }

    public function authenticate(CreateToken $request)
    {

        $lang_short_name = $request->get('lang');
        $identifier = $this->decodeCredentialIdentifier($request->get('login'));

        if(!$identifier) {
            $result['status'] = false;
            $result['lang'] = $lang_short_name;
            $result['message'] = 'login should be email or mobile number';
            return response()->json($result, 400);
        }

        $credentials[$identifier]   = $request->get('login');
        $credentials['password']    = $request->get('password');
        $credentials['sim_card_sn'] = $request->get('sim_card_sn');

        // this will remove any null value on the credentials array.
        $credentials = array_filter($credentials);

        try {
            if(!$token = $this->createToken($credentials)) {
                return response()->json(['message' => 'invalid_credentials','status'=>false], 200);
            }
        } catch (JWTException $e) {
            return response()->json(['message' => 'could_not_create_token', 'status' => false], 500);
        }

        return $this->prepareResponse($token,$request->get('lang'));
    }

    protected function decodeCredentialIdentifier($identifier)
    {
        $emailValidator = Validator::make(compact('identifier'), ['identifier' => 'email']);

        if($emailValidator->passes()) {
            return 'email';
        }

        $phoneValidator = Validator::make(compact('identifier'), ['identifier' => 'regex:'.static::PHONE_RE]);

        if($phoneValidator->passes()) {
            return 'mobile';
        }

        return null;

    }

    protected function createToken($credentials)
    {

        if(array_has($credentials, 'sim_card_sn')){
            $token = $this->createTokenFromSimCardSN($credentials);
        }else{
            $token = $this->createTokenFromUsernamePassword($credentials);
        }
        return $token;
    }

    protected function createTokenFromSimCardSN($credentials)
    {
        try {
            $user = ReaUser::where('sim_card_sn', $credentials['sim_card_sn'])->firstOrFail();
        } catch(ModelNotFoundException $e) {
            return null;
        }

        return JWTAuth::fromUser($user);
    }

    protected function createTokenFromUsernamePassword($credentials)
    {
        return JWTAuth::attempt($credentials);
    }

    protected function prepareResponse($token,$lang_short_name)
    {
        $user = JWTAuth::authenticate($token);
        // IF USER DEFAULT LANGUAGE IS NULL
        if($user->lang_id == 0){
            // FIND THE LANGUAGE DEMANDED IN THE LANGUAGE TABLE
            $lang_demanded = Language::getByLookupCode($lang_short_name);
            // IF LANGUAGE DEMANDED IS NOT EXIST IN LANGUAGE TABLE, RETURN THE DEFAULT LANGUAGE SYSTEM
            if(!$lang_demanded)
                $lang = Language::getByLookupCode(config('app.locale'));
            // IF LANGUAGE DEMAMDED IS EXIST IN LANGUAGE TABLE, RETURN THEM
            else
                $lang = $lang_demanded;

            // UPDATE THE USER DEFAULT LANGUAGE WITH THE DEFAULT SYSTEM LANGUAGE OR THE LANGUAGE DEMANDED BY USER
            ReaUser::updateLang($user->id,$lang['id']);//
        }
        // RETURN THE USER DEFAULT LANGUAGE SHORT NAME
        else{
            $lang = Language::getById($user->lang_id);
        }
        app()->setLocale($lang->lookup_code);
        $items = School::getSchoolsByUserId($user->id,$lang->lookup_code);

        $schools = $items->each(function ($item)
        {
            $item['school_name'] = $item['school_name_'.app()->getLocale()];
            $item['job_title']=MfkHelper::mkfDecode($item['school_job_mfk'],'school_job','lookup_code');
            unset($item['school_job_mfk']);
            unset($item['school_name_'.app()->getLocale()]);
        });

        return [
            'token'     => $token,
            'status'    => true,
            'schools'   => $schools,
            'user'      => $user->firstname.' '.$user->f_firstname.' '.$user->lastname,
            'lang'      => $lang->lookup_code,
            'hdate'=> HijriDateHelper::to_hijri(date('Ymd')),
            'hdate_long'=> HijriDateHelper::to_hijri(date('Ymd'),'hdate_long')
        ];
    }

    public function validateToken()
    {
        // Our routes file should have already authenticated this token, so we just return success here
        return self::response()->array(['status' => true])->statusCode(200);
    }

    public function LinkSimCard(Request $request,$sim_card_sn){
        $user = $this->me($request);
        ReaUser::updateSimCardSerialNumber($user->id,$sim_card_sn);
        return [
            'status' => true
        ];
    }

    public function register(UserRequest $request)
    {
        $newUser = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ];
        $user = User::create($newUser);
        $token = JWTAuth::fromUser($user);

        return response()->json(compact('token'));
    }
}
