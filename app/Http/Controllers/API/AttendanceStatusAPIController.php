<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AttendanceStatusRepository;
use App\Models\AttendanceStatus;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class AttendanceStatusAPIController extends AppBaseController
{
	/** @var  AttendanceStatusRepository */
	private $attendanceStatusRepository;

	function __construct(AttendanceStatusRepository $attendanceStatusRepo)
	{
		$this->attendanceStatusRepository = $attendanceStatusRepo;
	}

	/**
	 * Display a listing of the AttendanceStatus.
	 * GET|HEAD /attendanceStatuses
	 *
	 * @return Response
	 */
	public function index()
	{
		$attendanceStatuses = $this->attendanceStatusRepository->all();

		return $this->sendResponse($attendanceStatuses->toArray(), "AttendanceStatuses retrieved successfully");
	}

	/**
	 * Show the form for creating a new AttendanceStatus.
	 * GET|HEAD /attendanceStatuses/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created AttendanceStatus in storage.
	 * POST /attendanceStatuses
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(AttendanceStatus::$rules) > 0)
			$this->validateRequestOrFail($request, AttendanceStatus::$rules);

		$input = $request->all();

		$attendanceStatuses = $this->attendanceStatusRepository->create($input);

		return $this->sendResponse($attendanceStatuses->toArray(), "AttendanceStatus saved successfully");
	}

	/**
	 * Display the specified AttendanceStatus.
	 * GET|HEAD /attendanceStatuses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$attendanceStatus = $this->attendanceStatusRepository->apiFindOrFail($id);

		return $this->sendResponse($attendanceStatus->toArray(), "AttendanceStatus retrieved successfully");
	}

	/**
	 * Show the form for editing the specified AttendanceStatus.
	 * GET|HEAD /attendanceStatuses/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified AttendanceStatus in storage.
	 * PUT/PATCH /attendanceStatuses/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var AttendanceStatus $attendanceStatus */
		$attendanceStatus = $this->attendanceStatusRepository->apiFindOrFail($id);

		$result = $this->attendanceStatusRepository->updateRich($input, $id);

		$attendanceStatus = $attendanceStatus->fresh();

		return $this->sendResponse($attendanceStatus->toArray(), "AttendanceStatus updated successfully");
	}

	/**
	 * Remove the specified AttendanceStatus from storage.
	 * DELETE /attendanceStatuses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->attendanceStatusRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "AttendanceStatus deleted successfully");
	}
}
