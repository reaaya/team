<?php namespace App\Http\Controllers\API;

use App\Helpers\validateId;
use App\Http\Requests;
use App\Libraries\Repositories\ReaUserRepository;
use App\Models\ReaUser;
use App\Models\Student;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ReaUserAPIController extends AppBaseController
{
	/** @var  ReaUserRepository */
	private $reaUserRepository;

	function __construct(ReaUserRepository $reaUserRepo)
	{
		$this->reaUserRepository = $reaUserRepo;
	}

	/**
	 * Check if user is registred by idn_type and idn.
	 * GET|HEAD /reaUsers
	 *
	 * @return Response
	 */
	public function checkRegistration($idn_type,$idn)
	{
		$check = ReaUser::checkRegistration($idn,$idn_type);
		if($check){
			$data = [
				'students'=> Student::getByResponsibleId($check)
			];
			return $this->sendResponse($data,null);
		}else
			return $this->sendResponse(null,'users.errors.user_not_registred',false);
	}


	/**
	 * Display a listing of the ReaUser.
	 * GET|HEAD /reaUsers
	 *
	 * @return Response
	 */
	public function index()
	{
		$reaUsers = $this->reaUserRepository->all();

		return $this->sendResponse($reaUsers->toArray(), "ReaUsers retrieved successfully");
	}

	/**
	 * Show the form for creating a new ReaUser.
	 * GET|HEAD /reaUsers/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created ReaUser in storage.
	 * POST /reaUsers
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(ReaUser::$rules) > 0)
			$this->validateRequestOrFail($request, ReaUser::$rules);

		$input = $request->all();

		$reaUsers = $this->reaUserRepository->create($input);

		return $this->sendResponse($reaUsers->toArray(), "ReaUser saved successfully");
	}

	/**
	 * Display the specified ReaUser.
	 * GET|HEAD /reaUsers/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$reaUser = $this->reaUserRepository->apiFindOrFail($id);

		return $this->sendResponse($reaUser->toArray(), "ReaUser retrieved successfully");
	}

	/**
	 * Show the form for editing the specified ReaUser.
	 * GET|HEAD /reaUsers/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified ReaUser in storage.
	 * PUT/PATCH /reaUsers/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var ReaUser $reaUser */
		$reaUser = $this->reaUserRepository->apiFindOrFail($id);

		$result = $this->reaUserRepository->updateRich($input, $id);

		$reaUser = $reaUser->fresh();

		return $this->sendResponse($reaUser->toArray(), "ReaUser updated successfully");
	}

	/**
	 * Remove the specified ReaUser from storage.
	 * DELETE /reaUsers/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->reaUserRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "ReaUser deleted successfully");
	}
}
