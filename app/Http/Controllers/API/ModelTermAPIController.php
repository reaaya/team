<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ModelTermRepository;
use App\Models\ModelTerm;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ModelTermAPIController extends AppBaseController
{
	/** @var  ModelTermRepository */
	private $modelTermRepository;

	function __construct(ModelTermRepository $modelTermRepo)
	{
		$this->modelTermRepository = $modelTermRepo;
	}

	/**
	 * Display a listing of the ModelTerm.
	 * GET|HEAD /modelTerms
	 *
	 * @return Response
	 */
	public function index()
	{
		$modelTerms = $this->modelTermRepository->all();

		return $this->sendResponse($modelTerms->toArray(), "ModelTerms retrieved successfully");
	}

	/**
	 * Show the form for creating a new ModelTerm.
	 * GET|HEAD /modelTerms/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created ModelTerm in storage.
	 * POST /modelTerms
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(ModelTerm::$rules) > 0)
			$this->validateRequestOrFail($request, ModelTerm::$rules);

		$input = $request->all();

		$modelTerms = $this->modelTermRepository->create($input);

		return $this->sendResponse($modelTerms->toArray(), "ModelTerm saved successfully");
	}

	/**
	 * Display the specified ModelTerm.
	 * GET|HEAD /modelTerms/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$modelTerm = $this->modelTermRepository->apiFindOrFail($id);

		return $this->sendResponse($modelTerm->toArray(), "ModelTerm retrieved successfully");
	}

	/**
	 * Show the form for editing the specified ModelTerm.
	 * GET|HEAD /modelTerms/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified ModelTerm in storage.
	 * PUT/PATCH /modelTerms/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var ModelTerm $modelTerm */
		$modelTerm = $this->modelTermRepository->apiFindOrFail($id);

		$result = $this->modelTermRepository->updateRich($input, $id);

		$modelTerm = $modelTerm->fresh();

		return $this->sendResponse($modelTerm->toArray(), "ModelTerm updated successfully");
	}

	/**
	 * Remove the specified ModelTerm from storage.
	 * DELETE /modelTerms/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->modelTermRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "ModelTerm deleted successfully");
	}
}
