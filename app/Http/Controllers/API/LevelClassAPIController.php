<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\LevelClassRepository;
use App\Models\LevelClass;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class LevelClassAPIController extends AppBaseController
{
	/** @var  LevelClassRepository */
	private $levelClassRepository;

	function __construct(LevelClassRepository $levelClassRepo)
	{
		$this->levelClassRepository = $levelClassRepo;
	}

	/**
	 * Display a listing of the LevelClass.
	 * GET|HEAD /levelClasses
	 *
	 * @return Response
	 */
	public function index()
	{
		$levelClasses = $this->levelClassRepository->all();

		return $this->sendResponse($levelClasses->toArray(), "LevelClasses retrieved successfully");
	}

	/**
	 * Show the form for creating a new LevelClass.
	 * GET|HEAD /levelClasses/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created LevelClass in storage.
	 * POST /levelClasses
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(LevelClass::$rules) > 0)
			$this->validateRequestOrFail($request, LevelClass::$rules);

		$input = $request->all();

		$levelClasses = $this->levelClassRepository->create($input);

		return $this->sendResponse($levelClasses->toArray(), "LevelClass saved successfully");
	}

	/**
	 * Display the specified LevelClass.
	 * GET|HEAD /levelClasses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$levelClass = $this->levelClassRepository->apiFindOrFail($id);

		return $this->sendResponse($levelClass->toArray(), "LevelClass retrieved successfully");
	}

	/**
	 * Show the form for editing the specified LevelClass.
	 * GET|HEAD /levelClasses/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified LevelClass in storage.
	 * PUT/PATCH /levelClasses/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var LevelClass $levelClass */
		$levelClass = $this->levelClassRepository->apiFindOrFail($id);

		$result = $this->levelClassRepository->updateRich($input, $id);

		$levelClass = $levelClass->fresh();

		return $this->sendResponse($levelClass->toArray(), "LevelClass updated successfully");
	}

	/**
	 * Remove the specified LevelClass from storage.
	 * DELETE /levelClasses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->levelClassRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "LevelClass deleted successfully");
	}
}
