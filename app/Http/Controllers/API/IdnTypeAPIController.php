<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\IdnTypeRepository;
use App\Models\IdnType;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class IdnTypeAPIController extends AppBaseController
{
	/** @var  IdnTypeRepository */
	private $idnTypeRepository;

	function __construct(IdnTypeRepository $idnTypeRepo)
	{
		$this->idnTypeRepository = $idnTypeRepo;
	}

	/**
	 * Display a listing of the IdnType.
	 * GET|HEAD /idnTypes
	 *
	 * @return Response
	 */
	public function index()
	{
		$idnTypes = $this->idnTypeRepository->all();

		return $this->sendResponse($idnTypes->toArray(), "IdnTypes retrieved successfully");
	}

	/**
	 * Show the form for creating a new IdnType.
	 * GET|HEAD /idnTypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created IdnType in storage.
	 * POST /idnTypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(IdnType::$rules) > 0)
			$this->validateRequestOrFail($request, IdnType::$rules);

		$input = $request->all();

		$idnTypes = $this->idnTypeRepository->create($input);

		return $this->sendResponse($idnTypes->toArray(), "IdnType saved successfully");
	}

	/**
	 * Display the specified IdnType.
	 * GET|HEAD /idnTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$idnType = $this->idnTypeRepository->apiFindOrFail($id);

		return $this->sendResponse($idnType->toArray(), "IdnType retrieved successfully");
	}

	/**
	 * Show the form for editing the specified IdnType.
	 * GET|HEAD /idnTypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified IdnType in storage.
	 * PUT/PATCH /idnTypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var IdnType $idnType */
		$idnType = $this->idnTypeRepository->apiFindOrFail($id);

		$result = $this->idnTypeRepository->updateRich($input, $id);

		$idnType = $idnType->fresh();

		return $this->sendResponse($idnType->toArray(), "IdnType updated successfully");
	}

	/**
	 * Remove the specified IdnType from storage.
	 * DELETE /idnTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->idnTypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "IdnType deleted successfully");
	}
}
