<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolTermRepository;
use App\Models\SchoolTerm;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolTermAPIController extends AppBaseController
{
	/** @var  SchoolTermRepository */
	private $schoolTermRepository;

	function __construct(SchoolTermRepository $schoolTermRepo)
	{
		$this->schoolTermRepository = $schoolTermRepo;
	}

	/**
	 * Display a listing of the SchoolTerm.
	 * GET|HEAD /schoolTerms
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolTerms = $this->schoolTermRepository->all();

		return $this->sendResponse($schoolTerms->toArray(), "SchoolTerms retrieved successfully");
	}

	/**
	 * Show the form for creating a new SchoolTerm.
	 * GET|HEAD /schoolTerms/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created SchoolTerm in storage.
	 * POST /schoolTerms
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(SchoolTerm::$rules) > 0)
			$this->validateRequestOrFail($request, SchoolTerm::$rules);

		$input = $request->all();

		$schoolTerms = $this->schoolTermRepository->create($input);

		return $this->sendResponse($schoolTerms->toArray(), "SchoolTerm saved successfully");
	}

	/**
	 * Display the specified SchoolTerm.
	 * GET|HEAD /schoolTerms/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolTerm = $this->schoolTermRepository->apiFindOrFail($id);

		return $this->sendResponse($schoolTerm->toArray(), "SchoolTerm retrieved successfully");
	}

	/**
	 * Show the form for editing the specified SchoolTerm.
	 * GET|HEAD /schoolTerms/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified SchoolTerm in storage.
	 * PUT/PATCH /schoolTerms/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var SchoolTerm $schoolTerm */
		$schoolTerm = $this->schoolTermRepository->apiFindOrFail($id);

		$result = $this->schoolTermRepository->updateRich($input, $id);

		$schoolTerm = $schoolTerm->fresh();

		return $this->sendResponse($schoolTerm->toArray(), "SchoolTerm updated successfully");
	}

	/**
	 * Remove the specified SchoolTerm from storage.
	 * DELETE /schoolTerms/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->schoolTermRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "SchoolTerm deleted successfully");
	}
}
