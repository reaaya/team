<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\RatingRepository;
use App\Models\Rating;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class RatingAPIController extends AppBaseController
{
	/** @var  RatingRepository */
	private $ratingRepository;

	function __construct(RatingRepository $ratingRepo)
	{
		$this->ratingRepository = $ratingRepo;
	}

	/**
	 * Display a listing of the Rating.
	 * GET|HEAD /ratings
	 *
	 * @return Response
	 */
	public function index()
	{
		$ratings = $this->ratingRepository->all();

		return $this->sendResponse($ratings->toArray(), "Ratings retrieved successfully");
	}

	/**
	 * Show the form for creating a new Rating.
	 * GET|HEAD /ratings/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Rating in storage.
	 * POST /ratings
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Rating::$rules) > 0)
			$this->validateRequestOrFail($request, Rating::$rules);

		$input = $request->all();

		$ratings = $this->ratingRepository->create($input);

		return $this->sendResponse($ratings->toArray(), "Rating saved successfully");
	}

	/**
	 * Display the specified Rating.
	 * GET|HEAD /ratings/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$rating = $this->ratingRepository->apiFindOrFail($id);

		return $this->sendResponse($rating->toArray(), "Rating retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Rating.
	 * GET|HEAD /ratings/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Rating in storage.
	 * PUT/PATCH /ratings/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Rating $rating */
		$rating = $this->ratingRepository->apiFindOrFail($id);

		$result = $this->ratingRepository->updateRich($input, $id);

		$rating = $rating->fresh();

		return $this->sendResponse($rating->toArray(), "Rating updated successfully");
	}

	/**
	 * Remove the specified Rating from storage.
	 * DELETE /ratings/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ratingRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Rating deleted successfully");
	}
}
