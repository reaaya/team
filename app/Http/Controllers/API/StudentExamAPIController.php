<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\StudentExamRepository;
use App\Models\StudentExam;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class StudentExamAPIController extends AppBaseController
{
	/** @var  StudentExamRepository */
	private $studentExamRepository;

	function __construct(StudentExamRepository $studentExamRepo)
	{
		$this->studentExamRepository = $studentExamRepo;
	}

	/**
	 * Display a listing of the StudentExam.
	 * GET|HEAD /studentExams
	 *
	 * @return Response
	 */
	public function index()
	{
		$studentExams = $this->studentExamRepository->all();

		return $this->sendResponse($studentExams->toArray(), "StudentExams retrieved successfully");
	}

	/**
	 * Show the form for creating a new StudentExam.
	 * GET|HEAD /studentExams/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created StudentExam in storage.
	 * POST /studentExams
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(StudentExam::$rules) > 0)
			$this->validateRequestOrFail($request, StudentExam::$rules);

		$input = $request->all();

		$studentExams = $this->studentExamRepository->create($input);

		return $this->sendResponse($studentExams->toArray(), "StudentExam saved successfully");
	}

	/**
	 * Display the specified StudentExam.
	 * GET|HEAD /studentExams/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$studentExam = $this->studentExamRepository->apiFindOrFail($id);

		return $this->sendResponse($studentExam->toArray(), "StudentExam retrieved successfully");
	}

	/**
	 * Show the form for editing the specified StudentExam.
	 * GET|HEAD /studentExams/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified StudentExam in storage.
	 * PUT/PATCH /studentExams/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var StudentExam $studentExam */
		$studentExam = $this->studentExamRepository->apiFindOrFail($id);

		$result = $this->studentExamRepository->updateRich($input, $id);

		$studentExam = $studentExam->fresh();

		return $this->sendResponse($studentExam->toArray(), "StudentExam updated successfully");
	}

	/**
	 * Remove the specified StudentExam from storage.
	 * DELETE /studentExams/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->studentExamRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "StudentExam deleted successfully");
	}
}
