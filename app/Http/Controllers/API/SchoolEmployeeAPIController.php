<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolEmployeeRepository;
use App\Models\SchoolEmployee;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolEmployeeAPIController extends AppBaseController
{
	/** @var  SchoolEmployeeRepository */
	private $schoolEmployeeRepository;

	function __construct(SchoolEmployeeRepository $schoolEmployeeRepo)
	{
		$this->schoolEmployeeRepository = $schoolEmployeeRepo;
	}

	/**
	 * Display a listing of the SchoolEmployee.
	 * GET|HEAD /schoolEmployees
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolEmployees = $this->schoolEmployeeRepository->all();

		return $this->sendResponse($schoolEmployees->toArray(), "SchoolEmployees retrieved successfully");
	}

	/**
	 * Show the form for creating a new SchoolEmployee.
	 * GET|HEAD /schoolEmployees/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created SchoolEmployee in storage.
	 * POST /schoolEmployees
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(SchoolEmployee::$rules) > 0)
			$this->validateRequestOrFail($request, SchoolEmployee::$rules);

		$input = $request->all();

		$schoolEmployees = $this->schoolEmployeeRepository->create($input);

		return $this->sendResponse($schoolEmployees->toArray(), "SchoolEmployee saved successfully");
	}

	/**
	 * Display the specified SchoolEmployee.
	 * GET|HEAD /schoolEmployees/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolEmployee = $this->schoolEmployeeRepository->apiFindOrFail($id);

		return $this->sendResponse($schoolEmployee->toArray(), "SchoolEmployee retrieved successfully");
	}

	/**
	 * Show the form for editing the specified SchoolEmployee.
	 * GET|HEAD /schoolEmployees/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified SchoolEmployee in storage.
	 * PUT/PATCH /schoolEmployees/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var SchoolEmployee $schoolEmployee */
		$schoolEmployee = $this->schoolEmployeeRepository->apiFindOrFail($id);

		$result = $this->schoolEmployeeRepository->updateRich($input, $id);

		$schoolEmployee = $schoolEmployee->fresh();

		return $this->sendResponse($schoolEmployee->toArray(), "SchoolEmployee updated successfully");
	}

	/**
	 * Remove the specified SchoolEmployee from storage.
	 * DELETE /schoolEmployees/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->schoolEmployeeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "SchoolEmployee deleted successfully");
	}
}
