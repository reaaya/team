<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\HijraDateBaseRepository;
use App\Models\HijraDateBase;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class HijraDateBaseAPIController extends AppBaseController
{
	/** @var  HijraDateBaseRepository */
	private $hijraDateBaseRepository;

	function __construct(HijraDateBaseRepository $hijraDateBaseRepo)
	{
		$this->hijraDateBaseRepository = $hijraDateBaseRepo;
	}

	/**
	 * Display a listing of the HijraDateBase.
	 * GET|HEAD /hijraDateBases
	 *
	 * @return Response
	 */
	public function index()
	{
		$hijraDateBases = $this->hijraDateBaseRepository->all();

		return $this->sendResponse($hijraDateBases->toArray(), "HijraDateBases retrieved successfully");
	}

	/**
	 * Show the form for creating a new HijraDateBase.
	 * GET|HEAD /hijraDateBases/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created HijraDateBase in storage.
	 * POST /hijraDateBases
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(HijraDateBase::$rules) > 0)
			$this->validateRequestOrFail($request, HijraDateBase::$rules);

		$input = $request->all();

		$hijraDateBases = $this->hijraDateBaseRepository->create($input);

		return $this->sendResponse($hijraDateBases->toArray(), "HijraDateBase saved successfully");
	}

	/**
	 * Display the specified HijraDateBase.
	 * GET|HEAD /hijraDateBases/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$hijraDateBase = $this->hijraDateBaseRepository->apiFindOrFail($id);

		return $this->sendResponse($hijraDateBase->toArray(), "HijraDateBase retrieved successfully");
	}

	/**
	 * Show the form for editing the specified HijraDateBase.
	 * GET|HEAD /hijraDateBases/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified HijraDateBase in storage.
	 * PUT/PATCH /hijraDateBases/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var HijraDateBase $hijraDateBase */
		$hijraDateBase = $this->hijraDateBaseRepository->apiFindOrFail($id);

		$result = $this->hijraDateBaseRepository->updateRich($input, $id);

		$hijraDateBase = $hijraDateBase->fresh();

		return $this->sendResponse($hijraDateBase->toArray(), "HijraDateBase updated successfully");
	}

	/**
	 * Remove the specified HijraDateBase from storage.
	 * DELETE /hijraDateBases/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->hijraDateBaseRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "HijraDateBase deleted successfully");
	}
}
