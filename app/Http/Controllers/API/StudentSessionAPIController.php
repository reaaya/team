<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\StudentSessionRepository;
use App\Models\CourseSession;
use App\Models\StudentCourseSession;
use App\Models\StudentSession;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class StudentSessionAPIController extends AppBaseController
{
	/** @var  StudentSessionRepository */
	private $studentSessionRepository;

	function __construct(StudentSessionRepository $studentSessionRepo)
	{
		$this->group_num = Server::get('SCHOOLS',$this->user->current_school_id,'group_num');
		$this->school_id = $this->user->current_school_id;
		$this->studentSessionRepository = $studentSessionRepo;
	}


	/**
	 * Display a listing of the StudentSession.
	 * GET|HEAD /studentSessions
	 *
	 * @return Response
	 */
	public function index()
	{
		$studentSessions = $this->studentSessionRepository->all();

		return $this->sendResponse($studentSessions->toArray(), "StudentSessions retrieved successfully");
	}

	/**
	 * Show the form for creating a new StudentSession.
	 * GET|HEAD /studentSessions/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created StudentSession in storage.
	 * POST /studentSessions
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(StudentSession::$rules) > 0)
			$this->validateRequestOrFail($request, StudentSession::$rules);

		$input = $request->all();

		$studentSessions = $this->studentSessionRepository->create($input);

		return $this->sendResponse($studentSessions->toArray(), "StudentSession saved successfully");
	}

	/**
	 * Display the specified StudentSession.
	 * GET|HEAD /studentSessions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$studentSession = $this->studentSessionRepository->apiFindOrFail($id);

		return $this->sendResponse($studentSession->toArray(), "StudentSession retrieved successfully");
	}

	/**
	 * Show the form for editing the specified StudentSession.
	 * GET|HEAD /studentSessions/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified StudentSession in storage.
	 * PUT/PATCH /studentSessions/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var StudentSession $studentSession */
		$studentSession = $this->studentSessionRepository->apiFindOrFail($id);

		$result = $this->studentSessionRepository->updateRich($input, $id);

		$studentSession = $studentSession->fresh();

		return $this->sendResponse($studentSession->toArray(), "StudentSession updated successfully");
	}

	/**
	 * Remove the specified StudentSession from storage.
	 * DELETE /studentSessions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->studentSessionRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "StudentSession deleted successfully");
	}
}
