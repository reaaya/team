<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ClassCourseRepository;
use App\Models\ClassCourse;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ClassCourseAPIController extends AppBaseController
{
	/** @var  ClassCourseRepository */
	private $classCourseRepository;

	function __construct(ClassCourseRepository $classCourseRepo)
	{
		$this->classCourseRepository = $classCourseRepo;
	}

	/**
	 * Display a listing of the ClassCourse.
	 * GET|HEAD /classCourses
	 *
	 * @return Response
	 */
	public function index()
	{
		$classCourses = $this->classCourseRepository->all();

		return $this->sendResponse($classCourses->toArray(), "ClassCourses retrieved successfully");
	}

	/**
	 * Show the form for creating a new ClassCourse.
	 * GET|HEAD /classCourses/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created ClassCourse in storage.
	 * POST /classCourses
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(ClassCourse::$rules) > 0)
			$this->validateRequestOrFail($request, ClassCourse::$rules);

		$input = $request->all();

		$classCourses = $this->classCourseRepository->create($input);

		return $this->sendResponse($classCourses->toArray(), "ClassCourse saved successfully");
	}

	/**
	 * Display the specified ClassCourse.
	 * GET|HEAD /classCourses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$classCourse = $this->classCourseRepository->apiFindOrFail($id);

		return $this->sendResponse($classCourse->toArray(), "ClassCourse retrieved successfully");
	}

	/**
	 * Show the form for editing the specified ClassCourse.
	 * GET|HEAD /classCourses/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified ClassCourse in storage.
	 * PUT/PATCH /classCourses/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var ClassCourse $classCourse */
		$classCourse = $this->classCourseRepository->apiFindOrFail($id);

		$result = $this->classCourseRepository->updateRich($input, $id);

		$classCourse = $classCourse->fresh();

		return $this->sendResponse($classCourse->toArray(), "ClassCourse updated successfully");
	}

	/**
	 * Remove the specified ClassCourse from storage.
	 * DELETE /classCourses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->classCourseRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "ClassCourse deleted successfully");
	}
}
