<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TServiceRepository;
use App\Models\TService;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TServiceAPIController extends AppBaseController
{
	/** @var  TServiceRepository */
	private $tServiceRepository;

	function __construct(TServiceRepository $tServiceRepo)
	{
		$this->tServiceRepository = $tServiceRepo;
	}

	/**
	 * Display a listing of the TService.
	 * GET|HEAD /tServices
	 *
	 * @return Response
	 */
	public function index()
	{
		$tServices = $this->tServiceRepository->all();

		return $this->sendResponse($tServices->toArray(), "TServices retrieved successfully");
	}

	/**
	 * Show the form for creating a new TService.
	 * GET|HEAD /tServices/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created TService in storage.
	 * POST /tServices
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(TService::$rules) > 0)
			$this->validateRequestOrFail($request, TService::$rules);

		$input = $request->all();

		$tServices = $this->tServiceRepository->create($input);

		return $this->sendResponse($tServices->toArray(), "TService saved successfully");
	}

	/**
	 * Display the specified TService.
	 * GET|HEAD /tServices/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tService = $this->tServiceRepository->apiFindOrFail($id);

		return $this->sendResponse($tService->toArray(), "TService retrieved successfully");
	}

	/**
	 * Show the form for editing the specified TService.
	 * GET|HEAD /tServices/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified TService in storage.
	 * PUT/PATCH /tServices/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var TService $tService */
		$tService = $this->tServiceRepository->apiFindOrFail($id);

		$result = $this->tServiceRepository->updateRich($input, $id);

		$tService = $tService->fresh();

		return $this->sendResponse($tService->toArray(), "TService updated successfully");
	}

	/**
	 * Remove the specified TService from storage.
	 * DELETE /tServices/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->tServiceRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "TService deleted successfully");
	}
}
