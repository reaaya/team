<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\StudentCourseSessionRepository;
use App\Models\CourseSession;
use App\Models\StudentCourseSession;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class StudentCourseSessionAPIController extends AppBaseController
{
	/** @var  StudentCourseSessionRepository */
	private $studentSessionRepository;

	function __construct(StudentCourseSessionRepository $studentSessionRepo)
	{
		$this->studentSessionRepository = $studentSessionRepo;
	}
	/**
	 * Update only changed fields.
	 * POST|HEAD /studentSessions
	 *
	 * @return Response
	 */
	public function performUpdate(Request $request,$course_session_id)
	{
		$courseSessions = $request->get('course_sessions');
		dd($courseSessions);
	}
	/**
	 * Display a listing of the StudentCourseSession of a one course session.
	 * GET|HEAD /studentSessions
	 *
	 * @return Response
	 */
	public function listByCourseSessionId($course_session_id)
	{
		$items = StudentCourseSession::join('student','student.id','=','student_course_session.student_id')
								->where('student_course_session.course_session_id','=',$course_session_id)
								->get();
		$this->sendResponse(['status'=>true,'results'=>$items],null,200);
	}
	
	/**
	 * Display a listing of the StudentCourseSession.
	 * GET|HEAD /studentSessions
	 *
	 * @return Response
	 */
	public function index()
	{
		$studentSessions = $this->studentSessionRepository->all();

		return $this->sendResponse($studentSessions->toArray(), "StudentCourseSessions retrieved successfully");
	}

	/**
	 * Show the form for creating a new StudentCourseSession.
	 * GET|HEAD /studentSessions/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created StudentCourseSession in storage.
	 * POST /studentSessions
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(StudentCourseSession::$rules) > 0)
			$this->validateRequestOrFail($request, StudentCourseSession::$rules);

		$input = $request->all();

		$studentSessions = $this->studentSessionRepository->create($input);

		return $this->sendResponse($studentSessions->toArray(), "StudentCourseSession saved successfully");
	}

	/**
	 * Display the specified StudentCourseSession.
	 * GET|HEAD /studentSessions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$studentSession = $this->studentSessionRepository->apiFindOrFail($id);

		return $this->sendResponse($studentSession->toArray(), "StudentCourseSession retrieved successfully");
	}

	/**
	 * Show the form for editing the specified StudentCourseSession.
	 * GET|HEAD /studentSessions/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified StudentCourseSession in storage.
	 * PUT/PATCH /studentSessions/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var StudentCourseSession $studentSession */
		$studentSession = $this->studentSessionRepository->apiFindOrFail($id);

		$result = $this->studentSessionRepository->updateRich($input, $id);

		$studentSession = $studentSession->fresh();

		return $this->sendResponse($studentSession->toArray(), "StudentCourseSession updated successfully");
	}

	/**
	 * Remove the specified StudentCourseSession from storage.
	 * DELETE /studentSessions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->studentSessionRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "StudentCourseSession deleted successfully");
	}
}
