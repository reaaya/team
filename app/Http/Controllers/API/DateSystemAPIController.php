<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\DateSystemRepository;
use App\Models\DateSystem;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class DateSystemAPIController extends AppBaseController
{
	/** @var  DateSystemRepository */
	private $dateSystemRepository;

	function __construct(DateSystemRepository $dateSystemRepo)
	{
		$this->dateSystemRepository = $dateSystemRepo;
	}

	/**
	 * Display a listing of the DateSystem.
	 * GET|HEAD /dateSystems
	 *
	 * @return Response
	 */
	public function index()
	{
		$dateSystems = $this->dateSystemRepository->all();

		return $this->sendResponse($dateSystems->toArray(), "DateSystems retrieved successfully");
	}

	/**
	 * Show the form for creating a new DateSystem.
	 * GET|HEAD /dateSystems/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created DateSystem in storage.
	 * POST /dateSystems
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(DateSystem::$rules) > 0)
			$this->validateRequestOrFail($request, DateSystem::$rules);

		$input = $request->all();

		$dateSystems = $this->dateSystemRepository->create($input);

		return $this->sendResponse($dateSystems->toArray(), "DateSystem saved successfully");
	}

	/**
	 * Display the specified DateSystem.
	 * GET|HEAD /dateSystems/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$dateSystem = $this->dateSystemRepository->apiFindOrFail($id);

		return $this->sendResponse($dateSystem->toArray(), "DateSystem retrieved successfully");
	}

	/**
	 * Show the form for editing the specified DateSystem.
	 * GET|HEAD /dateSystems/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified DateSystem in storage.
	 * PUT/PATCH /dateSystems/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var DateSystem $dateSystem */
		$dateSystem = $this->dateSystemRepository->apiFindOrFail($id);

		$result = $this->dateSystemRepository->updateRich($input, $id);

		$dateSystem = $dateSystem->fresh();

		return $this->sendResponse($dateSystem->toArray(), "DateSystem updated successfully");
	}

	/**
	 * Remove the specified DateSystem from storage.
	 * DELETE /dateSystems/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->dateSystemRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "DateSystem deleted successfully");
	}
}
