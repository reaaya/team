<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ExamSessionRepository;
use App\Models\ExamSession;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ExamSessionAPIController extends AppBaseController
{
	/** @var  ExamSessionRepository */
	private $examSessionRepository;

	function __construct(ExamSessionRepository $examSessionRepo)
	{
		$this->examSessionRepository = $examSessionRepo;
	}

	/**
	 * Display a listing of the ExamSession.
	 * GET|HEAD /examSessions
	 *
	 * @return Response
	 */
	public function index()
	{
		$examSessions = $this->examSessionRepository->all();

		return $this->sendResponse($examSessions->toArray(), "ExamSessions retrieved successfully");
	}

	/**
	 * Show the form for creating a new ExamSession.
	 * GET|HEAD /examSessions/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created ExamSession in storage.
	 * POST /examSessions
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(ExamSession::$rules) > 0)
			$this->validateRequestOrFail($request, ExamSession::$rules);

		$input = $request->all();

		$examSessions = $this->examSessionRepository->create($input);

		return $this->sendResponse($examSessions->toArray(), "ExamSession saved successfully");
	}

	/**
	 * Display the specified ExamSession.
	 * GET|HEAD /examSessions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$examSession = $this->examSessionRepository->apiFindOrFail($id);

		return $this->sendResponse($examSession->toArray(), "ExamSession retrieved successfully");
	}

	/**
	 * Show the form for editing the specified ExamSession.
	 * GET|HEAD /examSessions/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified ExamSession in storage.
	 * PUT/PATCH /examSessions/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var ExamSession $examSession */
		$examSession = $this->examSessionRepository->apiFindOrFail($id);

		$result = $this->examSessionRepository->updateRich($input, $id);

		$examSession = $examSession->fresh();

		return $this->sendResponse($examSession->toArray(), "ExamSession updated successfully");
	}

	/**
	 * Remove the specified ExamSession from storage.
	 * DELETE /examSessions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->examSessionRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "ExamSession deleted successfully");
	}
}
