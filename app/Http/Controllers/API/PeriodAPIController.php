<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\PeriodRepository;
use App\Models\Period;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class PeriodAPIController extends AppBaseController
{
	/** @var  PeriodRepository */
	private $periodRepository;

	function __construct(PeriodRepository $periodRepo)
	{
		$this->periodRepository = $periodRepo;
	}

	/**
	 * Display a listing of the Period.
	 * GET|HEAD /periods
	 *
	 * @return Response
	 */
	public function index()
	{
		$periods = $this->periodRepository->all();

		return $this->sendResponse($periods->toArray(), "Periods retrieved successfully");
	}

	/**
	 * Show the form for creating a new Period.
	 * GET|HEAD /periods/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Period in storage.
	 * POST /periods
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Period::$rules) > 0)
			$this->validateRequestOrFail($request, Period::$rules);

		$input = $request->all();

		$periods = $this->periodRepository->create($input);

		return $this->sendResponse($periods->toArray(), "Period saved successfully");
	}

	/**
	 * Display the specified Period.
	 * GET|HEAD /periods/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$period = $this->periodRepository->apiFindOrFail($id);

		return $this->sendResponse($period->toArray(), "Period retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Period.
	 * GET|HEAD /periods/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Period in storage.
	 * PUT/PATCH /periods/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Period $period */
		$period = $this->periodRepository->apiFindOrFail($id);

		$result = $this->periodRepository->updateRich($input, $id);

		$period = $period->fresh();

		return $this->sendResponse($period->toArray(), "Period updated successfully");
	}

	/**
	 * Remove the specified Period from storage.
	 * DELETE /periods/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->periodRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Period deleted successfully");
	}
}
