<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AlertRepository;
use App\Models\Alert;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class AlertAPIController extends AppBaseController
{
	/** @var  AlertRepository */
	private $alertRepository;

	function __construct(AlertRepository $alertRepo)
	{
		$this->alertRepository = $alertRepo;
	}

	/**
	 * Display a listing of the Alert.
	 * GET|HEAD /alerts
	 *
	 * @return Response
	 */
	public function index()
	{
		$alerts = $this->alertRepository->all();

		return $this->sendResponse($alerts->toArray(), "Alerts retrieved successfully");
	}

	/**
	 * Show the form for creating a new Alert.
	 * GET|HEAD /alerts/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Alert in storage.
	 * POST /alerts
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Alert::$rules) > 0)
			$this->validateRequestOrFail($request, Alert::$rules);

		$input = $request->all();

		$alerts = $this->alertRepository->create($input);

		return $this->sendResponse($alerts->toArray(), "Alert saved successfully");
	}

	/**
	 * Display the specified Alert.
	 * GET|HEAD /alerts/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$alert = $this->alertRepository->apiFindOrFail($id);

		return $this->sendResponse($alert->toArray(), "Alert retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Alert.
	 * GET|HEAD /alerts/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Alert in storage.
	 * PUT/PATCH /alerts/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Alert $alert */
		$alert = $this->alertRepository->apiFindOrFail($id);

		$result = $this->alertRepository->updateRich($input, $id);

		$alert = $alert->fresh();

		return $this->sendResponse($alert->toArray(), "Alert updated successfully");
	}

	/**
	 * Remove the specified Alert from storage.
	 * DELETE /alerts/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->alertRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Alert deleted successfully");
	}
}
