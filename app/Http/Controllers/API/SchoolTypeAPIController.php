<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolTypeRepository;
use App\Models\SchoolType;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolTypeAPIController extends AppBaseController
{
	/** @var  SchoolTypeRepository */
	private $schoolTypeRepository;

	function __construct(SchoolTypeRepository $schoolTypeRepo)
	{
		$this->schoolTypeRepository = $schoolTypeRepo;
	}

	/**
	 * Display a listing of the SchoolType.
	 * GET|HEAD /schoolTypes
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolTypes = $this->schoolTypeRepository->all();

		return $this->sendResponse($schoolTypes->toArray(), "SchoolTypes retrieved successfully");
	}

	/**
	 * Show the form for creating a new SchoolType.
	 * GET|HEAD /schoolTypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created SchoolType in storage.
	 * POST /schoolTypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(SchoolType::$rules) > 0)
			$this->validateRequestOrFail($request, SchoolType::$rules);

		$input = $request->all();

		$schoolTypes = $this->schoolTypeRepository->create($input);

		return $this->sendResponse($schoolTypes->toArray(), "SchoolType saved successfully");
	}

	/**
	 * Display the specified SchoolType.
	 * GET|HEAD /schoolTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolType = $this->schoolTypeRepository->apiFindOrFail($id);

		return $this->sendResponse($schoolType->toArray(), "SchoolType retrieved successfully");
	}

	/**
	 * Show the form for editing the specified SchoolType.
	 * GET|HEAD /schoolTypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified SchoolType in storage.
	 * PUT/PATCH /schoolTypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var SchoolType $schoolType */
		$schoolType = $this->schoolTypeRepository->apiFindOrFail($id);

		$result = $this->schoolTypeRepository->updateRich($input, $id);

		$schoolType = $schoolType->fresh();

		return $this->sendResponse($schoolType->toArray(), "SchoolType updated successfully");
	}

	/**
	 * Remove the specified SchoolType from storage.
	 * DELETE /schoolTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->schoolTypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "SchoolType deleted successfully");
	}
}
