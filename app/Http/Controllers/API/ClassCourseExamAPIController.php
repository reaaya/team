<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ClassCourseExamRepository;
use App\Models\ClassCourseExam;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ClassCourseExamAPIController extends AppBaseController
{
	/** @var  ClassCourseExamRepository */
	private $classCourseExamRepository;

	function __construct(ClassCourseExamRepository $classCourseExamRepo)
	{
		$this->classCourseExamRepository = $classCourseExamRepo;
	}

	/**
	 * Display a listing of the ClassCourseExam.
	 * GET|HEAD /classCourseExams
	 *
	 * @return Response
	 */
	public function index()
	{
		$classCourseExams = $this->classCourseExamRepository->all();

		return $this->sendResponse($classCourseExams->toArray(), "ClassCourseExams retrieved successfully");
	}

	/**
	 * Show the form for creating a new ClassCourseExam.
	 * GET|HEAD /classCourseExams/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created ClassCourseExam in storage.
	 * POST /classCourseExams
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(ClassCourseExam::$rules) > 0)
			$this->validateRequestOrFail($request, ClassCourseExam::$rules);

		$input = $request->all();

		$classCourseExams = $this->classCourseExamRepository->create($input);

		return $this->sendResponse($classCourseExams->toArray(), "ClassCourseExam saved successfully");
	}

	/**
	 * Display the specified ClassCourseExam.
	 * GET|HEAD /classCourseExams/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$classCourseExam = $this->classCourseExamRepository->apiFindOrFail($id);

		return $this->sendResponse($classCourseExam->toArray(), "ClassCourseExam retrieved successfully");
	}

	/**
	 * Show the form for editing the specified ClassCourseExam.
	 * GET|HEAD /classCourseExams/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified ClassCourseExam in storage.
	 * PUT/PATCH /classCourseExams/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var ClassCourseExam $classCourseExam */
		$classCourseExam = $this->classCourseExamRepository->apiFindOrFail($id);

		$result = $this->classCourseExamRepository->updateRich($input, $id);

		$classCourseExam = $classCourseExam->fresh();

		return $this->sendResponse($classCourseExam->toArray(), "ClassCourseExam updated successfully");
	}

	/**
	 * Remove the specified ClassCourseExam from storage.
	 * DELETE /classCourseExams/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->classCourseExamRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "ClassCourseExam deleted successfully");
	}
}
