<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CourseRepository;
use App\Models\Course;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class CourseAPIController extends AppBaseController
{
	/** @var  CourseRepository */
	private $courseRepository;

	function __construct(CourseRepository $courseRepo)
	{
		//parent::__construct();
		$this->courseRepository = $courseRepo;
	}

	/**
	 * Display a listing of the Course.
	 * GET|HEAD /courses
	 *
	 * @return Response
	 */
	public function index()
	{
		$courses = $this->courseRepository->all();

		return $this->sendResponse($courses->toArray(), "Courses retrieved successfully");
	}

	/**
	 * Show the form for creating a new Course.
	 * GET|HEAD /courses/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Course in storage.
	 * POST /courses
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Course::$rules) > 0)
			$this->validateRequestOrFail($request, Course::$rules);

		$input = $request->all();

		$courses = $this->courseRepository->create($input);

		return $this->sendResponse($courses->toArray(), "Course saved successfully");
	}

	/**
	 * Display the specified Course.
	 * GET|HEAD /courses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$course = $this->courseRepository->apiFindOrFail($id);

		return $this->sendResponse($course->toArray(), "Course retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Course.
	 * GET|HEAD /courses/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Course in storage.
	 * PUT/PATCH /courses/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Course $course */
		$course = $this->courseRepository->apiFindOrFail($id);

		$result = $this->courseRepository->updateRich($input, $id);

		$course = $course->fresh();

		return $this->sendResponse($course->toArray(), "Course updated successfully");
	}

	/**
	 * Remove the specified Course from storage.
	 * DELETE /courses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->courseRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Course deleted successfully");
	}



	

	/**
	*	Created by Achraf
	*	Created at 07/03/2016
	*	Used in Function key: BF264
	*/
	public function getAllCourses()
	{

		$result = Course::select('id','course_name_ar as course_name')
			->where('active',"Y")->get();


		if( !$result->count() ){
			return $this->sendResponse(null, Lang::get('course.errors.empty_courses'), false);
		}

		return $this->sendResponse($result, Lang::get('course.success.valide_courses'), true);
	}


}
