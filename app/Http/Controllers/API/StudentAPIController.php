<?php namespace App\Http\Controllers\API;

use App\Helpers\Server;
use App\Http\Requests;
use Dingo\Api\Auth\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Libraries\Repositories\StudentRepository;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class StudentAPIController extends AppBaseController
{
	/** @var  StudentRepository */
	private $studentRepository;

	function __construct(StudentRepository $studentRepo)
	{
		parent::__construct();
		$this->group_num = Server::get('SCHOOLS',$this->user->current_school_id,'group_num');
		$this->school_id = $this->user->current_school_id;
		$this->studentRepository = $studentRepo;
	}
	/*
		Function Key:
		Created By Aziz: 12/03/2016
		List the student of a Course Session
	*/
	public function listStudentsByCourseSession(Request $request)
	{
		$course_session = $request->all();
		extract($course_session);
		$students = Student::select(
			DB::Raw('student.id AS student_id'),
			DB::Raw('CONCAT(student.firstname," ",student.f_firstname," ",student.lastname) AS fullname'),
			'student_session.coming_status_id','student_session.exit_status_id','student_session.coming_time',
			'student_session.exit_time','student_session.coming_alert_id','student_session.exit_alert_id'
		)
			->join('student_session','student_session.student_id','=','student.id')
			->where('student_session.group_num','=',$this->group_num)
			->where('student_session.school_id','=',$this->school_id)
			->where('student_session.year','=',$year)
			->where('student_session.hmonth','=',$hmonth)
			->where('student_session.hday_num','=',$hday_num)
			->where('student_session.level_class_id','=',$level_class_id)
			->where('student_session.symbol','=',$symbol)
			->where('student_session.session_order','=',$session_order)
			->get();

		return $this->sendResponse($students,null);
	}
	/**
	 * Display a listing of the Student.
	 * GET|HEAD /students
	 *
	 * @return Response
	 */
	public function index()
	{
		$students = $this->studentRepository->all();

		return $this->sendResponse($students->toArray(), "Students retrieved successfully");
	}

	/**
	 * Show the form for creating a new Student.
	 * GET|HEAD /students/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Student in storage.
	 * POST /students
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Student::$rules) > 0)
			$this->validateRequestOrFail($request, Student::$rules);

		$input = $request->all();

		$students = $this->studentRepository->create($input);

		return $this->sendResponse($students->toArray(), "Student saved successfully");
	}

	/**
	 * Display the specified Student.
	 * GET|HEAD /students/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$student = $this->studentRepository->apiFindOrFail($id);

		return $this->sendResponse($student->toArray(), "Student retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Student.
	 * GET|HEAD /students/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Student in storage.
	 * PUT/PATCH /students/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Student $student */
		$student = $this->studentRepository->apiFindOrFail($id);

		$result = $this->studentRepository->updateRich($input, $id);

		$student = $student->fresh();

		return $this->sendResponse($student->toArray(), "Student updated successfully");
	}

	/**
	 * Remove the specified Student from storage.
	 * DELETE /students/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->studentRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Student deleted successfully");
	}
}
