<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\GenreRepository;
use App\Models\Genre;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class GenreAPIController extends AppBaseController
{
	/** @var  GenreRepository */
	private $genreRepository;

	function __construct(GenreRepository $genreRepo)
	{
		$this->genreRepository = $genreRepo;
	}

	/**
	 * Display a listing of the Genre.
	 * GET|HEAD /genres
	 *
	 * @return Response
	 */
	public function index()
	{
		$genres = $this->genreRepository->all();

		return $this->sendResponse($genres->toArray(), "Genres retrieved successfully");
	}

	/**
	 * Show the form for creating a new Genre.
	 * GET|HEAD /genres/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Genre in storage.
	 * POST /genres
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Genre::$rules) > 0)
			$this->validateRequestOrFail($request, Genre::$rules);

		$input = $request->all();

		$genres = $this->genreRepository->create($input);

		return $this->sendResponse($genres->toArray(), "Genre saved successfully");
	}

	/**
	 * Display the specified Genre.
	 * GET|HEAD /genres/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$genre = $this->genreRepository->apiFindOrFail($id);

		return $this->sendResponse($genre->toArray(), "Genre retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Genre.
	 * GET|HEAD /genres/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Genre in storage.
	 * PUT/PATCH /genres/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Genre $genre */
		$genre = $this->genreRepository->apiFindOrFail($id);

		$result = $this->genreRepository->updateRich($input, $id);

		$genre = $genre->fresh();

		return $this->sendResponse($genre->toArray(), "Genre updated successfully");
	}

	/**
	 * Remove the specified Genre from storage.
	 * DELETE /genres/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->genreRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Genre deleted successfully");
	}
}
