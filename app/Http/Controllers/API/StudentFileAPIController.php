<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\StudentFileRepository;
use App\Models\StudentFile;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class StudentFileAPIController extends AppBaseController
{
	/** @var  StudentFileRepository */
	private $studentFileRepository;

	function __construct(StudentFileRepository $studentFileRepo)
	{
		$this->studentFileRepository = $studentFileRepo;
	}

	/**
	 * Display a listing of the StudentFile.
	 * GET|HEAD /studentFiles
	 *
	 * @return Response
	 */
	public function index()
	{
		$studentFiles = $this->studentFileRepository->all();

		return $this->sendResponse($studentFiles->toArray(), "StudentFiles retrieved successfully");
	}

	/**
	 * Show the form for creating a new StudentFile.
	 * GET|HEAD /studentFiles/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created StudentFile in storage.
	 * POST /studentFiles
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(StudentFile::$rules) > 0)
			$this->validateRequestOrFail($request, StudentFile::$rules);

		$input = $request->all();

		$studentFiles = $this->studentFileRepository->create($input);

		return $this->sendResponse($studentFiles->toArray(), "StudentFile saved successfully");
	}

	/**
	 * Display the specified StudentFile.
	 * GET|HEAD /studentFiles/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$studentFile = $this->studentFileRepository->apiFindOrFail($id);

		return $this->sendResponse($studentFile->toArray(), "StudentFile retrieved successfully");
	}

	/**
	 * Show the form for editing the specified StudentFile.
	 * GET|HEAD /studentFiles/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified StudentFile in storage.
	 * PUT/PATCH /studentFiles/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var StudentFile $studentFile */
		$studentFile = $this->studentFileRepository->apiFindOrFail($id);

		$result = $this->studentFileRepository->updateRich($input, $id);

		$studentFile = $studentFile->fresh();

		return $this->sendResponse($studentFile->toArray(), "StudentFile updated successfully");
	}

	/**
	 * Remove the specified StudentFile from storage.
	 * DELETE /studentFiles/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->studentFileRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "StudentFile deleted successfully");
	}
}
