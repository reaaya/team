<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolPeriodRepository;
use App\Models\SchoolPeriod;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolPeriodAPIController extends AppBaseController
{
	/** @var  SchoolPeriodRepository */
	private $schoolPeriodRepository;

	function __construct(SchoolPeriodRepository $schoolPeriodRepo)
	{
		$this->schoolPeriodRepository = $schoolPeriodRepo;
	}

	/**
	 * Display a listing of the SchoolPeriod.
	 * GET|HEAD /schoolPeriods
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolPeriods = $this->schoolPeriodRepository->all();

		return $this->sendResponse($schoolPeriods->toArray(), "SchoolPeriods retrieved successfully");
	}

	/**
	 * Show the form for creating a new SchoolPeriod.
	 * GET|HEAD /schoolPeriods/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created SchoolPeriod in storage.
	 * POST /schoolPeriods
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(SchoolPeriod::$rules) > 0)
			$this->validateRequestOrFail($request, SchoolPeriod::$rules);

		$input = $request->all();

		$schoolPeriods = $this->schoolPeriodRepository->create($input);

		return $this->sendResponse($schoolPeriods->toArray(), "SchoolPeriod saved successfully");
	}

	/**
	 * Display the specified SchoolPeriod.
	 * GET|HEAD /schoolPeriods/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolPeriod = $this->schoolPeriodRepository->apiFindOrFail($id);

		return $this->sendResponse($schoolPeriod->toArray(), "SchoolPeriod retrieved successfully");
	}

	/**
	 * Show the form for editing the specified SchoolPeriod.
	 * GET|HEAD /schoolPeriods/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified SchoolPeriod in storage.
	 * PUT/PATCH /schoolPeriods/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var SchoolPeriod $schoolPeriod */
		$schoolPeriod = $this->schoolPeriodRepository->apiFindOrFail($id);

		$result = $this->schoolPeriodRepository->updateRich($input, $id);

		$schoolPeriod = $schoolPeriod->fresh();

		return $this->sendResponse($schoolPeriod->toArray(), "SchoolPeriod updated successfully");
	}

	/**
	 * Remove the specified SchoolPeriod from storage.
	 * DELETE /schoolPeriods/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->schoolPeriodRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "SchoolPeriod deleted successfully");
	}
}
