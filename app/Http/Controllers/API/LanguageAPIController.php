<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\LanguageRepository;
use App\Models\Language;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class LanguageAPIController extends AppBaseController
{
	/** @var  LanguageRepository */
	private $languageRepository;

	function __construct(LanguageRepository $languageRepo)
	{
		$this->languageRepository = $languageRepo;
	}

	/**
	 * Display a listing of the Language.
	 * GET|HEAD /languages
	 *
	 * @return Response
	 */
	public function index()
	{
		$languages = $this->languageRepository->all();

		return $this->sendResponse($languages->toArray(), "Languages retrieved successfully");
	}

	/**
	 * Show the form for creating a new Language.
	 * GET|HEAD /languages/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Language in storage.
	 * POST /languages
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Language::$rules) > 0)
			$this->validateRequestOrFail($request, Language::$rules);

		$input = $request->all();

		$languages = $this->languageRepository->create($input);

		return $this->sendResponse($languages->toArray(), "Language saved successfully");
	}

	/**
	 * Display the specified Language.
	 * GET|HEAD /languages/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$language = $this->languageRepository->apiFindOrFail($id);

		return $this->sendResponse($language->toArray(), "Language retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Language.
	 * GET|HEAD /languages/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Language in storage.
	 * PUT/PATCH /languages/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Language $language */
		$language = $this->languageRepository->apiFindOrFail($id);

		$result = $this->languageRepository->updateRich($input, $id);

		$language = $language->fresh();

		return $this->sendResponse($language->toArray(), "Language updated successfully");
	}

	/**
	 * Remove the specified Language from storage.
	 * DELETE /languages/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->languageRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Language deleted successfully");
	}
}
