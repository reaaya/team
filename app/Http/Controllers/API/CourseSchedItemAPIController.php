<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CourseSchedItemRepository;
use App\Models\CourseSchedItem;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CourseSchedItemAPIController extends AppBaseController
{
	/** @var  CourseSchedItemRepository */
	private $courseSchedItemRepository;

	function __construct(CourseSchedItemRepository $courseSchedItemRepo)
	{
		$this->courseSchedItemRepository = $courseSchedItemRepo;
	}

	/**
	 * Display a listing of the CourseSchedItem.
	 * GET|HEAD /courseSchedItems
	 *
	 * @return Response
	 */
	public function index()
	{
		$courseSchedItems = $this->courseSchedItemRepository->all();

		return $this->sendResponse($courseSchedItems->toArray(), "CourseSchedItems retrieved successfully");
	}

	/**
	 * Show the form for creating a new CourseSchedItem.
	 * GET|HEAD /courseSchedItems/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created CourseSchedItem in storage.
	 * POST /courseSchedItems
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(CourseSchedItem::$rules) > 0)
			$this->validateRequestOrFail($request, CourseSchedItem::$rules);

		$input = $request->all();

		$courseSchedItems = $this->courseSchedItemRepository->create($input);

		return $this->sendResponse($courseSchedItems->toArray(), "CourseSchedItem saved successfully");
	}

	/**
	 * Display the specified CourseSchedItem.
	 * GET|HEAD /courseSchedItems/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$courseSchedItem = $this->courseSchedItemRepository->apiFindOrFail($id);

		return $this->sendResponse($courseSchedItem->toArray(), "CourseSchedItem retrieved successfully");
	}

	/**
	 * Show the form for editing the specified CourseSchedItem.
	 * GET|HEAD /courseSchedItems/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified CourseSchedItem in storage.
	 * PUT/PATCH /courseSchedItems/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var CourseSchedItem $courseSchedItem */
		$courseSchedItem = $this->courseSchedItemRepository->apiFindOrFail($id);

		$result = $this->courseSchedItemRepository->updateRich($input, $id);

		$courseSchedItem = $courseSchedItem->fresh();

		return $this->sendResponse($courseSchedItem->toArray(), "CourseSchedItem updated successfully");
	}

	/**
	 * Remove the specified CourseSchedItem from storage.
	 * DELETE /courseSchedItems/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->courseSchedItemRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "CourseSchedItem deleted successfully");
	}




	/**
	*	Created by Achraf
	*	Created at 07/03/2016
	*	Function key: BF264 / Q0010
	*/
	public function GetItems(Request $request)
	{

		$school_year_id = $request->get('school_year_id',null);
		$level_class_id = $request->get('level_class_id',null);
		$symbol_id = $request->get('symbol_id',null);

		$result = CourseSchedItem::select('wday_id','id','session_order','session_start_time','session_end_time','course_id')
			->where('school_year_id',$school_year_id)
			->where('level_class_id',$level_class_id)
			->where('symbol',$symbol_id)->get();

		$planning = array();
		$i = 1;
		foreach ($result as $item) {
			$planning[$item->wday_id][$item->session_order] = array(
				'id' => $item->id,
				'session_start_time' => $item->session_start_time,
				'session_end_time' => $item->session_end_time,
				'course_id' => $item->course_id,
				);
		}

		if( !$result->count() ){
			return $this->sendResponse(null, Lang::get('course_sched_item.errors.empty_course_sched_item'), false);	
		}

		return $this->sendResponse($planning, Lang::get('course_sched_item.success.valide_course_sched_item'), true);
	}



	/**
	*	Created by Achraf
	*	Created at 08/03/2016
	*	Function key: BF264 / Q0011
	*/
	public function GetDefaultWeekTemplate()
	{

		$result = DB::table('week_template')
		->select('id','week_template_name','day1_template_id','day2_template_id','day3_template_id','day4_template_id','day5_template_id','day6_template_id','day7_template_id')
		->where(function($query){
                 $query->whereNull('school_id');
                 $query->orWhere('school_id', 1);
        	})
		->where(function($query){
                 $query->whereNull('level_class_id');
                 $query->orWhere('level_class_id', 1);
        	})
		->where('active','=','Y')
		->get();

		return $this->sendResponse($result, Lang::get('course_sched_item.success.valide_default_week_template'), true);	
	}



	/**
	*	Created by Achraf
	*	Created at 08/03/2016
	*	Function key: BF264 / Q0012
	*/
	public function CreatFromWeekTemplate(Request $request)
	{
		if( !$request->get('week_template_id')
			OR !$request->get('school_year_id')
			OR !$request->get('level_class_id')
			OR !$request->get('symbol')
		){
			return $this->sendResponse(null, 'course_sched_item.errors.empty_params', false);
		}

		$week_template = DB::table('week_template')->where('id',$request->get('week_template_id') )->first();
		for ($wday=1; $wday <= 7; $wday++){
			$num_day_name = "day{$wday}_template_id";
			$query = "insert into `course_sched_item`
					(`school_year_id`,`level_class_id`, `symbol`,`wday_id`,`session_order`,
					 `session_start_time`,`session_end_time`,`course_id`, 
					 active, version, created_by, created_at) 
					select :school_year_id_{$wday},:level_class_id_{$wday},:symbol_{$wday},:wday_{$wday},
					`session_order`,`session_start_time`,`session_end_time`, 
					null, 'Y', 0, :user_id_{$wday}, now() 
					from day_template_item 
					where day_template_id = :day_template_id_{$wday}
					";
			$params_query = [
				":school_year_id_{$wday}" => $request->get('school_year_id'),
				":level_class_id_{$wday}" => $request->get('level_class_id'),
				":symbol_{$wday}" => $request->get('symbol'),
				":wday_{$wday}" => $wday,
				":user_id_{$wday}" => Auth::user()->id,
				":day_template_id_{$wday}" => $week_template->{$num_day_name},
			];
			DB::insert( $query, $params_query );
		}

		
		return $this->sendResponse(null, Lang::get('course_sched_item.success.valide_default_week_template'), true);
	}




	/**
	*	Created by Achraf
	*	Created at 08/03/2016
	*	Function key: used in BF264 / Save Items table
	*/
	public function SaveItems(Request $request)
	{
		$items = $request->get('course_sched_item');

		if( empty($items) ){
			return $this->sendResponse(null, 'course_sched_item.errors.empty_params', false);
		}

		foreach($items as $id => $course_id){
				if(empty($course_id)) $course_id = null;
				CourseSchedItem::where( 'id' ,$id )->update(['course_id' => $course_id ]);
		}
		return $this->sendResponse(null, Lang::get('course_sched_item.success.valide_default_week_template'), true);
	
	}


}
