<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolMemberDayRepository;
use App\Models\SchoolMemberDay;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolMemberDayAPIController extends AppBaseController
{
	/** @var  SchoolMemberDayRepository */
	private $schoolMemberDayRepository;

	function __construct(SchoolMemberDayRepository $schoolMemberDayRepo)
	{
		$this->schoolMemberDayRepository = $schoolMemberDayRepo;
	}

	/**
	 * Display a listing of the SchoolMemberDay.
	 * GET|HEAD /schoolMemberDays
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolMemberDays = $this->schoolMemberDayRepository->all();

		return $this->sendResponse($schoolMemberDays->toArray(), "SchoolMemberDays retrieved successfully");
	}

	/**
	 * Show the form for creating a new SchoolMemberDay.
	 * GET|HEAD /schoolMemberDays/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created SchoolMemberDay in storage.
	 * POST /schoolMemberDays
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(SchoolMemberDay::$rules) > 0)
			$this->validateRequestOrFail($request, SchoolMemberDay::$rules);

		$input = $request->all();

		$schoolMemberDays = $this->schoolMemberDayRepository->create($input);

		return $this->sendResponse($schoolMemberDays->toArray(), "SchoolMemberDay saved successfully");
	}

	/**
	 * Display the specified SchoolMemberDay.
	 * GET|HEAD /schoolMemberDays/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolMemberDay = $this->schoolMemberDayRepository->apiFindOrFail($id);

		return $this->sendResponse($schoolMemberDay->toArray(), "SchoolMemberDay retrieved successfully");
	}

	/**
	 * Show the form for editing the specified SchoolMemberDay.
	 * GET|HEAD /schoolMemberDays/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified SchoolMemberDay in storage.
	 * PUT/PATCH /schoolMemberDays/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var SchoolMemberDay $schoolMemberDay */
		$schoolMemberDay = $this->schoolMemberDayRepository->apiFindOrFail($id);

		$result = $this->schoolMemberDayRepository->updateRich($input, $id);

		$schoolMemberDay = $schoolMemberDay->fresh();

		return $this->sendResponse($schoolMemberDay->toArray(), "SchoolMemberDay updated successfully");
	}

	/**
	 * Remove the specified SchoolMemberDay from storage.
	 * DELETE /schoolMemberDays/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->schoolMemberDayRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "SchoolMemberDay deleted successfully");
	}
}
