<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\StudentFileStatusRepository;
use App\Models\StudentFileStatus;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class StudentFileStatusAPIController extends AppBaseController
{
	/** @var  StudentFileStatusRepository */
	private $studentFileStatusRepository;

	function __construct(StudentFileStatusRepository $studentFileStatusRepo)
	{
		$this->studentFileStatusRepository = $studentFileStatusRepo;
	}

	/**
	 * Display a listing of the StudentFileStatus.
	 * GET|HEAD /studentFileStatuses
	 *
	 * @return Response
	 */
	public function index()
	{
		$studentFileStatuses = $this->studentFileStatusRepository->all();

		return $this->sendResponse($studentFileStatuses->toArray(), "StudentFileStatuses retrieved successfully");
	}

	/**
	 * Show the form for creating a new StudentFileStatus.
	 * GET|HEAD /studentFileStatuses/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created StudentFileStatus in storage.
	 * POST /studentFileStatuses
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(StudentFileStatus::$rules) > 0)
			$this->validateRequestOrFail($request, StudentFileStatus::$rules);

		$input = $request->all();

		$studentFileStatuses = $this->studentFileStatusRepository->create($input);

		return $this->sendResponse($studentFileStatuses->toArray(), "StudentFileStatus saved successfully");
	}

	/**
	 * Display the specified StudentFileStatus.
	 * GET|HEAD /studentFileStatuses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$studentFileStatus = $this->studentFileStatusRepository->apiFindOrFail($id);

		return $this->sendResponse($studentFileStatus->toArray(), "StudentFileStatus retrieved successfully");
	}

	/**
	 * Show the form for editing the specified StudentFileStatus.
	 * GET|HEAD /studentFileStatuses/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified StudentFileStatus in storage.
	 * PUT/PATCH /studentFileStatuses/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var StudentFileStatus $studentFileStatus */
		$studentFileStatus = $this->studentFileStatusRepository->apiFindOrFail($id);

		$result = $this->studentFileStatusRepository->updateRich($input, $id);

		$studentFileStatus = $studentFileStatus->fresh();

		return $this->sendResponse($studentFileStatus->toArray(), "StudentFileStatus updated successfully");
	}

	/**
	 * Remove the specified StudentFileStatus from storage.
	 * DELETE /studentFileStatuses/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->studentFileStatusRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "StudentFileStatus deleted successfully");
	}
}
