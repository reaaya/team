<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCountryRequest;
use App\Http\Requests\UpdateCountryRequest;
use App\Libraries\Repositories\CountryRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CountryController extends AppBaseController
{

	/** @var  CountryRepository */
	private $countryRepository;

	function __construct(CountryRepository $countryRepo)
	{
		$this->countryRepository = $countryRepo;
	}

	/**
	 * Display a listing of the Country.
	 *
	 * @return Response
	 */
	public function index()
	{
		$countries = $this->countryRepository->paginate(10);

		return view('countries.index')
			->with('countries', $countries);
	}

	/**
	 * Show the form for creating a new Country.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('countries.create');
	}

	/**
	 * Store a newly created Country in storage.
	 *
	 * @param CreateCountryRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCountryRequest $request)
	{
		$input = $request->all();

		$country = $this->countryRepository->create($input);

		Flash::success('Country saved successfully.');

		return redirect(route('countries.index'));
	}

	/**
	 * Display the specified Country.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$country = $this->countryRepository->find($id);

		if(empty($country))
		{
			Flash::error('Country not found');

			return redirect(route('countries.index'));
		}

		return view('countries.show')->with('country', $country);
	}

	/**
	 * Show the form for editing the specified Country.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$country = $this->countryRepository->find($id);

		if(empty($country))
		{
			Flash::error('Country not found');

			return redirect(route('countries.index'));
		}

		return view('countries.edit')->with('country', $country);
	}

	/**
	 * Update the specified Country in storage.
	 *
	 * @param  int              $id
	 * @param UpdateCountryRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCountryRequest $request)
	{
		$country = $this->countryRepository->find($id);

		if(empty($country))
		{
			Flash::error('Country not found');

			return redirect(route('countries.index'));
		}

		$this->countryRepository->updateRich($request->all(), $id);

		Flash::success('Country updated successfully.');

		return redirect(route('countries.index'));
	}

	/**
	 * Remove the specified Country from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$country = $this->countryRepository->find($id);

		if(empty($country))
		{
			Flash::error('Country not found');

			return redirect(route('countries.index'));
		}

		$this->countryRepository->delete($id);

		Flash::success('Country deleted successfully.');

		return redirect(route('countries.index'));
	}
}
