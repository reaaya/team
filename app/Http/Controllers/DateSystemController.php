<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDateSystemRequest;
use App\Http\Requests\UpdateDateSystemRequest;
use App\Libraries\Repositories\DateSystemRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class DateSystemController extends AppBaseController
{

	/** @var  DateSystemRepository */
	private $dateSystemRepository;

	function __construct(DateSystemRepository $dateSystemRepo)
	{
		$this->dateSystemRepository = $dateSystemRepo;
	}

	/**
	 * Display a listing of the DateSystem.
	 *
	 * @return Response
	 */
	public function index()
	{
		$dateSystems = $this->dateSystemRepository->paginate(10);

		return view('dateSystems.index')
			->with('dateSystems', $dateSystems);
	}

	/**
	 * Show the form for creating a new DateSystem.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('dateSystems.create');
	}

	/**
	 * Store a newly created DateSystem in storage.
	 *
	 * @param CreateDateSystemRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDateSystemRequest $request)
	{
		$input = $request->all();

		$dateSystem = $this->dateSystemRepository->create($input);

		Flash::success('DateSystem saved successfully.');

		return redirect(route('dateSystems.index'));
	}

	/**
	 * Display the specified DateSystem.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$dateSystem = $this->dateSystemRepository->find($id);

		if(empty($dateSystem))
		{
			Flash::error('DateSystem not found');

			return redirect(route('dateSystems.index'));
		}

		return view('dateSystems.show')->with('dateSystem', $dateSystem);
	}

	/**
	 * Show the form for editing the specified DateSystem.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$dateSystem = $this->dateSystemRepository->find($id);

		if(empty($dateSystem))
		{
			Flash::error('DateSystem not found');

			return redirect(route('dateSystems.index'));
		}

		return view('dateSystems.edit')->with('dateSystem', $dateSystem);
	}

	/**
	 * Update the specified DateSystem in storage.
	 *
	 * @param  int              $id
	 * @param UpdateDateSystemRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateDateSystemRequest $request)
	{
		$dateSystem = $this->dateSystemRepository->find($id);

		if(empty($dateSystem))
		{
			Flash::error('DateSystem not found');

			return redirect(route('dateSystems.index'));
		}

		$this->dateSystemRepository->updateRich($request->all(), $id);

		Flash::success('DateSystem updated successfully.');

		return redirect(route('dateSystems.index'));
	}

	/**
	 * Remove the specified DateSystem from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$dateSystem = $this->dateSystemRepository->find($id);

		if(empty($dateSystem))
		{
			Flash::error('DateSystem not found');

			return redirect(route('dateSystems.index'));
		}

		$this->dateSystemRepository->delete($id);

		Flash::success('DateSystem deleted successfully.');

		return redirect(route('dateSystems.index'));
	}
}
