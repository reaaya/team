<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSchoolPeriodRequest;
use App\Http\Requests\UpdateSchoolPeriodRequest;
use App\Libraries\Repositories\SchoolPeriodRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolPeriodController extends AppBaseController
{

	/** @var  SchoolPeriodRepository */
	private $schoolPeriodRepository;

	function __construct(SchoolPeriodRepository $schoolPeriodRepo)
	{
		$this->schoolPeriodRepository = $schoolPeriodRepo;
	}

	/**
	 * Display a listing of the SchoolPeriod.
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolPeriods = $this->schoolPeriodRepository->paginate(10);

		return view('schoolPeriods.index')
			->with('schoolPeriods', $schoolPeriods);
	}

	/**
	 * Show the form for creating a new SchoolPeriod.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schoolPeriods.create');
	}

	/**
	 * Store a newly created SchoolPeriod in storage.
	 *
	 * @param CreateSchoolPeriodRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSchoolPeriodRequest $request)
	{
		$input = $request->all();

		$schoolPeriod = $this->schoolPeriodRepository->create($input);

		Flash::success('SchoolPeriod saved successfully.');

		return redirect(route('schoolPeriods.index'));
	}

	/**
	 * Display the specified SchoolPeriod.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolPeriod = $this->schoolPeriodRepository->find($id);

		if(empty($schoolPeriod))
		{
			Flash::error('SchoolPeriod not found');

			return redirect(route('schoolPeriods.index'));
		}

		return view('schoolPeriods.show')->with('schoolPeriod', $schoolPeriod);
	}

	/**
	 * Show the form for editing the specified SchoolPeriod.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$schoolPeriod = $this->schoolPeriodRepository->find($id);

		if(empty($schoolPeriod))
		{
			Flash::error('SchoolPeriod not found');

			return redirect(route('schoolPeriods.index'));
		}

		return view('schoolPeriods.edit')->with('schoolPeriod', $schoolPeriod);
	}

	/**
	 * Update the specified SchoolPeriod in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSchoolPeriodRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSchoolPeriodRequest $request)
	{
		$schoolPeriod = $this->schoolPeriodRepository->find($id);

		if(empty($schoolPeriod))
		{
			Flash::error('SchoolPeriod not found');

			return redirect(route('schoolPeriods.index'));
		}

		$this->schoolPeriodRepository->updateRich($request->all(), $id);

		Flash::success('SchoolPeriod updated successfully.');

		return redirect(route('schoolPeriods.index'));
	}

	/**
	 * Remove the specified SchoolPeriod from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$schoolPeriod = $this->schoolPeriodRepository->find($id);

		if(empty($schoolPeriod))
		{
			Flash::error('SchoolPeriod not found');

			return redirect(route('schoolPeriods.index'));
		}

		$this->schoolPeriodRepository->delete($id);

		Flash::success('SchoolPeriod deleted successfully.');

		return redirect(route('schoolPeriods.index'));
	}
}
