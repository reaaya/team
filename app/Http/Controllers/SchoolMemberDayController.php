<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSchoolMemberDayRequest;
use App\Http\Requests\UpdateSchoolMemberDayRequest;
use App\Libraries\Repositories\SchoolMemberDayRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolMemberDayController extends AppBaseController
{

	/** @var  SchoolMemberDayRepository */
	private $schoolMemberDayRepository;

	function __construct(SchoolMemberDayRepository $schoolMemberDayRepo)
	{
		$this->schoolMemberDayRepository = $schoolMemberDayRepo;
	}

	/**
	 * Display a listing of the SchoolMemberDay.
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolMemberDays = $this->schoolMemberDayRepository->paginate(10);

		return view('schoolMemberDays.index')
			->with('schoolMemberDays', $schoolMemberDays);
	}

	/**
	 * Show the form for creating a new SchoolMemberDay.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schoolMemberDays.create');
	}

	/**
	 * Store a newly created SchoolMemberDay in storage.
	 *
	 * @param CreateSchoolMemberDayRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSchoolMemberDayRequest $request)
	{
		$input = $request->all();

		$schoolMemberDay = $this->schoolMemberDayRepository->create($input);

		Flash::success('SchoolMemberDay saved successfully.');

		return redirect(route('schoolMemberDays.index'));
	}

	/**
	 * Display the specified SchoolMemberDay.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolMemberDay = $this->schoolMemberDayRepository->find($id);

		if(empty($schoolMemberDay))
		{
			Flash::error('SchoolMemberDay not found');

			return redirect(route('schoolMemberDays.index'));
		}

		return view('schoolMemberDays.show')->with('schoolMemberDay', $schoolMemberDay);
	}

	/**
	 * Show the form for editing the specified SchoolMemberDay.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$schoolMemberDay = $this->schoolMemberDayRepository->find($id);

		if(empty($schoolMemberDay))
		{
			Flash::error('SchoolMemberDay not found');

			return redirect(route('schoolMemberDays.index'));
		}

		return view('schoolMemberDays.edit')->with('schoolMemberDay', $schoolMemberDay);
	}

	/**
	 * Update the specified SchoolMemberDay in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSchoolMemberDayRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSchoolMemberDayRequest $request)
	{
		$schoolMemberDay = $this->schoolMemberDayRepository->find($id);

		if(empty($schoolMemberDay))
		{
			Flash::error('SchoolMemberDay not found');

			return redirect(route('schoolMemberDays.index'));
		}

		$this->schoolMemberDayRepository->updateRich($request->all(), $id);

		Flash::success('SchoolMemberDay updated successfully.');

		return redirect(route('schoolMemberDays.index'));
	}

	/**
	 * Remove the specified SchoolMemberDay from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$schoolMemberDay = $this->schoolMemberDayRepository->find($id);

		if(empty($schoolMemberDay))
		{
			Flash::error('SchoolMemberDay not found');

			return redirect(route('schoolMemberDays.index'));
		}

		$this->schoolMemberDayRepository->delete($id);

		Flash::success('SchoolMemberDay deleted successfully.');

		return redirect(route('schoolMemberDays.index'));
	}
}
