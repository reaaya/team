<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateEFileRequest;
use App\Http\Requests\UpdateEFileRequest;
use App\Libraries\Repositories\EFileRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class EFileController extends AppBaseController
{

	/** @var  EFileRepository */
	private $eFileRepository;

	function __construct(EFileRepository $eFileRepo)
	{
		$this->eFileRepository = $eFileRepo;
	}

	/**
	 * Display a listing of the EFile.
	 *
	 * @return Response
	 */
	public function index()
	{
		$eFiles = $this->eFileRepository->paginate(10);

		return view('eFiles.index')
			->with('eFiles', $eFiles);
	}

	/**
	 * Show the form for creating a new EFile.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('eFiles.create');
	}

	/**
	 * Store a newly created EFile in storage.
	 *
	 * @param CreateEFileRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateEFileRequest $request)
	{
		$input = $request->all();

		$eFile = $this->eFileRepository->create($input);

		Flash::success('EFile saved successfully.');

		return redirect(route('eFiles.index'));
	}

	/**
	 * Display the specified EFile.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$eFile = $this->eFileRepository->find($id);

		if(empty($eFile))
		{
			Flash::error('EFile not found');

			return redirect(route('eFiles.index'));
		}

		return view('eFiles.show')->with('eFile', $eFile);
	}

	/**
	 * Show the form for editing the specified EFile.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$eFile = $this->eFileRepository->find($id);

		if(empty($eFile))
		{
			Flash::error('EFile not found');

			return redirect(route('eFiles.index'));
		}

		return view('eFiles.edit')->with('eFile', $eFile);
	}

	/**
	 * Update the specified EFile in storage.
	 *
	 * @param  int              $id
	 * @param UpdateEFileRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateEFileRequest $request)
	{
		$eFile = $this->eFileRepository->find($id);

		if(empty($eFile))
		{
			Flash::error('EFile not found');

			return redirect(route('eFiles.index'));
		}

		$this->eFileRepository->updateRich($request->all(), $id);

		Flash::success('EFile updated successfully.');

		return redirect(route('eFiles.index'));
	}

	/**
	 * Remove the specified EFile from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$eFile = $this->eFileRepository->find($id);

		if(empty($eFile))
		{
			Flash::error('EFile not found');

			return redirect(route('eFiles.index'));
		}

		$this->eFileRepository->delete($id);

		Flash::success('EFile deleted successfully.');

		return redirect(route('eFiles.index'));
	}
}
