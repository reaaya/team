<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSchoolClassRequest;
use App\Http\Requests\UpdateSchoolClassRequest;
use App\Libraries\Repositories\SchoolClassRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SchoolClassController extends AppBaseController
{

	/** @var  SchoolClassRepository */
	private $schoolClassRepository;

	function __construct(SchoolClassRepository $schoolClassRepo)
	{
		$this->schoolClassRepository = $schoolClassRepo;
	}

	/**
	 * Display a listing of the SchoolClass.
	 *
	 * @return Response
	 */
	public function index()
	{
		$schoolClasses = $this->schoolClassRepository->paginate(10);

		return view('schoolClasses.index')
			->with('schoolClasses', $schoolClasses);
	}

	/**
	 * Show the form for creating a new SchoolClass.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schoolClasses.create');
	}

	/**
	 * Store a newly created SchoolClass in storage.
	 *
	 * @param CreateSchoolClassRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSchoolClassRequest $request)
	{
		$input = $request->all();

		$schoolClass = $this->schoolClassRepository->create($input);

		Flash::success('SchoolClass saved successfully.');

		return redirect(route('schoolClasses.index'));
	}

	/**
	 * Display the specified SchoolClass.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$schoolClass = $this->schoolClassRepository->find($id);

		if(empty($schoolClass))
		{
			Flash::error('SchoolClass not found');

			return redirect(route('schoolClasses.index'));
		}

		return view('schoolClasses.show')->with('schoolClass', $schoolClass);
	}

	/**
	 * Show the form for editing the specified SchoolClass.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$schoolClass = $this->schoolClassRepository->find($id);

		if(empty($schoolClass))
		{
			Flash::error('SchoolClass not found');

			return redirect(route('schoolClasses.index'));
		}

		return view('schoolClasses.edit')->with('schoolClass', $schoolClass);
	}

	/**
	 * Update the specified SchoolClass in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSchoolClassRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSchoolClassRequest $request)
	{
		$schoolClass = $this->schoolClassRepository->find($id);

		if(empty($schoolClass))
		{
			Flash::error('SchoolClass not found');

			return redirect(route('schoolClasses.index'));
		}

		$this->schoolClassRepository->updateRich($request->all(), $id);

		Flash::success('SchoolClass updated successfully.');

		return redirect(route('schoolClasses.index'));
	}

	/**
	 * Remove the specified SchoolClass from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$schoolClass = $this->schoolClassRepository->find($id);

		if(empty($schoolClass))
		{
			Flash::error('SchoolClass not found');

			return redirect(route('schoolClasses.index'));
		}

		$this->schoolClassRepository->delete($id);

		Flash::success('SchoolClass deleted successfully.');

		return redirect(route('schoolClasses.index'));
	}
}
