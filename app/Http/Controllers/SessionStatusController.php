<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSessionStatusRequest;
use App\Http\Requests\UpdateSessionStatusRequest;
use App\Libraries\Repositories\SessionStatusRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SessionStatusController extends AppBaseController
{

	/** @var  SessionStatusRepository */
	private $sessionStatusRepository;

	function __construct(SessionStatusRepository $sessionStatusRepo)
	{
		$this->sessionStatusRepository = $sessionStatusRepo;
	}

	/**
	 * Display a listing of the SessionStatus.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sessionStatuses = $this->sessionStatusRepository->paginate(10);

		return view('sessionStatuses.index')
			->with('sessionStatuses', $sessionStatuses);
	}

	/**
	 * Show the form for creating a new SessionStatus.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('sessionStatuses.create');
	}

	/**
	 * Store a newly created SessionStatus in storage.
	 *
	 * @param CreateSessionStatusRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSessionStatusRequest $request)
	{
		$input = $request->all();

		$sessionStatus = $this->sessionStatusRepository->create($input);

		Flash::success('SessionStatus saved successfully.');

		return redirect(route('sessionStatuses.index'));
	}

	/**
	 * Display the specified SessionStatus.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$sessionStatus = $this->sessionStatusRepository->find($id);

		if(empty($sessionStatus))
		{
			Flash::error('SessionStatus not found');

			return redirect(route('sessionStatuses.index'));
		}

		return view('sessionStatuses.show')->with('sessionStatus', $sessionStatus);
	}

	/**
	 * Show the form for editing the specified SessionStatus.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$sessionStatus = $this->sessionStatusRepository->find($id);

		if(empty($sessionStatus))
		{
			Flash::error('SessionStatus not found');

			return redirect(route('sessionStatuses.index'));
		}

		return view('sessionStatuses.edit')->with('sessionStatus', $sessionStatus);
	}

	/**
	 * Update the specified SessionStatus in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSessionStatusRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSessionStatusRequest $request)
	{
		$sessionStatus = $this->sessionStatusRepository->find($id);

		if(empty($sessionStatus))
		{
			Flash::error('SessionStatus not found');

			return redirect(route('sessionStatuses.index'));
		}

		$this->sessionStatusRepository->updateRich($request->all(), $id);

		Flash::success('SessionStatus updated successfully.');

		return redirect(route('sessionStatuses.index'));
	}

	/**
	 * Remove the specified SessionStatus from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$sessionStatus = $this->sessionStatusRepository->find($id);

		if(empty($sessionStatus))
		{
			Flash::error('SessionStatus not found');

			return redirect(route('sessionStatuses.index'));
		}

		$this->sessionStatusRepository->delete($id);

		Flash::success('SessionStatus deleted successfully.');

		return redirect(route('sessionStatuses.index'));
	}
}
