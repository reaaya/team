<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseSessionsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('course_session', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->integer('validated_by');
			$table->dateTime('validated_at');
			$table->string('active', 255);
			$table->integer('version');
			$table->string('update_groups_mfk', 255);
			$table->string('delete_groups_mfk', 255);
			$table->string('display_groups_mfk', 255);
			$table->integer('sci_id');
			$table->string('session_hdate', 255);
			$table->string('session_end_time', 255);
			$table->string('session_start_time', 255);
			$table->integer('course_id');
			$table->integer('level_class_id');
			$table->integer('prof_id');
			$table->integer('school_id');
			$table->integer('session_status_id');
			$table->string('session_status_comment', 255);
			$table->string('symbol', 255);
			$table->string('year', 255);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('course_session');
	}

}
