<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('school', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->integer('validated_by');
			$table->dateTime('validated_at');
			$table->string('active', 255);
			$table->integer('version');
			$table->string('update_groups_mfk', 255);
			$table->string('delete_groups_mfk', 255);
			$table->string('display_groups_mfk', 255);
			$table->integer('sci_id');
			$table->integer('scapacity');
			$table->string('expiring_hdate', 255);
			$table->integer('city_id');
			$table->integer('genre_id');
			$table->integer('group_school_id');
			$table->integer('lang_id');
			$table->integer('school_type_id');
			$table->string('period_mfk', 255);
			$table->decimal('sp2', 5, 2);
			$table->string('address', 255);
			$table->string('maps_location_url', 255);
			$table->string('pc', 255);
			$table->string('quarter', 255);
			$table->string('school_name_ar', 255);
			$table->string('school_name_en', 255);
			$table->smallInteger('sp1');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('school');
	}

}
