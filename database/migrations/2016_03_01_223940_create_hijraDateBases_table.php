<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHijraDateBasesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hijra_date_base', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('HIJRI_YEAR');
			$table->integer('HIJRI_MONTH');
			$table->integer('HIJRI_MONTH_DAYS');
			$table->string('GREG_DATE', 255);
			$table->string('HOLIDAY_DAYS', 255);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hijra_date_base');
	}

}
