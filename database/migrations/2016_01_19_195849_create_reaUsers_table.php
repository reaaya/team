<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReaUsersTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//$this->down();

		Schema::create('rea_user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('creation_user_id');
			$table->integer('update_user_id');
			$table->integer('validation_user_id');
			$table->timestamp('validation_date');
			$table->string('active', 255);
			$table->integer('version');
			$table->string('update_groups_mfk', 255);
			$table->string('delete_groups_mfk', 255);
			$table->string('display_groups_mfk', 255);
			$table->integer('sci_id');
			$table->timestamp('creation_date');
			$table->timestamp('update_date');


			$table->integer('city_id');
			$table->integer('country_id');
			$table->integer('current_school_id');
			$table->integer('idn_type_id');
			$table->string('address', 255);
			$table->string('cp', 255);
			$table->string('firstname', 255);
			$table->string('f_firstname', 255);
			$table->string('email', 100);
			$table->string('idn', 255);
			$table->string('lastname', 255);
			$table->string('mobile', 255);
			$table->string('pwd', 255);
			$table->string('quarter', 255);
			$table->string('sim_card_sn', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rea_user');
	}

}
