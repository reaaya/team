<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lang', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('creation_user_id');
			$table->integer('update_user_id');
			$table->integer('validation_user_id');
			$table->timestamp('validation_date')->default(null);
			$table->string('active', 255);
			$table->integer('version');
			$table->string('update_groups_mfk', 255);
			$table->string('delete_groups_mfk', 255);
			$table->string('display_groups_mfk', 255);
			$table->integer('sci_id');
			$table->timestamp('creation_date');
			$table->timestamp('update_date');
			$table->string('short_name', 2);
			$table->string('folder_name', 2);
			$table->string('long_name', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lang');
	}

}
