<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTypesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//$this->down();

		Schema::create('attendance_type', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('creation_user_id');
			$table->timestamp('creation_date');
			$table->integer('update_user_id');
			$table->timestamp('update_date');
			$table->integer('validation_user_id');
			$table->timestamp('validation_date');
			$table->string('active', 255);
			$table->integer('version');
			$table->string('update_groups_mfk', 255);
			$table->string('delete_groups_mfk', 255);
			$table->string('display_groups_mfk', 255);
			$table->integer('sci_id');
			$table->string('attendance_type_name', 255);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attendance_type');
	}

}
