<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolLevelsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//$this->down();

		Schema::create('school_level', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('creation_user_id');
			$table->integer('update_user_id');
			$table->integer('validation_user_id');
			$table->timestamp('validation_date');
			$table->string('active', 255);
			$table->integer('version');
			$table->string('update_groups_mfk', 255);
			$table->string('delete_groups_mfk', 255);
			$table->string('display_groups_mfk', 255);
			$table->integer('sci_id');
			$table->integer('school_id');
			$table->string('school_level_name', 255);
			$table->timestamp('creation_date');
			$table->timestamp('update_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('school_level');
	}

}
