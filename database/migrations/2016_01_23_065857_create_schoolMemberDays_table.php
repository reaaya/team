<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolMemberDaysTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//$this->down();

		Schema::create('school_member_day', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('creation_user_id');
			$table->timestamp('creation_date');
			$table->integer('update_user_id');
			$table->timestamp('update_date');
			$table->integer('validation_user_id');
			$table->timestamp('validation_date');
			$table->string('active', 255);
			$table->integer('version');
			$table->string('update_groups_mfk', 255);
			$table->string('delete_groups_mfk', 255);
			$table->string('display_groups_mfk', 255);
			$table->integer('sci_id');
			$table->string('day_hdate', 255);
			$table->string('coming_time', 255);
			$table->string('exit_time', 255);
			$table->integer('coming_status_id');
			$table->integer('exit_status_id');
			$table->integer('rea_user_id');
			$table->integer('school_id');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('school_member_day');
	}

}
