<?php

use Illuminate\Database\Seeder;
use App\Models\CourseSession;
use App\Models\Course;
use App\Models\LevelClass;
use App\Models\School;
use App\Models\SessionStatus;
use Carbon\Carbon;
use App\Models\SchoolEmployee;
class CourseSessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseSession::truncate();
        $courses = Course::all(['id'])->toArray();
        $levelClass = LevelClass::all(['id'])->toArray();

        $now = Carbon::now();
        $now->setWeekendDays([0,6]);
        $day = 1;
        $first_day_time = $now->startOfMonth();
        $nb_days = $now->endOfMonth()->day;
        $now = Carbon::now();
        while($day<=$nb_days){
            $session_start_time = $first_day_time->day($day);
            if(!$first_day_time->isWeekend()){
                $hour=7;
                $n = 0;
                if($now->day > $session_start_time->day)
                    $status = 1;
                elseif($now->day < $session_start_time->day)
                    $status = 2;
                else
                    $status = 3;

                while($hour<13){
                    $course_session[] = [
                        'active' => 'Y',
                        'sci_id' => '0',
                        'session_name' => str_random(10),
                        'course_id' => $courses[random_int(0,count($courses)-1)]['id'],
                        'level_class_id' => $levelClass[random_int(0,count($levelClass)-1)]['id'],
                        'school_id' => 1,
                        'symbol' => str_random(5),
                        'year' => '1437',
                        'session_status_id'=>$status,
                        'prof_id' => 1,
                        'session_start_time'=> $session_start_time->hour($hour)->minute(0)->toDateTimeString(),
                        'session_end_time'=> $session_start_time->minute(59)->toDateTimeString(),
                    ];
                    $hour++;
                    $n++;
                }
            }
            $day++;
        }
        CourseSession::insert($course_session);
    }
}
