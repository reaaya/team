<?php

use Illuminate\Database\Seeder;
use App\Models\Course;
class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::truncate();
        $i = 0;
        while($i < 9){
            $courses[] = [
                'active' => 'Y','sci_id' => '0','course_name' => str_random(10)
            ];
            $i++;
        };
        Course::insert($courses);
    }
}
