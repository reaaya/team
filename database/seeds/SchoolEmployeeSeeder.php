<?php

use Illuminate\Database\Seeder;
use App\Models\SchoolEmployee;
class SchoolEmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolEmployee::truncate();
        $school_employee = [
            [
                'rea_user_id' => 1,
                'job_title'=> "teacher",
                'school_id'=> 1
            ],
            [
                'rea_user_id' => 2,
                'job_title'=> "teacher",
                'school_id'=> 2
            ],
            [
                'rea_user_id' => 1,
                'job_title'=> "student_guide",
                'school_id'=> 2
            ],
            [
                'rea_user_id' => 3,
                'job_title'=> "director",
                'school_id'=> 3
            ]
        ] ;
        SchoolEmployee::insert($school_employee);
    }
}
