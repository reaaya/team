<?php

use Illuminate\Database\Seeder;
use App\Models\SessionStatus;
class SessionStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SessionStatus::truncate();
        $session_status =[
            ['active' => 'Y','sci_id' => '0','session_status_name' => 'closed'],
            ['active' => 'Y','sci_id' => '0','session_status_name' => 'coming'],
            ['active' => 'Y','sci_id' => '0','session_status_name' => 'open'],
            ['active' => 'Y','sci_id' => '0','session_status_name' => 'cancelled'],
        ];
        SessionStatus::insert($session_status);
    }
}
