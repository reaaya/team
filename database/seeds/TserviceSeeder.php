<?php

use Illuminate\Database\Seeder;
use App\Models\TService;
class TserviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TService::truncate();
        $tservice = array(
            array('id' => '1','creation_user_id' => '1','creation_date' => '2016-01-05 16:35:05','update_user_id' => '1','update_date' => '2016-01-05 16:35:05','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','tservice_name' => 'تدريس'),
            array('id' => '2','creation_user_id' => '1','creation_date' => '2016-01-05 16:35:15','update_user_id' => '1','update_date' => '2016-01-05 16:35:15','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','tservice_name' => 'تعليم قرآن'),
            array('id' => '3','creation_user_id' => '1','creation_date' => '2016-01-05 16:35:26','update_user_id' => '1','update_date' => '2016-01-05 16:35:26','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','tservice_name' => 'تدريب رياضي')
        );
        TService::insert($tservice);
    }
}
