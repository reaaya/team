<?php

use Illuminate\Database\Seeder;
use App\Models\Genre;
class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genre::truncate();
        $genre = array(
            array('id' => '1','creation_user_id' => '1','creation_date' => '2016-01-05 16:33:19','update_user_id' => '1','update_date' => '2016-01-05 16:33:19','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','genre_name' => 'بنين','genre_name2' => 'ذكر'),
            array('id' => '2','creation_user_id' => '1','creation_date' => '2016-01-05 16:33:40','update_user_id' => '1','update_date' => '2016-01-05 16:33:40','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','genre_name' => 'بنات','genre_name2' => 'أنثى')
        );
        Genre::insert($genre);
    }
}
