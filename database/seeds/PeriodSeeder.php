<?php

use Illuminate\Database\Seeder;
use App\Models\Period;
class PeriodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Period::truncate();
        $period = array(
            array('id' => '1','creation_user_id' => '1','creation_date' => '2016-01-05 13:48:45','update_user_id' => '1','update_date' => '2016-01-05 13:48:45','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','period_name' => 'فترة صباحية'),
            array('id' => '2','creation_user_id' => '1','creation_date' => '2016-01-05 14:01:17','update_user_id' => '1','update_date' => '2016-01-05 14:01:17','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','period_name' => 'فترة مسائية'),
            array('id' => '3','creation_user_id' => '1','creation_date' => '2016-01-05 14:01:17','update_user_id' => '1','update_date' => '2016-01-05 14:01:17','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','period_name' => 'فترة ليلية')
        );
        Period::insert($period);
    }
}
