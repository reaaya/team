@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Languages</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('languages.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($languages->isEmpty())
                <div class="well text-center">No Languages found.</div>
            @else
                @include('languages.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $languages])


    </div>
@endsection
