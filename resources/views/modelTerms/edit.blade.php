@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($modelTerm, ['route' => ['modelTerms.update', $modelTerm->id], 'method' => 'patch']) !!}

        @include('modelTerms.fields')

    {!! Form::close() !!}
</div>
@endsection
