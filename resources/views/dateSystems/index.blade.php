@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">DateSystems</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('dateSystems.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($dateSystems->isEmpty())
                <div class="well text-center">No DateSystems found.</div>
            @else
                @include('dateSystems.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $dateSystems])


    </div>
@endsection
