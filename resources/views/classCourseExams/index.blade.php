@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">ClassCourseExams</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('classCourseExams.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($classCourseExams->isEmpty())
                <div class="well text-center">No ClassCourseExams found.</div>
            @else
                @include('classCourseExams.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $classCourseExams])


    </div>
@endsection
