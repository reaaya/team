@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'cities.store']) !!}

        @include('cities.fields')

    {!! Form::close() !!}
</div>
@endsection
