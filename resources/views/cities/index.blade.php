@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cities</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('cities.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($cities->isEmpty())
                <div class="well text-center">No Cities found.</div>
            @else
                @include('cities.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $cities])


    </div>
@endsection
