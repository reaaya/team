<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Rea User Id</th>
			<th>Job Title</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($schoolEmployees as $schoolEmployee)
        <tr>
            <td>{!! $schoolEmployee->id !!}</td>
			<td>{!! $schoolEmployee->creation_user_id !!}</td>
			<td>{!! $schoolEmployee->creation_date !!}</td>
			<td>{!! $schoolEmployee->update_user_id !!}</td>
			<td>{!! $schoolEmployee->update_date !!}</td>
			<td>{!! $schoolEmployee->validation_user_id !!}</td>
			<td>{!! $schoolEmployee->validation_date !!}</td>
			<td>{!! $schoolEmployee->active !!}</td>
			<td>{!! $schoolEmployee->version !!}</td>
			<td>{!! $schoolEmployee->update_groups_mfk !!}</td>
			<td>{!! $schoolEmployee->delete_groups_mfk !!}</td>
			<td>{!! $schoolEmployee->display_groups_mfk !!}</td>
			<td>{!! $schoolEmployee->sci_id !!}</td>
			<td>{!! $schoolEmployee->rea_user_id !!}</td>
			<td>{!! $schoolEmployee->job_title !!}</td>
            <td>
                <a href="{!! route('schoolEmployees.edit', [$schoolEmployee->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('schoolEmployees.delete', [$schoolEmployee->id]) !!}" onclick="return confirm('Are you sure wants to delete this SchoolEmployee?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
