@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'schoolEmployees.store']) !!}

        @include('schoolEmployees.fields')

    {!! Form::close() !!}
</div>
@endsection
