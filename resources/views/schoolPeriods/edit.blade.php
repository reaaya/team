@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($schoolPeriod, ['route' => ['schoolPeriods.update', $schoolPeriod->id], 'method' => 'patch']) !!}

        @include('schoolPeriods.fields')

    {!! Form::close() !!}
</div>
@endsection
