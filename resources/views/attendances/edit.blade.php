@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($attendance, ['route' => ['attendances.update', $attendance->id], 'method' => 'patch']) !!}

        @include('attendances.fields')

    {!! Form::close() !!}
</div>
@endsection
