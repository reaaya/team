<!-- Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
	{!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Creation User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
	{!! Form::number('creation_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Creation Date Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('creation_date', 'Creation Date:') !!}
	{!! Form::date('creation_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Update User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
	{!! Form::number('update_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Update Date Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('update_date', 'Update Date:') !!}
	{!! Form::date('update_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Validation User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
	{!! Form::number('validation_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Validation Date Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('validation_date', 'Validation Date:') !!}
	{!! Form::date('validation_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('active', 'Active:') !!}
	{!! Form::text('active', null, ['class' => 'form-control']) !!}
</div>

<!-- Version Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('version', 'Version:') !!}
	{!! Form::number('version', null, ['class' => 'form-control']) !!}
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
	{!! Form::text('update_groups_mfk', null, ['class' => 'form-control']) !!}
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
	{!! Form::text('delete_groups_mfk', null, ['class' => 'form-control']) !!}
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
	{!! Form::text('display_groups_mfk', null, ['class' => 'form-control']) !!}
</div>

<!-- Sci Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('sci_id', 'Sci Id:') !!}
	{!! Form::number('sci_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Att Hdate Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('att_hdate', 'Att Hdate:') !!}
	{!! Form::text('att_hdate', null, ['class' => 'form-control']) !!}
</div>

<!-- Att Time Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('att_time', 'Att Time:') !!}
	{!! Form::text('att_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Attendance Type Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('attendance_type_id', 'Attendance Type Id:') !!}
	{!! Form::number('attendance_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Rea User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rea_user_id', 'Rea User Id:') !!}
	{!! Form::number('rea_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Rejected Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rejected', 'Rejected:') !!}
	{!! Form::text('rejected', null, ['class' => 'form-control']) !!}
</div>

<!-- Att Comments Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('att_comments', 'Att Comments:') !!}
	{!! Form::text('att_comments', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
