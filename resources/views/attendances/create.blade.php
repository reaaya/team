@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'attendances.store']) !!}

        @include('attendances.fields')

    {!! Form::close() !!}
</div>
@endsection
