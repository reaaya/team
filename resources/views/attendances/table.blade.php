<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Att Hdate</th>
			<th>Att Time</th>
			<th>Attendance Type Id</th>
			<th>Rea User Id</th>
			<th>Rejected</th>
			<th>Att Comments</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($attendances as $attendance)
        <tr>
            <td>{!! $attendance->id !!}</td>
			<td>{!! $attendance->creation_user_id !!}</td>
			<td>{!! $attendance->creation_date !!}</td>
			<td>{!! $attendance->update_user_id !!}</td>
			<td>{!! $attendance->update_date !!}</td>
			<td>{!! $attendance->validation_user_id !!}</td>
			<td>{!! $attendance->validation_date !!}</td>
			<td>{!! $attendance->active !!}</td>
			<td>{!! $attendance->version !!}</td>
			<td>{!! $attendance->update_groups_mfk !!}</td>
			<td>{!! $attendance->delete_groups_mfk !!}</td>
			<td>{!! $attendance->display_groups_mfk !!}</td>
			<td>{!! $attendance->sci_id !!}</td>
			<td>{!! $attendance->att_hdate !!}</td>
			<td>{!! $attendance->att_time !!}</td>
			<td>{!! $attendance->attendance_type_id !!}</td>
			<td>{!! $attendance->rea_user_id !!}</td>
			<td>{!! $attendance->rejected !!}</td>
			<td>{!! $attendance->att_comments !!}</td>
            <td>
                <a href="{!! route('attendances.edit', [$attendance->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('attendances.delete', [$attendance->id]) !!}" onclick="return confirm('Are you sure wants to delete this Attendance?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
