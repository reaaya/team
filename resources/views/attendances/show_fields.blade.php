<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $attendance->id !!}</p>
</div>

<!-- Creation User Id Field -->
<div class="form-group">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
    <p>{!! $attendance->creation_user_id !!}</p>
</div>

<!-- Creation Date Field -->
<div class="form-group">
    {!! Form::label('creation_date', 'Creation Date:') !!}
    <p>{!! $attendance->creation_date !!}</p>
</div>

<!-- Update User Id Field -->
<div class="form-group">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
    <p>{!! $attendance->update_user_id !!}</p>
</div>

<!-- Update Date Field -->
<div class="form-group">
    {!! Form::label('update_date', 'Update Date:') !!}
    <p>{!! $attendance->update_date !!}</p>
</div>

<!-- Validation User Id Field -->
<div class="form-group">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
    <p>{!! $attendance->validation_user_id !!}</p>
</div>

<!-- Validation Date Field -->
<div class="form-group">
    {!! Form::label('validation_date', 'Validation Date:') !!}
    <p>{!! $attendance->validation_date !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $attendance->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $attendance->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $attendance->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $attendance->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $attendance->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $attendance->sci_id !!}</p>
</div>

<!-- Att Hdate Field -->
<div class="form-group">
    {!! Form::label('att_hdate', 'Att Hdate:') !!}
    <p>{!! $attendance->att_hdate !!}</p>
</div>

<!-- Att Time Field -->
<div class="form-group">
    {!! Form::label('att_time', 'Att Time:') !!}
    <p>{!! $attendance->att_time !!}</p>
</div>

<!-- Attendance Type Id Field -->
<div class="form-group">
    {!! Form::label('attendance_type_id', 'Attendance Type Id:') !!}
    <p>{!! $attendance->attendance_type_id !!}</p>
</div>

<!-- Rea User Id Field -->
<div class="form-group">
    {!! Form::label('rea_user_id', 'Rea User Id:') !!}
    <p>{!! $attendance->rea_user_id !!}</p>
</div>

<!-- Rejected Field -->
<div class="form-group">
    {!! Form::label('rejected', 'Rejected:') !!}
    <p>{!! $attendance->rejected !!}</p>
</div>

<!-- Att Comments Field -->
<div class="form-group">
    {!! Form::label('att_comments', 'Att Comments:') !!}
    <p>{!! $attendance->att_comments !!}</p>
</div>

