<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Attendance Status Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($attendanceStatuses as $attendanceStatus)
        <tr>
            <td>{!! $attendanceStatus->id !!}</td>
			<td>{!! $attendanceStatus->creation_user_id !!}</td>
			<td>{!! $attendanceStatus->creation_date !!}</td>
			<td>{!! $attendanceStatus->update_user_id !!}</td>
			<td>{!! $attendanceStatus->update_date !!}</td>
			<td>{!! $attendanceStatus->validation_user_id !!}</td>
			<td>{!! $attendanceStatus->validation_date !!}</td>
			<td>{!! $attendanceStatus->active !!}</td>
			<td>{!! $attendanceStatus->version !!}</td>
			<td>{!! $attendanceStatus->update_groups_mfk !!}</td>
			<td>{!! $attendanceStatus->delete_groups_mfk !!}</td>
			<td>{!! $attendanceStatus->display_groups_mfk !!}</td>
			<td>{!! $attendanceStatus->sci_id !!}</td>
			<td>{!! $attendanceStatus->attendance_status_name !!}</td>
            <td>
                <a href="{!! route('attendanceStatuses.edit', [$attendanceStatus->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('attendanceStatuses.delete', [$attendanceStatus->id]) !!}" onclick="return confirm('Are you sure wants to delete this AttendanceStatus?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
