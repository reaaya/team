@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'schoolYears.store']) !!}

        @include('schoolYears.fields')

    {!! Form::close() !!}
</div>
@endsection
