<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Session Status Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($sessionStatuses as $sessionStatus)
        <tr>
            <td>{!! $sessionStatus->id !!}</td>
			<td>{!! $sessionStatus->creation_user_id !!}</td>
			<td>{!! $sessionStatus->creation_date !!}</td>
			<td>{!! $sessionStatus->update_user_id !!}</td>
			<td>{!! $sessionStatus->update_date !!}</td>
			<td>{!! $sessionStatus->validation_user_id !!}</td>
			<td>{!! $sessionStatus->validation_date !!}</td>
			<td>{!! $sessionStatus->active !!}</td>
			<td>{!! $sessionStatus->version !!}</td>
			<td>{!! $sessionStatus->update_groups_mfk !!}</td>
			<td>{!! $sessionStatus->delete_groups_mfk !!}</td>
			<td>{!! $sessionStatus->display_groups_mfk !!}</td>
			<td>{!! $sessionStatus->sci_id !!}</td>
			<td>{!! $sessionStatus->session_status_name !!}</td>
            <td>
                <a href="{!! route('sessionStatuses.edit', [$sessionStatus->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('sessionStatuses.delete', [$sessionStatus->id]) !!}" onclick="return confirm('Are you sure wants to delete this SessionStatus?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
