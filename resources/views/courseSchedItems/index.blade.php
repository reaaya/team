@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">CourseSchedItems</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('courseSchedItems.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($courseSchedItems->isEmpty())
                <div class="well text-center">No CourseSchedItems found.</div>
            @else
                @include('courseSchedItems.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $courseSchedItems])


    </div>
@endsection
