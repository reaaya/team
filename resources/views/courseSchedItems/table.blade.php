<table class="table">
    <thead>
    <th>Id</th>
			<th>Created By</th>
			<th>Created At</th>
			<th>Updated By</th>
			<th>Updated At</th>
			<th>Validated By</th>
			<th>Validated At</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Session End Time</th>
			<th>Session Start Time</th>
			<th>Course Id</th>
			<th>Level Class Id</th>
			<th>School Year Id</th>
			<th>Symbol</th>
			<th>Session Order</th>
			<th>Wday Id</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($courseSchedItems as $courseSchedItem)
        <tr>
            <td>{!! $courseSchedItem->id !!}</td>
			<td>{!! $courseSchedItem->created_by !!}</td>
			<td>{!! $courseSchedItem->created_at !!}</td>
			<td>{!! $courseSchedItem->updated_by !!}</td>
			<td>{!! $courseSchedItem->updated_at !!}</td>
			<td>{!! $courseSchedItem->validated_by !!}</td>
			<td>{!! $courseSchedItem->validated_at !!}</td>
			<td>{!! $courseSchedItem->active !!}</td>
			<td>{!! $courseSchedItem->version !!}</td>
			<td>{!! $courseSchedItem->update_groups_mfk !!}</td>
			<td>{!! $courseSchedItem->delete_groups_mfk !!}</td>
			<td>{!! $courseSchedItem->display_groups_mfk !!}</td>
			<td>{!! $courseSchedItem->sci_id !!}</td>
			<td>{!! $courseSchedItem->session_end_time !!}</td>
			<td>{!! $courseSchedItem->session_start_time !!}</td>
			<td>{!! $courseSchedItem->course_id !!}</td>
			<td>{!! $courseSchedItem->level_class_id !!}</td>
			<td>{!! $courseSchedItem->school_year_id !!}</td>
			<td>{!! $courseSchedItem->symbol !!}</td>
			<td>{!! $courseSchedItem->session_order !!}</td>
			<td>{!! $courseSchedItem->wday_id !!}</td>
            <td>
                <a href="{!! route('courseSchedItems.edit', [$courseSchedItem->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('courseSchedItems.delete', [$courseSchedItem->id]) !!}" onclick="return confirm('Are you sure wants to delete this CourseSchedItem?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
