<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Day Hdate</th>
			<th>Coming Time</th>
			<th>Exit Time</th>
			<th>Coming Status Id</th>
			<th>Exit Status Id</th>
			<th>Rea User Id</th>
			<th>School Id</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($schoolMemberDays as $schoolMemberDay)
        <tr>
            <td>{!! $schoolMemberDay->id !!}</td>
			<td>{!! $schoolMemberDay->creation_user_id !!}</td>
			<td>{!! $schoolMemberDay->creation_date !!}</td>
			<td>{!! $schoolMemberDay->update_user_id !!}</td>
			<td>{!! $schoolMemberDay->update_date !!}</td>
			<td>{!! $schoolMemberDay->validation_user_id !!}</td>
			<td>{!! $schoolMemberDay->validation_date !!}</td>
			<td>{!! $schoolMemberDay->active !!}</td>
			<td>{!! $schoolMemberDay->version !!}</td>
			<td>{!! $schoolMemberDay->update_groups_mfk !!}</td>
			<td>{!! $schoolMemberDay->delete_groups_mfk !!}</td>
			<td>{!! $schoolMemberDay->display_groups_mfk !!}</td>
			<td>{!! $schoolMemberDay->sci_id !!}</td>
			<td>{!! $schoolMemberDay->day_hdate !!}</td>
			<td>{!! $schoolMemberDay->coming_time !!}</td>
			<td>{!! $schoolMemberDay->exit_time !!}</td>
			<td>{!! $schoolMemberDay->coming_status_id !!}</td>
			<td>{!! $schoolMemberDay->exit_status_id !!}</td>
			<td>{!! $schoolMemberDay->rea_user_id !!}</td>
			<td>{!! $schoolMemberDay->school_id !!}</td>
            <td>
                <a href="{!! route('schoolMemberDays.edit', [$schoolMemberDay->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('schoolMemberDays.delete', [$schoolMemberDay->id]) !!}" onclick="return confirm('Are you sure wants to delete this SchoolMemberDay?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
