@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'schoolMemberDays.store']) !!}

        @include('schoolMemberDays.fields')

    {!! Form::close() !!}
</div>
@endsection
