@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">CourseSessions</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('courseSessions.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($courseSessions->isEmpty())
                <div class="well text-center">No CourseSessions found.</div>
            @else
                @include('courseSessions.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $courseSessions])


    </div>
@endsection
