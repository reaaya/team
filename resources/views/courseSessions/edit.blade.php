@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($courseSession, ['route' => ['courseSessions.update', $courseSession->id], 'method' => 'patch']) !!}

        @include('courseSessions.fields')

    {!! Form::close() !!}
</div>
@endsection
