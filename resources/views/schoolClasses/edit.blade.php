@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($schoolClass, ['route' => ['schoolClasses.update', $schoolClass->id], 'method' => 'patch']) !!}

        @include('schoolClasses.fields')

    {!! Form::close() !!}
</div>
@endsection
