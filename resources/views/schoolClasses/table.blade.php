<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Level Class Id</th>
			<th>School Year Id</th>
			<th>School Class Name</th>
			<th>Symbol</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($schoolClasses as $schoolClass)
        <tr>
            <td>{!! $schoolClass->id !!}</td>
			<td>{!! $schoolClass->creation_user_id !!}</td>
			<td>{!! $schoolClass->creation_date !!}</td>
			<td>{!! $schoolClass->update_user_id !!}</td>
			<td>{!! $schoolClass->update_date !!}</td>
			<td>{!! $schoolClass->validation_user_id !!}</td>
			<td>{!! $schoolClass->validation_date !!}</td>
			<td>{!! $schoolClass->active !!}</td>
			<td>{!! $schoolClass->version !!}</td>
			<td>{!! $schoolClass->update_groups_mfk !!}</td>
			<td>{!! $schoolClass->delete_groups_mfk !!}</td>
			<td>{!! $schoolClass->display_groups_mfk !!}</td>
			<td>{!! $schoolClass->sci_id !!}</td>
			<td>{!! $schoolClass->level_class_id !!}</td>
			<td>{!! $schoolClass->school_year_id !!}</td>
			<td>{!! $schoolClass->school_class_name !!}</td>
			<td>{!! $schoolClass->symbol !!}</td>
            <td>
                <a href="{!! route('schoolClasses.edit', [$schoolClass->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('schoolClasses.delete', [$schoolClass->id]) !!}" onclick="return confirm('Are you sure wants to delete this SchoolClass?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
