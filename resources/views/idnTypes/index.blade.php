@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">IdnTypes</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('idnTypes.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($idnTypes->isEmpty())
                <div class="well text-center">No IdnTypes found.</div>
            @else
                @include('idnTypes.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $idnTypes])


    </div>
@endsection
