@extends('layouts.app_no_container_js')
@section('content')
<div class="m-b-lg">
    <div class="wrapper text-center">
        <strong>Input your email to reset your password</strong>
    </div>
    <form class="form-reset-password" role="form" method="POST" action="{!! url('/password/email') !!}" name="reset" ng-init="isCollapsed=true">
        <div class="list-group list-group-sm">
            <div class="list-group-item {!! $errors->has('email') ? ' has-error' : '' !!}">
                <input name="email" type="email" placeholder="Email" value="{!! old('email') !!}" ng-model="email" class="form-control no-border" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{!! $errors->first('email') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <button type="submit" ng-disabled="reset.$invalid" class="btn btn-lg btn-primary btn-block" ng-click="isCollapsed = !isCollapsed">
            <i class="fa fa-btn fa-envelope"></i>
            <span>Send Password Reset Link</span>
        </button>
        
        <div class="text-center m-t m-b">
            <a href="{!! url('/login') !!}" >
                <i class="fa fa-btn fa-sign-in"></i>
                <span>Sign in</span>
            </a>
        </div>

        @if (session('status'))
            <div collapse="isCollapsed" class="m-t">
                <div class="alert alert-success">
                    <p>{!! session('status') !!}
                        <a href="{!! url('/login') !!}" class="btn btn-sm btn-success">Sign in</a>
                    </p>
                </div>
            </div>
        @endif

        {!! csrf_field() !!}
    </form>
</div>
@endsection