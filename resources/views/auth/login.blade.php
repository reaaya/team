@extends('layouts.app_no_container_js')

@section('content')
<div class="wrapper text-center">
  <strong>Sign in to get in touch</strong>
</div>

<div class="m-b-lg">
    <!--<form class="form-validation form-login" role="form" method="POST" action="{!! url('/login') !!}">-->
    {!! Form::open(['url' => 'login', 'method' => 'POST', 'class'=> 'form-validation form-login']) !!}
    
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">

        <div class="text-danger wrapper text-center" ng-show="authError">
        </div>
        <div class="list-group list-group-sm">
            <div class="list-group-item {!! isset($errors) && $errors->has('email') ? ' has-error' : '' !!}">
                <input type="email" placeholder="Email" class="form-control no-border" name="email" value="{!! old('email') !!}" ng-model="user.email" required> @if (isset($errors) && $errors->has('email'))
                <span class="help-block">
                        <strong>{!! $errors->first('email') !!}</strong>
                    </span> @endif
            </div>
            <div class="list-group-item {!! isset($errors) && $errors->has('password') ? ' has-error' : '' !!}">
                <input type="password" placeholder="Password" class="form-control no-border" name="password" ng-model="user.password" required>
                @if (isset($errors) && $errors->has('password'))
                    <span class="help-block">
                        <strong>{!! $errors->first('password') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="checkbox m-b-md m-t-none">
            <label class="i-checks">
                <input type="checkbox" ng-model="remember" name="remember" ><i></i> Remember Me</a>
            </label>
        </div>
        <button type="submit" class="btn btn-lg btn-primary btn-block" >
            <i class="fa fa-btn fa-sign-in"></i>
            <span>Log in</span>
        </button>
        
        <div class="text-center m-t m-b">
            <a href="{!! url('/password/reset') !!}" >Forgot password?</a>
        </div>
        <div class="line line-dashed"></div>
        <p class="text-center"><small>Do not have an account?</small></p>
        <a href="{!! url('/register') !!}" class="btn btn-lg btn-default btn-block">Create an account</a>
    {!! Form::close() !!}
</div>
@endsection