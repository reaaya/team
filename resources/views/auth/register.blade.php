@extends('layouts.app_no_container_js') @section('content')
<form class="form-register" role="form" method="POST" action="{!! url('/register') !!}">
    {!! csrf_field() !!}
    <div class="text-danger wrapper text-center" ng-show="authError">
    </div>
    <div class="list-group list-group-sm">
        <div class="list-group-item {!! $errors->has('name') ? ' has-error' : '' !!}">
            <input placeholder="Name" type="text" class="form-control no-border" name="name" value="{!! old('name') !!}" ng-model="user.name" required> @if ($errors->has('name'))
            <span class="help-block">
                        <strong>{!! $errors->first('name') !!}</strong>
                    </span> @endif
        </div>
        <div class="list-group-item {!! $errors->has('email') ? ' has-error' : '' !!}">
            <input type="email" placeholder="Email" class="form-control no-border" name="email" value="{!! old('email') !!}" ng-model="user.email" required> @if ($errors->has('email'))
            <span class="help-block">
                        <strong>{!! $errors->first('email') !!}</strong>
                    </span> @endif
        </div>
        <div class="list-group-item {!! $errors->has('password') ? ' has-error' : '' !!}">
            <input type="password" placeholder="Password" class="form-control no-border" name="password" ng-model="user.password" required> @if ($errors->has('password'))
            <span class="help-block">
                        <strong>{!! $errors->first('password') !!}</strong>
                    </span> @endif
        </div>
        <div class="list-group-item {!! $errors->has('password_confirmation') ? ' has-error' : '' !!}">
            <input type="password" placeholder="Confirm Password" class="form-control no-border" name="password_confirmation" ng-model="user.password_confirmation" required> @if ($errors->has('password_confirmation'))
            <span class="help-block">
                        <strong>{!! $errors->first('password_confirmation') !!}</strong>
                    </span> @endif
        </div>
    </div>
    <div class="checkbox m-b-md m-t-none">
        <label class="i-checks">
            <input type="checkbox" ng-model="agree" required><i></i> Agree the <a href="#" >terms and policy</a>
        </label>
    </div>
    <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="signup()" ng-disabled='form.$invalid'>Sign up</button>
    

    <div class="line line-dashed"></div>
    <p class="text-center"><small>Already have an account?</small></p>
    <div class="form-group">
            <a href="{!! url('/login') !!}" class="btn btn-lg btn-default btn-block" >
                <i class="fa fa-btn fa-user"></i>
                <span>Login</span>
            </a>
    </div>
</form>
@endsection