@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'tServices.store']) !!}

        @include('tServices.fields')

    {!! Form::close() !!}
</div>
@endsection
