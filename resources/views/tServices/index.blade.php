@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">TServices</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('tServices.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($tServices->isEmpty())
                <div class="well text-center">No TServices found.</div>
            @else
                @include('tServices.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $tServices])


    </div>
@endsection
