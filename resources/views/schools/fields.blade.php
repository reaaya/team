<!-- Scapacity Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('scapacity', 'Scapacity:') !!}
	{!! Form::number('scapacity', null, ['class' => 'form-control']) !!}
</div>

<!-- Expiring Hdate Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('expiring_hdate', 'Expiring Hdate:') !!}
	{!! Form::text('expiring_hdate', null, ['class' => 'form-control']) !!}
</div>

<!-- City Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('city_id', 'City Id:') !!}
	{!! Form::number('city_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Genre Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('genre_id', 'Genre Id:') !!}
	{!! Form::number('genre_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Group School Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('group_school_id', 'Group School Id:') !!}
	{!! Form::number('group_school_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Lang Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('lang_id', 'Lang Id:') !!}
	{!! Form::number('lang_id', null, ['class' => 'form-control']) !!}
</div>

<!-- School Type Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('school_type_id', 'School Type Id:') !!}
	{!! Form::number('school_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Period Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('period_mfk', 'Period Mfk:') !!}
	{!! Form::text('period_mfk', null, ['class' => 'form-control']) !!}
</div>

<!-- Sp2 Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('sp2', 'Sp2:') !!}
	{!! Form::number('sp2', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('address', 'Address:') !!}
	{!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Maps Location Url Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('maps_location_url', 'Maps Location Url:') !!}
	{!! Form::text('maps_location_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Pc Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('pc', 'Pc:') !!}
	{!! Form::text('pc', null, ['class' => 'form-control']) !!}
</div>

<!-- Quarter Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('quarter', 'Quarter:') !!}
	{!! Form::text('quarter', null, ['class' => 'form-control']) !!}
</div>

<!-- School Name Ar Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('school_name_ar', 'School Name Ar:') !!}
	{!! Form::text('school_name_ar', null, ['class' => 'form-control']) !!}
</div>

<!-- School Name En Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('school_name_en', 'School Name En:') !!}
	{!! Form::text('school_name_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Sp1 Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('sp1', 'Sp1:') !!}
	{!! Form::number('sp1', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
