@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Schools</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('schools.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($schools->isEmpty())
                <div class="well text-center">No Schools found.</div>
            @else
                @include('schools.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $schools])


    </div>
@endsection
