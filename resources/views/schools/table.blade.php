<table class="table">
    <thead>
    <th>Id</th>
			<th>Created By</th>
			<th>Created At</th>
			<th>Updated By</th>
			<th>Updated At</th>
			<th>Validated By</th>
			<th>Validated At</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Scapacity</th>
			<th>Expiring Hdate</th>
			<th>City Id</th>
			<th>Genre Id</th>
			<th>Group School Id</th>
			<th>Lang Id</th>
			<th>School Type Id</th>
			<th>Period Mfk</th>
			<th>Sp2</th>
			<th>Address</th>
			<th>Maps Location Url</th>
			<th>Pc</th>
			<th>Quarter</th>
			<th>School Name Ar</th>
			<th>School Name En</th>
			<th>Sp1</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($schools as $school)
        <tr>
            <td>{!! $school->id !!}</td>
			<td>{!! $school->created_by !!}</td>
			<td>{!! $school->created_at !!}</td>
			<td>{!! $school->updated_by !!}</td>
			<td>{!! $school->updated_at !!}</td>
			<td>{!! $school->validated_by !!}</td>
			<td>{!! $school->validated_at !!}</td>
			<td>{!! $school->active !!}</td>
			<td>{!! $school->version !!}</td>
			<td>{!! $school->update_groups_mfk !!}</td>
			<td>{!! $school->delete_groups_mfk !!}</td>
			<td>{!! $school->display_groups_mfk !!}</td>
			<td>{!! $school->sci_id !!}</td>
			<td>{!! $school->scapacity !!}</td>
			<td>{!! $school->expiring_hdate !!}</td>
			<td>{!! $school->city_id !!}</td>
			<td>{!! $school->genre_id !!}</td>
			<td>{!! $school->group_school_id !!}</td>
			<td>{!! $school->lang_id !!}</td>
			<td>{!! $school->school_type_id !!}</td>
			<td>{!! $school->period_mfk !!}</td>
			<td>{!! $school->sp2 !!}</td>
			<td>{!! $school->address !!}</td>
			<td>{!! $school->maps_location_url !!}</td>
			<td>{!! $school->pc !!}</td>
			<td>{!! $school->quarter !!}</td>
			<td>{!! $school->school_name_ar !!}</td>
			<td>{!! $school->school_name_en !!}</td>
			<td>{!! $school->sp1 !!}</td>
            <td>
                <a href="{!! route('schools.edit', [$school->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('schools.delete', [$school->id]) !!}" onclick="return confirm('Are you sure wants to delete this School?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
