@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">AttendanceTypes</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('attendanceTypes.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($attendanceTypes->isEmpty())
                <div class="well text-center">No AttendanceTypes found.</div>
            @else
                @include('attendanceTypes.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $attendanceTypes])


    </div>
@endsection
