<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Attendance Type Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($attendanceTypes as $attendanceType)
        <tr>
            <td>{!! $attendanceType->id !!}</td>
			<td>{!! $attendanceType->creation_user_id !!}</td>
			<td>{!! $attendanceType->creation_date !!}</td>
			<td>{!! $attendanceType->update_user_id !!}</td>
			<td>{!! $attendanceType->update_date !!}</td>
			<td>{!! $attendanceType->validation_user_id !!}</td>
			<td>{!! $attendanceType->validation_date !!}</td>
			<td>{!! $attendanceType->active !!}</td>
			<td>{!! $attendanceType->version !!}</td>
			<td>{!! $attendanceType->update_groups_mfk !!}</td>
			<td>{!! $attendanceType->delete_groups_mfk !!}</td>
			<td>{!! $attendanceType->display_groups_mfk !!}</td>
			<td>{!! $attendanceType->sci_id !!}</td>
			<td>{!! $attendanceType->attendance_type_name !!}</td>
            <td>
                <a href="{!! route('attendanceTypes.edit', [$attendanceType->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('attendanceTypes.delete', [$attendanceType->id]) !!}" onclick="return confirm('Are you sure wants to delete this AttendanceType?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
