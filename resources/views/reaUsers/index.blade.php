@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">ReaUsers</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('reaUsers.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($reaUsers->isEmpty())
                <div class="well text-center">No ReaUsers found.</div>
            @else
                @include('reaUsers.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $reaUsers])


    </div>
@endsection
