@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'reaUsers.store']) !!}

        @include('reaUsers.fields')

    {!! Form::close() !!}
</div>
@endsection
