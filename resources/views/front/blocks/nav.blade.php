<!-- list -->
<ul class="nav">
  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.HEADER">Navigation</span>
  </li>
  <li>
    <a href class="auto">      
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
      <span class="font-bold" translate="aside.nav.DASHBOARD">Dashboard</span>
    </a>
    <ul class="nav nav-sub dk">
      <li class="nav-sub-header">
        <a href>
          <span translate="aside.nav.DASHBOARD">Dashboard</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="admin.schools_generator">
          <span>Schools Generator</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="coursesession.timeline">
          <span>Courses Session</span>
        </a>
      </li>
    </ul>
  </li>
</ul>
<!-- / list -->
