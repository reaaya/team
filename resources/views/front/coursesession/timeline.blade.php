<div ng-controller="CourseSessionTimelineController">
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n h3">Courses Sessions</h1>
    </div>
    <div class="wrapper-md">
        

        <div class="" >
            <ul class="liste-timeline-course-session" >
                <li ng-repeat="item in TimelineCourseSession">
                    <div class="panel panel-default">
                        <div class="panel-heading font-bold">
                            <h4 class="m-n h4 text-center">Session {{item.session_order}}</h4>
                            <h5 class="m-n font-thin h5 text-center">{{item.course}}</h5>
                        </div>
                        <div class="panel-body">
                            <div class="text-center">
                                <span class="circle-time circle-start-time">
                                    <i class="fa fa-clock-o"></i>
                                    <span>{{item.session_start_time}}</span>
                                </span>
                                <span class="circle-difference-time">
                                    <i>40 min</i>
                                </span>
                                <span class="circle-time circle-end-time">
                                    <i class="fa fa-clock-o"></i>
                                    <span>{{item.session_end_time}}</span>
                                </span>
                            </div>
                        </div>
                        <div class="panel-footer text-center footer-btns-timeline">
                            <a href="#" class="btn btn-danger"><i class="fa fa-times"></i><span>Cancel</span></a>
                            <a href="#" class="btn btn-primary"><i class="fa fa-folder-open"></i><span>Open</span></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>


    </div>
</div>