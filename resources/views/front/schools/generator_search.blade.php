<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Schools generator</h1>
</div>
<div class="wrapper-md">

    <div class="panel panel-default" >
        <div class="panel-heading font-bold">
            Select school class
        </div>

        <div class="panel-body" >
            {!! Form::open(['route' => 'schools.generator', 'id'=> 'FormSearchGenerator' ]) !!}

                <div class="form-container" >
                    <div class="form-group">
                        <div class="row" >
                            <label class="col-sm-3 control-label text-left">{!! Lang::get('schools.school_name') !!}</label>
                            <div class="col-sm-9">{!! Form::select('school_id', [0 => '-- Please select the School --'] + $schools, 0, ['id'=> 'school_id', 'class'=>'form-control' ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row" >
                            <label class="col-sm-3 control-label text-left">{!! Lang::get('schools.school_years') !!}</label>
                            <div class="col-sm-9">
                                {!! Form::select('school_year_id', [0 => '-- Please select the Year --'] , 0, ['id'=> 'school_year_id', 'class'=>'form-control', 'disabled'=>'disabled']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row" >
                            <label class="col-sm-3 control-label text-left">{!! Lang::get('schools.school_levels') !!}</label>
                            <div class="col-sm-9">
                                {!! Form::select('level_class_id', [0 => '-- Please select the Level --'] , 0, ['id'=> 'level_class_id', 'class'=>'form-control', 'disabled'=>'disabled']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row" >
                            <label class="col-sm-3 control-label text-left">{!! Lang::get('schools.school_class') !!}</label>
                            <div class="col-sm-9">
                                {!! Form::select('symbol_id', [0 => '-- Please select the class --'] , 0, ['id'=> 'symbol_id', 'class'=>'form-control', 'disabled'=>'disabled']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-submit text-right">
                        {!! Form::submit('Search', [ 'id'=> 'btn-submit-generator', 'class'=>'btn btn-large btn-primary'] ) !!}
                    </div>
                </div>

            {!! Form::close() !!}
        </div>
    </div>

    

        {!! Form::open(['route' => 'schools.generator', 'id'=> 'FormGenerateCoursesWeek' ]) !!}
            <div class="form-container" >
                <div class="form-group">
                    <table class="table table-bordered table-striped table-generate-courses-week" id="TableGenerateCoursesWeek" >
                        <thead>
                            <tr class="HeadNewTemplate" id="HeadNewTemplate" >
                                <th width="50%" ><div class="alert alert-danger" id="MsgEmptySessionsTemplate" >Empty Q0010</div></th>
                                <th >
                                    {!! Form::select('week_template', [0 => '-- Please select yout template --'] , 0, ['id'=> 'week_template_id', 'class'=>'form-control', 'disabled'=>'disabled']) !!}
                                </th>
                                <th><button class="btn btn-primary" id="BtnCreatFromTemplate" >Creat from template</button></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="td-session" >Session</td>
                                <td class="td-day td-day-1" >Sunday</td>
                                <td class="td-day td-day-2" >Monday</td>
                                <td class="td-day td-day-3" >Tuesday</td>
                                <td class="td-day td-day-4" >Wednesday</td>
                                <td class="td-day td-day-5" >Thursday</td>
                                <td class="td-day td-day-6" >Friday</td>
                                <td class="td-day td-day-7" >Saturday</td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="FootSaveSessions " id="FootSaveSessions"  >
                        <div class="alert alert-success" id="MsgSuccessUpdateSessions" >Success Update</div>
                        <div class="text-right" ><button class="btn btn-primary" id="BtnSaveSessions" >Save Sessions</button></div>
                    </div>
                    
                </div>
            </div>
        {!! Form::close() !!}

    </div>

    

    <script type="text/javascript">
            $(document).ready(function(){

                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                });

                
                FormSearchGenerator = $('#FormSearchGenerator');
                FormGenerateCoursesWeek = $('#FormGenerateCoursesWeek');
                FormGenerateCoursesWeek.hide();

                SelectSchoolID = $('#school_id');
                SelectSchoolYearID = $('#school_year_id');
                SelectLevelClassID = $('#level_class_id');
                SelectSymbolID = $('#symbol_id');
                

                TableGenerateCoursesWeek = $('#TableGenerateCoursesWeek');

                HeadNewTemplate = $('#HeadNewTemplate');
                HeadNewTemplate.hide();

                FootSaveSessions = $('#FootSaveSessions');
                FootSaveSessions.hide();

                SelectWeekTemplate = $('#week_template_id');
                SelectWeekTemplate.attr('disabled',true);

                BtnCreatFromTemplate = $('#BtnCreatFromTemplate');
                
                BtnCreatFromTemplate.attr('disabled',true);

                BtnSaveSessions = $('#BtnSaveSessions');


                BtnSubmitGenerator = $('#btn-submit-generator');
                BtnSubmitGenerator.attr('disabled',true);

                MsgSuccessUpdateSessions = $('#MsgSuccessUpdateSessions');
                MsgSuccessUpdateSessions.hide();

                MsgEmptySessionsTemplate = $('#MsgEmptySessionsTemplate');



                SelectOptionsAllCourses = $('<select>');
                SelectOptionsAllCourses.prepend( $('<option>', {value:"",text: "-- Select Course --" } ) );
                $.get("{!! url('/api/Course/getAllCourses') !!}",function(response){
                    if(response.data){
                        $.each( response.data, function( key, item ) {
                            SelectOptionsAllCourses.append($('<option>', {
                                value: item.id,
                                text: item.course_name
                            }));
                        });
                    }
                });


                SelectSchoolID.on('change',function(){
                    if(SelectSchoolID.val()==="0"){
                        SelectSchoolYearID.attr('disabled','disabled').val("0");
                        SelectLevelClassID.attr('disabled','disabled').val("0");
                        SelectSymbolID.attr('disabled','disabled').val("0");
                        BtnSubmitGenerator.attr('disabled',true);
                        HeadNewTemplate.hide();
                        FormGenerateCoursesWeek.hide();
                    }else{
                        $.get("{!! url('/api/SchoolYear/getAllSchoolYearsBySchoolID') !!}/"+SelectSchoolID.val(),function(response){
                            SelectSchoolYearID.attr('disabled',false).val("0");
                            SelectSchoolYearID.find('option[value!="0"]').remove();
                            SelectLevelClassID.find('option[value!="0"]').remove();
                            SelectSymbolID.find('option[value!="0"]').remove();
                            HeadNewTemplate.hide();
                            FormGenerateCoursesWeek.hide();

                            if(response.data){
                                $.each( response.data, function( key, item ) {
                                    SelectSchoolYearID.append($('<option>', {
                                        value: item.id,
                                        text: item.school_year_name
                                    }));
                                });
                            }

                            SelectSchoolYearID.change();
                        },'json');
                    }
                });



                SelectSchoolYearID.on('change',function(){
                    if(SelectSchoolYearID.val()==="0"){
                        SelectLevelClassID.attr('disabled','disabled').val("0");
                        SelectSymbolID.attr('disabled','disabled').val("0");
                        BtnSubmitGenerator.attr('disabled',true);
                        HeadNewTemplate.hide();
                        FormGenerateCoursesWeek.hide();
                    }else{
                        $.get("{!! url('/api/SchoolLevel/getAllSchoolLevelsBySchoolYearID') !!}/"+SelectSchoolYearID.val(),function(response){
                            SelectLevelClassID.attr('disabled',false).val("0");
                            SelectLevelClassID.find('option[value!="0"]').remove();
                            SelectSymbolID.find('option[value!="0"]').remove();
                            HeadNewTemplate.hide();
                            FormGenerateCoursesWeek.hide();

                            if(response.data){
                                $.each( response.data, function( key, item ) {
                                    SelectLevelClassID.append($('<option>', {
                                        value: item.id,
                                        text: item.level_class_name
                                    }));
                                });
                            }

                            SelectLevelClassID.change();
                        },'json');
                    }
                });


                SelectLevelClassID.on('change',function(){
                    if(SelectLevelClassID.val()==="0"){
                        SelectSymbolID.attr('disabled','disabled').val("0");
                        BtnSubmitGenerator.attr('disabled',true);
                        HeadNewTemplate.hide();
                        FormGenerateCoursesWeek.hide();
                    }else{
                        $.get("{!! url('/api/SchoolClass/getAllSchoolClassBySchoolYearIDAndLevelClassID') !!}/"+SelectSchoolYearID.val()+'/'+SelectLevelClassID.val(),function(response){
                            SelectSymbolID.attr('disabled',false).val("0");
                            SelectSymbolID.find('option[value!="0"]').remove();
                            BtnSubmitGenerator.attr('disabled',true);
                            HeadNewTemplate.hide();
                            FormGenerateCoursesWeek.hide();

                            if(response.data){
                                $.each( response.data, function( key, item ) {
                                    SelectSymbolID.append($('<option>', {
                                        value: item.id,
                                        text: item.symbol
                                    }));
                                });
                            }
                        },'json');

                        SelectSymbolID.change();
                    }
                });
                
                SelectSymbolID.on('change',function(){
                    if(SelectSymbolID.val()==="0"){
                        BtnSubmitGenerator.attr('disabled',true);
                        HeadNewTemplate.hide();
                        FormGenerateCoursesWeek.hide();
                    }else{
                        BtnSubmitGenerator.attr('disabled',false);
                    }
                });


                FormSearchGenerator.submit(function(event) {
                    $.post("{!! url('/api/CourseSchedItem/GetItems') !!}",FormSearchGenerator.serialize(),function(response){
                        TableGenerateCoursesWeek.find('.ligne_session_course').remove();
                        responseSearch = response;
                        if(!response.status){
                            newSchoolClassTableGenerator(responseSearch);
                        }else{
                            printSchoolClassTableGenerator(responseSearch);
                        }

                    },'json');
                    return false;
                });

                function CountMaxSessions(data){
                    max_sessions = 0;
                    $.each( data, function( key, item ) {
                            if(Object.keys(item).length>max_sessions){
                                max_sessions = size = Object.keys(item).length;
                            }
                        });
                    return max_sessions;
                }

                function SessionsTimesIsEqual(data){
                    is_equal = true;
                    times = [];
                    times['start_time'] = [];
                    times['end_time'] = [];
                    max_sessions = CountMaxSessions(data);
                    $.each( data, function( key, item ) {
                            for (i = 1; i <= max_sessions; i++) {
                                if( typeof times['start_time'][i] === 'undefined' ){
                                    times[i] = item.session_start_time;
                                }
                                if( typeof times['end_time'][i] === 'undefined' ){
                                    times[i] = item.session_end_time;
                                }

                                if( times['start_time'][i]!== item.session_start_time){
                                    is_equal = false;
                                }
                                if( times['end_time'][i]!== item.session_end_time){
                                    is_equal = false;
                                }
                            }
                    });
                    return is_equal;
                }

                function GenerateSelectDaySession(data, TimesIsEqual){
                    if(!data){
                        return null;
                    }
                    SelectDaySession = $('<div>');

                    if( TimesIsEqual===false ){
                        SelectDaySession.append( $('<span class="span-start-time" >').html('<h6>Start <i>'+data.session_start_time+'</i></h5>') );
                        SelectDaySession.append( $('<span class="span-end-time" >').html('<h6>End <i>'+data.session_end_time+'</i>') );
                    }
                    
                    NewSelect = SelectOptionsAllCourses.clone();
                    SelectDaySession.append( NewSelect );
                    SelectDaySession.find('select').attr( {name:'course_sched_item['+data.id+']', class:'form-control'} );
                    SelectDaySession.find('select').find('option[value="'+data.course_id+'"]').attr('selected', 'selected');

                    return SelectDaySession;
                }

                function printSchoolClassTableGenerator(response){
                    MsgSuccessUpdateSessions.hide();
                    FootSaveSessions.show();
                    HeadNewTemplate.hide();
                    FormGenerateCoursesWeek.find('tbody').show();
                    FormGenerateCoursesWeek.hide();
                    
                    max_sessions = CountMaxSessions(response.data);
                    TimesIsEqual = SessionsTimesIsEqual(response.data);

                    if( typeof response.data[1] === 'undefined' ) response.data[1] = {};
                    if( typeof response.data[2] === 'undefined' ) response.data[2] = {};
                    if( typeof response.data[3] === 'undefined' ) response.data[3] = {};
                    if( typeof response.data[4] === 'undefined' ) response.data[4] = {};
                    if( typeof response.data[5] === 'undefined' ) response.data[5] = {};
                    if( typeof response.data[6] === 'undefined' ) response.data[6] = {};
                    if( typeof response.data[7] === 'undefined' ) response.data[7] = {};

                    

                    for (i = 1; i <= max_sessions; i++) {
                        if( typeof response.data[1][i] === 'undefined' ) response.data[1][i] = null;
                        if( typeof response.data[2][i] === 'undefined' ) response.data[2][i] = null;
                        if( typeof response.data[3][i] === 'undefined' ) response.data[3][i] = null;
                        if( typeof response.data[4][i] === 'undefined' ) response.data[4][i] = null;
                        if( typeof response.data[5][i] === 'undefined' ) response.data[5][i] = null;
                        if( typeof response.data[6][i] === 'undefined' ) response.data[6][i] = null;
                        if( typeof response.data[7][i] === 'undefined' ) response.data[7][i] = null;

                        column_Session = $('<td>',{class:'td-session'}).append( '<h5><strong>Session</strong> <i>'+i+'</i></h5>' );
                        if( TimesIsEqual ){
                            column_Session.append( '<h6>Start <i>'+response.data[1][i].session_start_time+'</i></h6>' )
                            column_Session.append( '<h6>End <i>'+response.data[1][i].session_end_time+'</i></h6>' )
                        }
                        column_Sunday = $('<td>',{class:'td-day td-day-1'}).append( GenerateSelectDaySession( response.data[1][i], TimesIsEqual) );
                        column_Monday = $('<td>',{class:'td-day td-day-2'}).append( GenerateSelectDaySession( response.data[2][i], TimesIsEqual) );
                        column_Tuesday = $('<td>',{class:'td-day td-day-3'}).append( GenerateSelectDaySession( response.data[3][i], TimesIsEqual) );
                        column_Wednesday = $('<td>',{class:'td-day td-day-4'}).append( GenerateSelectDaySession( response.data[4][i], TimesIsEqual) );
                        column_Thursday = $('<td>',{class:'td-day td-day-5'}).append( GenerateSelectDaySession( response.data[5][i], TimesIsEqual));
                        column_Friday = $('<td>',{class:'td-day td-day-6'}).append( GenerateSelectDaySession( response.data[6][i], TimesIsEqual) );
                        column_Saturday = $('<td>',{class:'td-day td-day-7'}).append( GenerateSelectDaySession( response.data[7][i], TimesIsEqual) );


                        
                        new_tr = $('<tr>',{class:'ligne_session_course'})
                        .append(column_Session)
                        .append(column_Sunday)
                        .append(column_Monday)
                        .append(column_Tuesday)
                        .append(column_Wednesday)
                        .append(column_Thursday)
                        .append(column_Friday)
                        .append(column_Saturday);

                        TableGenerateCoursesWeek.find('tbody').append(new_tr);
                    }


                    $.get("{!! url('/api/School/GetWeekDays') !!}/"+SelectSchoolID.val(),function(response2){
                        $('.td-day, td-session').show();
                        //$('.TdMsgSuccessUpdateSessions').attr('colspan',6-response2.data.length);
                        $.each( response2.data, function( key, item_ ) {
                            class_ = '.td-day-'+item_;
                            $(class_).hide();
                        });
                        FormGenerateCoursesWeek.show();
                    },'json');
                    
                   
                }

                function newSchoolClassTableGenerator(response){
                    HeadNewTemplate.show();
                    FormGenerateCoursesWeek.show();
                    FormGenerateCoursesWeek.find('tbody').hide();
                    FootSaveSessions.hide();
                    
                    SelectWeekTemplate.attr('disabled',false).val("0");
                    SelectWeekTemplate.find('option[value!="0"]').remove();
                    BtnCreatFromTemplate.attr('disabled',true);

                    $.get("{!! url('/api/WeekTemplate/GetDefaultTemplate') !!}",function(response){
                            if(response.data){
                                $.each( response.data, function( key, item ) {
                                    SelectWeekTemplate.append($('<option>', {
                                        value: item.id,
                                        text: item.week_template_name
                                    }));
                                });
                            }
                        },'json');
                }


                SelectWeekTemplate.on('change',function(){
                    if(SelectWeekTemplate.val()==="0"){
                        BtnCreatFromTemplate.attr('disabled',true);
                    }else{
                        BtnCreatFromTemplate.attr('disabled',false);
                    }
                });

                BtnCreatFromTemplate.click(function(event) {
                    data = {
                        school_year_id: SelectSchoolYearID.val(),
                        level_class_id: SelectLevelClassID.val(),
                        symbol: SelectSymbolID.val(),
                        week_template_id: SelectWeekTemplate.val()
                    };
                    $.post("{!! url('/api/CourseSchedItem/CreatFromWeekTemplate') !!}",data,function(response){
                            FormSearchGenerator.submit();
                        },'json');
                    return false;
                });


                
                BtnSaveSessions.click(function(event) {
                    MsgSuccessUpdateSessions.hide();
                    $.post("{!! url('/api/CourseSchedItem/SaveItems') !!}",FormGenerateCoursesWeek.serialize(),function(response){
                            MsgSuccessUpdateSessions.show();
                        },'json');
                    return false;
                });

                

            });
    </script>
