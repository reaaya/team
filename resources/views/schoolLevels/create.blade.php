@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'schoolLevels.store']) !!}

        @include('schoolLevels.fields')

    {!! Form::close() !!}
</div>
@endsection
