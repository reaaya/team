@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($schoolLevel, ['route' => ['schoolLevels.update', $schoolLevel->id], 'method' => 'patch']) !!}

        @include('schoolLevels.fields')

    {!! Form::close() !!}
</div>
@endsection
