<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $examSession->id !!}</p>
</div>

<!-- Creation User Id Field -->
<div class="form-group">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
    <p>{!! $examSession->creation_user_id !!}</p>
</div>

<!-- Creation Date Field -->
<div class="form-group">
    {!! Form::label('creation_date', 'Creation Date:') !!}
    <p>{!! $examSession->creation_date !!}</p>
</div>

<!-- Update User Id Field -->
<div class="form-group">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
    <p>{!! $examSession->update_user_id !!}</p>
</div>

<!-- Update Date Field -->
<div class="form-group">
    {!! Form::label('update_date', 'Update Date:') !!}
    <p>{!! $examSession->update_date !!}</p>
</div>

<!-- Validation User Id Field -->
<div class="form-group">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
    <p>{!! $examSession->validation_user_id !!}</p>
</div>

<!-- Validation Date Field -->
<div class="form-group">
    {!! Form::label('validation_date', 'Validation Date:') !!}
    <p>{!! $examSession->validation_date !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $examSession->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $examSession->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $examSession->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $examSession->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $examSession->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $examSession->sci_id !!}</p>
</div>

<!-- Exam Session Name Field -->
<div class="form-group">
    {!! Form::label('exam_session_name', 'Exam Session Name:') !!}
    <p>{!! $examSession->exam_session_name !!}</p>
</div>

