<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Exam Session Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($examSessions as $examSession)
        <tr>
            <td>{!! $examSession->id !!}</td>
			<td>{!! $examSession->creation_user_id !!}</td>
			<td>{!! $examSession->creation_date !!}</td>
			<td>{!! $examSession->update_user_id !!}</td>
			<td>{!! $examSession->update_date !!}</td>
			<td>{!! $examSession->validation_user_id !!}</td>
			<td>{!! $examSession->validation_date !!}</td>
			<td>{!! $examSession->active !!}</td>
			<td>{!! $examSession->version !!}</td>
			<td>{!! $examSession->update_groups_mfk !!}</td>
			<td>{!! $examSession->delete_groups_mfk !!}</td>
			<td>{!! $examSession->display_groups_mfk !!}</td>
			<td>{!! $examSession->sci_id !!}</td>
			<td>{!! $examSession->exam_session_name !!}</td>
            <td>
                <a href="{!! route('examSessions.edit', [$examSession->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('examSessions.delete', [$examSession->id]) !!}" onclick="return confirm('Are you sure wants to delete this ExamSession?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
