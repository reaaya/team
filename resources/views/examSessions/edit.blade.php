@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($examSession, ['route' => ['examSessions.update', $examSession->id], 'method' => 'patch']) !!}

        @include('examSessions.fields')

    {!! Form::close() !!}
</div>
@endsection
