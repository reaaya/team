<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
    <meta charset="utf-8" />
    <title>Angular version | Angulr</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/libs/assets/animate.css/animate.css") !!}" type="text/css" />
    <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/libs/assets/font-awesome/css/font-awesome.min.css") !!}" type="text/css" />
    <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/libs/assets/simple-line-icons/css/simple-line-icons.css") !!}" type="text/css" />
    <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/libs/jquery/bootstrap/dist/css/bootstrap.css") !!}" type="text/css" />
    <!-- build:css css/app.min.css -->
    <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/css/font.css") !!}" type="text/css" />
    <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/css/app.css") !!}" type="text/css" />
    <!-- endbuild -->
    <!--<link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/libs/assets/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") !!}" type="text/css" />-->
    <!--<link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/css/app.rtl.css") !!}" type="text/css" />-->
</head>

<body>
    <div class="app app-header-fixed ">
        <div class="container w-xxl w-auto-xs" ng-controller="SigninFormController" ng-init="app.settings.container = false;">
            <div class="text-center" >
                <a href="{!! url('/') !!}" class="link-logo">
                    <img src="{!! URL::asset("themes/admin/assets/img/logo.png") !!}" />
                </a>
            </div>
            @yield('content')
        </div>
        <div class="text-center">
            <p>
                <small class="text-muted">Reaaya<br>&copy; 2016</small>
            </p>
        </div>
    </div>
    <script src="{!! URL::asset("themes/admin/assets/libs/jquery/jquery/dist/jquery.js") !!}"></script>
    <script src="{!! URL::asset("themes/admin/assets/libs/jquery/bootstrap/dist/js/bootstrap.js") !!}"></script>
    <script src="{!! URL::asset("themes/admin/theme/html/js/ui-load.js") !!}"></script>
    <script src="{!! URL::asset("themes/admin/theme/html/js/ui-jp.config.js") !!}"></script>
    <script src="{!! URL::asset("themes/admin/theme/html/js/ui-jp.js") !!}"></script>
    <script src="{!! URL::asset("themes/admin/theme/html/js/ui-nav.js") !!}"></script>
    <script src="{!! URL::asset("themes/admin/theme/html/js/ui-toggle.js") !!}"></script>
    <script src="{!! URL::asset("themes/admin/theme/html/js/ui-client.js") !!}"></script>
</body>
</html>