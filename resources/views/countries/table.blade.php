<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Time Offset</th>
			<th>Maintenance End Time</th>
			<th>Maintenance Start Time</th>
			<th>Date System Id</th>
			<th>Abrev</th>
			<th>Country Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($countries as $country)
        <tr>
            <td>{!! $country->id !!}</td>
			<td>{!! $country->creation_user_id !!}</td>
			<td>{!! $country->creation_date !!}</td>
			<td>{!! $country->update_user_id !!}</td>
			<td>{!! $country->update_date !!}</td>
			<td>{!! $country->validation_user_id !!}</td>
			<td>{!! $country->validation_date !!}</td>
			<td>{!! $country->active !!}</td>
			<td>{!! $country->version !!}</td>
			<td>{!! $country->update_groups_mfk !!}</td>
			<td>{!! $country->delete_groups_mfk !!}</td>
			<td>{!! $country->display_groups_mfk !!}</td>
			<td>{!! $country->sci_id !!}</td>
			<td>{!! $country->time_offset !!}</td>
			<td>{!! $country->maintenance_end_time !!}</td>
			<td>{!! $country->maintenance_start_time !!}</td>
			<td>{!! $country->date_system_id !!}</td>
			<td>{!! $country->abrev !!}</td>
			<td>{!! $country->country_name !!}</td>
            <td>
                <a href="{!! route('countries.edit', [$country->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('countries.delete', [$country->id]) !!}" onclick="return confirm('Are you sure wants to delete this Country?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
