<!-- Hijri Year Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('HIJRI_YEAR', 'Hijri Year:') !!}
	{!! Form::number('HIJRI_YEAR', null, ['class' => 'form-control']) !!}
</div>

<!-- Hijri Month Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('HIJRI_MONTH', 'Hijri Month:') !!}
	{!! Form::number('HIJRI_MONTH', null, ['class' => 'form-control']) !!}
</div>

<!-- Hijri Month Days Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('HIJRI_MONTH_DAYS', 'Hijri Month Days:') !!}
	{!! Form::number('HIJRI_MONTH_DAYS', null, ['class' => 'form-control']) !!}
</div>

<!-- Greg Date Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('GREG_DATE', 'Greg Date:') !!}
	{!! Form::text('GREG_DATE', null, ['class' => 'form-control']) !!}
</div>

<!-- Holiday Days Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('HOLIDAY_DAYS', 'Holiday Days:') !!}
	{!! Form::text('HOLIDAY_DAYS', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
