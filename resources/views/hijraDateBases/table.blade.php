<table class="table">
    <thead>
    <th>Hijri Year</th>
			<th>Hijri Month</th>
			<th>Hijri Month Days</th>
			<th>Greg Date</th>
			<th>Holiday Days</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($hijraDateBases as $hijraDateBase)
        <tr>
            <td>{!! $hijraDateBase->HIJRI_YEAR !!}</td>
			<td>{!! $hijraDateBase->HIJRI_MONTH !!}</td>
			<td>{!! $hijraDateBase->HIJRI_MONTH_DAYS !!}</td>
			<td>{!! $hijraDateBase->GREG_DATE !!}</td>
			<td>{!! $hijraDateBase->HOLIDAY_DAYS !!}</td>
            <td>
                <a href="{!! route('hijraDateBases.edit', [$hijraDateBase->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('hijraDateBases.delete', [$hijraDateBase->id]) !!}" onclick="return confirm('Are you sure wants to delete this HijraDateBase?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
