@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'hijraDateBases.store']) !!}

        @include('hijraDateBases.fields')

    {!! Form::close() !!}
</div>
@endsection
