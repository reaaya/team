<!-- Lookup Code Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('lookup_code', 'Lookup Code:') !!}
	{!! Form::text('lookup_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Id School Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id_school', 'Id School:') !!}
	{!! Form::number('id_school', null, ['class' => 'form-control']) !!}
</div>

<!-- Max Pct Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('max_pct', 'Max Pct:') !!}
	{!! Form::number('max_pct', null, ['class' => 'form-control']) !!}
</div>

<!-- Min Pct Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('min_pct', 'Min Pct:') !!}
	{!! Form::number('min_pct', null, ['class' => 'form-control']) !!}
</div>

<!-- Rating Name Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rating_name', 'Rating Name:') !!}
	{!! Form::text('rating_name', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
