<table class="table">
    <thead>
    <th>Lookup Code</th>
			<th>Id School</th>
			<th>Max Pct</th>
			<th>Min Pct</th>
			<th>Rating Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($ratings as $rating)
        <tr>
            <td>{!! $rating->lookup_code !!}</td>
			<td>{!! $rating->id_school !!}</td>
			<td>{!! $rating->max_pct !!}</td>
			<td>{!! $rating->min_pct !!}</td>
			<td>{!! $rating->rating_name !!}</td>
            <td>
                <a href="{!! route('ratings.edit', [$rating->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('ratings.delete', [$rating->id]) !!}" onclick="return confirm('Are you sure wants to delete this Rating?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
