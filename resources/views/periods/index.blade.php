@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Periods</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('periods.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($periods->isEmpty())
                <div class="well text-center">No Periods found.</div>
            @else
                @include('periods.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $periods])


    </div>
@endsection
