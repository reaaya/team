<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Course Id</th>
			<th>Level Class Id</th>
			<th>Class Course Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($classCourses as $classCourse)
        <tr>
            <td>{!! $classCourse->id !!}</td>
			<td>{!! $classCourse->creation_user_id !!}</td>
			<td>{!! $classCourse->creation_date !!}</td>
			<td>{!! $classCourse->update_user_id !!}</td>
			<td>{!! $classCourse->update_date !!}</td>
			<td>{!! $classCourse->validation_user_id !!}</td>
			<td>{!! $classCourse->validation_date !!}</td>
			<td>{!! $classCourse->active !!}</td>
			<td>{!! $classCourse->version !!}</td>
			<td>{!! $classCourse->update_groups_mfk !!}</td>
			<td>{!! $classCourse->delete_groups_mfk !!}</td>
			<td>{!! $classCourse->display_groups_mfk !!}</td>
			<td>{!! $classCourse->sci_id !!}</td>
			<td>{!! $classCourse->course_id !!}</td>
			<td>{!! $classCourse->level_class_id !!}</td>
			<td>{!! $classCourse->class_course_name !!}</td>
            <td>
                <a href="{!! route('classCourses.edit', [$classCourse->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('classCourses.delete', [$classCourse->id]) !!}" onclick="return confirm('Are you sure wants to delete this ClassCourse?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
