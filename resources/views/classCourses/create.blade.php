@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'classCourses.store']) !!}

        @include('classCourses.fields')

    {!! Form::close() !!}
</div>
@endsection
