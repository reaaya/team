@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">ClassCourses</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('classCourses.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($classCourses->isEmpty())
                <div class="well text-center">No ClassCourses found.</div>
            @else
                @include('classCourses.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $classCourses])


    </div>
@endsection
