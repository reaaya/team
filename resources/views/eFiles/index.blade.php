@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">EFiles</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('eFiles.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($eFiles->isEmpty())
                <div class="well text-center">No EFiles found.</div>
            @else
                @include('eFiles.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $eFiles])


    </div>
@endsection
