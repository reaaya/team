<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $eFile->id !!}</p>
</div>

<!-- Creation User Id Field -->
<div class="form-group">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
    <p>{!! $eFile->creation_user_id !!}</p>
</div>

<!-- Creation Date Field -->
<div class="form-group">
    {!! Form::label('creation_date', 'Creation Date:') !!}
    <p>{!! $eFile->creation_date !!}</p>
</div>

<!-- Update User Id Field -->
<div class="form-group">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
    <p>{!! $eFile->update_user_id !!}</p>
</div>

<!-- Update Date Field -->
<div class="form-group">
    {!! Form::label('update_date', 'Update Date:') !!}
    <p>{!! $eFile->update_date !!}</p>
</div>

<!-- Validation User Id Field -->
<div class="form-group">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
    <p>{!! $eFile->validation_user_id !!}</p>
</div>

<!-- Validation Date Field -->
<div class="form-group">
    {!! Form::label('validation_date', 'Validation Date:') !!}
    <p>{!! $eFile->validation_date !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $eFile->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $eFile->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $eFile->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $eFile->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $eFile->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $eFile->sci_id !!}</p>
</div>

<!-- Efile Name Field -->
<div class="form-group">
    {!! Form::label('efile_name', 'Efile Name:') !!}
    <p>{!! $eFile->efile_name !!}</p>
</div>

